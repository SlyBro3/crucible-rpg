package com.me.sly.CrucibleEmpire.MobSpawns;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.me.sly.CrucibleEmpire.Main.ceMain;


public class GradeAMobSpawns implements Listener {
	public static ceMain plugin;
    public GradeAMobSpawns(ceMain instance) {
        plugin = instance;
    }
	@EventHandler
	public void SkeletonDrop(final EntityDeathEvent event){
		int ChestHP = plugin.rand(1, 51);
		int LegHP = plugin.rand(1, 51);
		int BootHP = plugin.rand(1, 51);
		int HelmHP = plugin.rand(1, 51);
		int SwordDMG = plugin.rand(1, 51);
		int AxeDMG = plugin.rand(1, 51);
		int BowDMG = plugin.rand(1, 51);
		int SpearDMG = plugin.rand(1, 51);
		ItemStack Boots = new ItemStack(Material.DIAMOND_BOOTS);
		ItemStack Leggings = new ItemStack(Material.DIAMOND_LEGGINGS);
		ItemStack Chestplate = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemStack Helmet = new ItemStack(Material.DIAMOND_HELMET);
		ItemStack Sword = new ItemStack(Material.DIAMOND_SWORD );
		ItemStack Axe = new ItemStack(Material.DIAMOND_AXE);
		ItemStack Bow = new ItemStack(Material.BOW);
		ItemStack Spear = new ItemStack(Material.DIAMOND_SPADE);
		ItemStack Stave = new ItemStack(Material.DIAMOND_HOE);
		ItemStack EScroll = new ItemStack(Material.PAPER);
		ItemStack WScroll = new ItemStack(Material.PAPER);
		ItemMeta BootsMeta = Boots.getItemMeta();
		ItemMeta LeggingsMeta = Leggings.getItemMeta();
		ItemMeta ChestplateMeta = Chestplate.getItemMeta();
		ItemMeta HelmetMeta = Helmet.getItemMeta();
		ItemMeta SwordMeta = Sword.getItemMeta();
		ItemMeta AxeMeta = Axe.getItemMeta();
		ItemMeta BowMeta = Bow.getItemMeta();
		ItemMeta SpearMeta = Spear.getItemMeta();
		ItemMeta StaveMeta = Stave.getItemMeta();
		ItemMeta EMeta = EScroll.getItemMeta();
		ItemMeta WMeta = WScroll.getItemMeta();
		ArrayList<String> BootsLore = new ArrayList<String>();
		ArrayList<String> LeggingsLore = new ArrayList<String>();
		ArrayList<String> ChestplateLore = new ArrayList<String>();
		ArrayList<String> HelmetLore = new ArrayList<String>();
		ArrayList<String> SwordLore = new ArrayList<String>();
		ArrayList<String> AxeLore = new ArrayList<String>();
		ArrayList<String> ELore = new ArrayList<String>();
		ArrayList<String> WLore = new ArrayList<String>();
		ArrayList<String> BowLore = new ArrayList<String>();
		ArrayList<String> SpearLore = new ArrayList<String>();
		ArrayList<String> StaveLore = new ArrayList<String>();
		BootsMeta.setDisplayName(ChatColor.AQUA + "Mythical Boots");
		LeggingsMeta.setDisplayName(ChatColor.AQUA + "Mythical Leggings");
		ChestplateMeta.setDisplayName(ChatColor.AQUA + "Mythical Chestplate");
		HelmetMeta.setDisplayName(ChatColor.AQUA + "Mythical Helmet");
		SwordMeta.setDisplayName(ChatColor.AQUA + "Mythical Long Sword");
		AxeMeta.setDisplayName(ChatColor.AQUA + "Mythical War Axe");
		BowMeta.setDisplayName(ChatColor.AQUA + "Mythical Long Bow");
		SpearMeta.setDisplayName(ChatColor.AQUA + "Mythical Glave");
		StaveMeta.setDisplayName(ChatColor.AQUA + "Mythical Stave");
		EMeta.setDisplayName(ChatColor.BLUE + "Armor Enchant Scroll:");
		ELore.add("�fHas a chance to");
		ELore.add("�ffail after +3");
		WMeta.setDisplayName(ChatColor.BLUE + "Weapon Enchant Scroll:");
		WLore.add("�fHas a chance to");
		WLore.add("�ffail after +3");
		if(BootHP < 20){
		BootsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(1500, 1900));
		BootsLore.add("");
		BootsLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(BootHP > 20 && BootHP < 41){
		BootsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(1900, 2200));
		BootsLore.add("");
		BootsLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(BootHP > 40 && BootHP < 46){
		BootsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(2200, 2500));
		BootsLore.add("");
		BootsLore.add(ChatColor.GOLD + "�oRare");
		}else{
		BootsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(2500, 2800));
		BootsLore.add("");
		BootsLore.add("�b�oLegendary");
		}
		if(LegHP < 20){
		LeggingsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(2400, 3000));
		LeggingsLore.add("");
		LeggingsLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(LegHP > 20 && LegHP < 41){
		LeggingsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(3000, 3300));
		LeggingsLore.add("");
		LeggingsLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(LegHP > 40 && LegHP < 46){
		LeggingsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(3300, 3700));
		LeggingsLore.add("");
		LeggingsLore.add(ChatColor.GOLD + "�oRare");
		}else{
		LeggingsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(3700, 4000));
		LeggingsLore.add("");
		LeggingsLore.add("�b�oLegendary");
		}
		if(ChestHP < 20){
		ChestplateLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(2400, 3000));
		ChestplateLore.add("");
		ChestplateLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(ChestHP > 20 && ChestHP < 41){
		ChestplateLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(3000, 3300));
		ChestplateLore.add("");
		ChestplateLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(ChestHP > 40 && ChestHP < 46){
		ChestplateLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(3300, 3700));
		ChestplateLore.add("");
		ChestplateLore.add(ChatColor.GOLD + "�oRare");
		}else{
		ChestplateLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(3700, 4000));
		ChestplateLore.add("");
		ChestplateLore.add("�b�oLegendary");
		}
		if(HelmHP < 20){
		HelmetLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(1500, 1900));
		HelmetLore.add("");
		HelmetLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(HelmHP > 20 && HelmHP < 41){
		HelmetLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(1900, 2200));
		HelmetLore.add("");
		HelmetLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(HelmHP > 40 && HelmHP < 46){
		HelmetLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(2200, 2500));
		HelmetLore.add("");
		HelmetLore.add(ChatColor.GOLD + "�oRare");
		}else{
		HelmetLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(2500, 2800));
		HelmetLore.add("");
		HelmetLore.add("�b�oLegendary");
		}
		if(SwordDMG < 20){
			SwordLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(80, 120) + " - " + plugin.rand(140, 180) + " Damage");
			SwordLore.add("");
			SwordLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(SwordDMG > 20 && SwordDMG < 41){
			SwordLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(140, 180) + " - " + plugin.rand(180, 240) + " Damage");
			SwordLore.add("");
			SwordLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(SwordDMG > 40 && SwordDMG < 46){
			SwordLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(180, 220) + " - " + plugin.rand(220, 280) + " Damage");
			SwordLore.add("");
			SwordLore.add(ChatColor.GOLD + "�oRare");
		}else{
			SwordLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(220, 300) + " - " + plugin.rand(300, 340) + " Damage");
			SwordLore.add("");
			SwordLore.add("�b�oLegendary");
		}
		if(AxeDMG < 20){
			AxeLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(120, 160) + " - " + plugin.rand(160, 240) + " Damage");
			AxeLore.add("");
			AxeLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(AxeDMG > 20 && AxeDMG < 41){
			AxeLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(200, 260) + " - " + plugin.rand(260, 280) + " Damage");
			AxeLore.add("");
			AxeLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(AxeDMG > 40 && AxeDMG < 46){
			AxeLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(220, 280) + " - " + plugin.rand(280, 340) + " Damage");
			AxeLore.add("");
			AxeLore.add(ChatColor.GOLD + "�oRare");
		}else{
			AxeLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(280, 360) + " - " + plugin.rand(360, 400) + " Damage");
			AxeLore.add("");
			AxeLore.add("�b�oLegendary");
		}
		if(BowDMG < 20){
			BowLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(130, 150) + " - " + plugin.rand(150, 175) + " Damage");
			BowLore.add("");
			BowLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(BowDMG > 20 && BowDMG < 41){
			BowLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(145, 160) + " - " + plugin.rand(160, 190) + " Damage");
			BowLore.add("");
			BowLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(BowDMG > 40 && BowDMG < 46){
			BowLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(175, 190) + " - " + plugin.rand(190, 210) + " Damage");
			BowLore.add("");
			BowLore.add(ChatColor.GOLD + "�oRare");
		}else{
			BowLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(200, 220) + " - " + plugin.rand(220, 250) + " Damage");
			BowLore.add("");
			BowLore.add("�b�oLegendary");
		}
		if(SpearDMG < 20){
			SpearLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(65, 75) + " - " + plugin.rand(75, 90) + " Damage");
			SpearLore.add("");
			SpearLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(SpearDMG > 20 && SpearDMG < 41){
			SpearLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(75, 85) + " - " + plugin.rand(85, 100) + " Damage");
			SpearLore.add("");
			SpearLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(SpearDMG > 40 && SpearDMG < 46){
			SpearLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(90, 110) + " - " + plugin.rand(110, 130) + " Damage");
			SpearLore.add("");
			SpearLore.add(ChatColor.GOLD + "�oRare");
		}else{
			SpearLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(105, 120) + " - " + plugin.rand(120, 150) + " Damage");
			SpearLore.add("");
			SpearLore.add("�b�oLegendary");
		}
		BootsMeta.setLore(BootsLore);
		SwordMeta.setLore(SwordLore);
		ChestplateMeta.setLore(ChestplateLore);
		LeggingsMeta.setLore(LeggingsLore);
		HelmetMeta.setLore(HelmetLore);
		AxeMeta.setLore(AxeLore);
		BowMeta.setLore(BowLore);
		SpearMeta.setLore(SpearLore);
		StaveMeta.setLore(StaveLore);
		Boots.setItemMeta(BootsMeta);
		Sword.setItemMeta(SwordMeta);
		Axe.setItemMeta(AxeMeta);
		Chestplate.setItemMeta(ChestplateMeta);
		Leggings.setItemMeta(LeggingsMeta);
		Helmet.setItemMeta(HelmetMeta);
		Spear.setItemMeta(SpearMeta);
		Bow.setItemMeta(BowMeta);
		Stave.setItemMeta(StaveMeta);
		event.setDroppedExp(0);
		EMeta.setLore(ELore);
		WMeta.setLore(WLore);
		EScroll.setItemMeta(EMeta);
		WScroll.setItemMeta(WMeta);
		event.setDroppedExp(0);
		if(event.getEntity().getKiller() instanceof Player){
		if(!(event.getEntity() instanceof Player)){
			if(event.getEntity().isCustomNameVisible() == true){
		if(((String)event.getEntity().getMetadata("Grade").get(0).value()).contains("A") && !event.getEntity().hasMetadata("GolemBoss")){
			int Drop = plugin.rand(1, 151);
			int Stat = plugin.rand(1, 12);
			int Gold = plugin.rand(1, 21);
			int Scroll = plugin.rand(1, 2001);
			int Block = plugin.rand(1, 26);
			int HPRegen= plugin.rand(1, 9);
			ItemStack GoldDrop = new ItemStack(Material.DOUBLE_PLANT, plugin.rand(30, 70));
			ItemMeta GoldMeta = GoldDrop.getItemMeta();
			ArrayList<String> GoldLore = new ArrayList<String>();
			GoldMeta.setDisplayName("�6�lGold Coin");
			GoldLore.add("�eUsed as currency in");
			GoldLore.add("�eall of Tranomia");
			GoldMeta.setLore(GoldLore);
			GoldDrop.setItemMeta(GoldMeta);
		event.getDrops().clear();
		if(Gold <= 3){	
		}else{
		event.getDrops().add(GoldDrop);
		}
		if(event.getEntity().getEquipment().getItemInHand().getType() == Material.BOW){
			ItemStack Arrow = new ItemStack(Material.ARROW);
			ItemMeta ArrowMeta = Arrow.getItemMeta();
			List<String> ArrowLore = new ArrayList<String>();
			ArrowMeta.setDisplayName("�bMythical Arrow");
			ArrowLore.add("�eThis arrow is shot");
			ArrowLore.add("�eby the �bMythical Long Bow"); 
			ArrowLore.add("�9Grade A Arrow");
			ArrowMeta.setLore(ArrowLore);
			Arrow.setItemMeta(ArrowMeta);
				Arrow.setAmount(plugin.rand(1, 6));
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Arrow);
			}
				event.setDroppedExp(0);
				if(Drop == 6){ // HELMET
				if(Block == 1){
					HelmetLore.add(HelmetLore.get(2));
					HelmetLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 15));
					HelmetMeta.setLore(HelmetLore);
					Helmet.setItemMeta(HelmetMeta);
				}
				if(HPRegen == 1){
					HelmetLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(100, 150) + "/s");
					HelmetMeta.setLore(HelmetLore);
					Helmet.setItemMeta(HelmetMeta);
				}else if(HPRegen == 2){
					HelmetLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(50, 85) + "/s");
					HelmetMeta.setLore(HelmetLore);
					Helmet.setItemMeta(HelmetMeta);
				}
				String rsrs = HelmetLore.get(2);
				HelmetLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				HelmetLore.add(rsrs);
				HelmetMeta.setLore(HelmetLore);
				Helmet.setItemMeta(HelmetMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Helmet);
			}else if(Drop == 12){ // CHESTPLATE
				if(Block == 1){
					ChestplateLore.add(ChestplateLore.get(2));
					ChestplateLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 15));
					ChestplateMeta.setLore(ChestplateLore);
					Chestplate.setItemMeta(ChestplateMeta);
				}
				if(HPRegen == 1){
					ChestplateLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(125, 175) + "/s");
					ChestplateMeta.setLore(ChestplateLore);
					Chestplate.setItemMeta(ChestplateMeta);
				}else if(HPRegen == 2){
					ChestplateLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(50, 85) + "/s");
					ChestplateMeta.setLore(ChestplateLore);
					Chestplate.setItemMeta(ChestplateMeta);
				}
				String rsrs = ChestplateLore.get(2);
				ChestplateLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				ChestplateLore.add(rsrs);
				ChestplateMeta.setLore(ChestplateLore);
				Chestplate.setItemMeta(ChestplateMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Chestplate);
			}else if(Drop == 14){ // LEGS
				if(Block == 1){
					LeggingsLore.add(LeggingsLore.get(2));
					LeggingsLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 15));
					LeggingsMeta.setLore(LeggingsLore);
					Leggings.setItemMeta(LeggingsMeta);
				}
				if(HPRegen == 1){
					LeggingsLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(125, 175) + "/s");
					LeggingsMeta.setLore(LeggingsLore);
					Leggings.setItemMeta(LeggingsMeta);
				}else if(HPRegen == 2){
					LeggingsLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(50, 85) + "/s");
					LeggingsMeta.setLore(LeggingsLore);
					Leggings.setItemMeta(LeggingsMeta);
				}
				String rsrs = LeggingsLore.get(2);
				LeggingsLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				LeggingsLore.add(rsrs);
				LeggingsMeta.setLore(LeggingsLore);
				Leggings.setItemMeta(LeggingsMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Leggings);
			}else if(Drop == 11){ // BOOTS
				if(Block == 1){
					BootsLore.add(BootsLore.get(2));
					BootsLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 15));
					BootsMeta.setLore(BootsLore);
					Boots.setItemMeta(BootsMeta);
				}
				if(HPRegen == 1){
					BootsLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(100, 150) + "/s");
					BootsMeta.setLore(BootsLore);
					Boots.setItemMeta(BootsMeta);
				}else if(HPRegen == 2){
					BootsLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(50, 85) + "/s");
					BootsMeta.setLore(BootsLore);
					Boots.setItemMeta(BootsMeta);
				}
				String rsrs = BootsLore.get(2);
				BootsLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				BootsLore.add(rsrs);
				BootsMeta.setLore(BootsLore);
				Boots.setItemMeta(BootsMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Boots);
			}else if(Drop == 10 || Drop == 1 || Drop == 2){ // SWORD
				if(event.getEntity().getEquipment().getItemInHand().getType() == Material.DIAMOND_SWORD){
				//Fire Damage
				if(Stat == 1){
					SwordLore.set(1, ChatColor.RED + "Fire DMG " + ChatColor.DARK_RED + "+" + plugin.rand(5, 17));
					SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �4Fire");
					SwordMeta.setLore(SwordLore);
					Sword.setItemMeta(SwordMeta);
					//Ice Damage
				}else if(Stat == 2){
					SwordLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.DARK_BLUE + "+" + plugin.rand(5, 17));
					SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �1Ice");
					SwordMeta.setLore(SwordLore);
					Sword.setItemMeta(SwordMeta);
					//Soul Siphon
				}else if(Stat == 3){
					SwordLore.set(1, ChatColor.YELLOW + "Soul Siphon " + ChatColor.GOLD + "" + plugin.rand(1, 4) + "%");
					SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �6Vampirism");
					SwordMeta.setLore(SwordLore);
					Sword.setItemMeta(SwordMeta);
				}else if(Stat == 4){
					SwordLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(8, 20));
					SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �2Poison");
					SwordMeta.setLore(SwordLore);
					Sword.setItemMeta(SwordMeta);
					//Soul Siphon
				}	
				String rsrs = SwordLore.get(2);
				SwordLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				SwordLore.add(rsrs);
				SwordMeta.setLore(SwordLore);
				Sword.setItemMeta(SwordMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Sword);
				}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.DIAMOND_AXE){
					//Fire Damage
					if(Stat == 5){
						AxeLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.DARK_BLUE + "+" + plugin.rand(2, 10));
						AxeMeta.setDisplayName(AxeMeta.getDisplayName() + " of �1Ice");
						AxeMeta.setLore(AxeLore);
						Axe.setItemMeta(AxeMeta);
						//Soul Siphon
					}else if(Stat == 4){
						AxeLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(5, 14));
						AxeMeta.setDisplayName(AxeMeta.getDisplayName() + " of �2Poison");
						AxeMeta.setLore(AxeLore);
						Axe.setItemMeta(AxeMeta);
						//Soul Siphon
					}
					String rsrs = AxeLore.get(2);
					AxeLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
					AxeLore.add(rsrs);
					AxeMeta.setLore(AxeLore);
					Axe.setItemMeta(AxeMeta);
					event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Axe);	
				}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.DIAMOND_SPADE){
					//Fire Damage
					if(Stat == 5){
						SpearLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.DARK_BLUE+ "+" + plugin.rand(2, 10));
						SpearMeta.setDisplayName(SpearMeta.getDisplayName() + " of �2Ice");
						SpearMeta.setLore(SpearLore);
						Spear.setItemMeta(SpearMeta);
						//Soul Siphon
					}else if(Stat == 4){
						SpearLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(5, 14));
						SpearMeta.setDisplayName(SpearMeta.getDisplayName() + " of �2Poison");
						SpearMeta.setLore(SpearLore);
						Spear.setItemMeta(SpearMeta);
						//Soul Siphon
					}else if(Stat == 3){
						SpearLore.set(1, ChatColor.RED + "Fire DMG " + ChatColor.DARK_RED + "+" + plugin.rand(5, 17));
						SpearMeta.setDisplayName(SpearMeta.getDisplayName() + " of �4Fire");
						SpearMeta.setLore(SpearLore);
						Spear.setItemMeta(SpearMeta);
						//Ice Damage
					}
					String rsrs = SpearLore.get(2);
					SpearLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
					SpearLore.add(rsrs);
					SpearMeta.setLore(SpearLore);
					Spear.setItemMeta(SpearMeta);
					event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Spear);	
				}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.BOW){
					//Fire Damage
					if(Stat == 5){
						BowLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.DARK_BLUE + "+" + plugin.rand(2, 10));
						BowMeta.setDisplayName(BowMeta.getDisplayName() + "of �1Ice");
						BowMeta.setLore(BowLore);
						Bow.setItemMeta(BowMeta);
						//Soul Siphon
					}else if(Stat == 4){
						BowLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(5, 14));
						BowMeta.setDisplayName(BowMeta.getDisplayName() + " of �2Poison");
						BowMeta.setLore(BowLore);
						Bow.setItemMeta(BowMeta);
						//Soul Siphon
					}
					String rsrs = BowLore.get(2);
					BowLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
					BowLore.add(rsrs);
					BowMeta.setLore(BowLore);
					Bow.setItemMeta(BowMeta);
					event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Bow);	
				}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.DIAMOND_HOE){
						StaveLore.set(1, "�4Unbound");
					String rsrs = StaveLore.get(2);
					StaveLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
					StaveLore.add(rsrs);
					StaveMeta.setLore(StaveLore);
					Stave.setItemMeta(StaveMeta);
					event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Stave);	
				}
			}
				if(Scroll == 100){
					event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), EScroll);
				}else if(Scroll == 101){
					event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), WScroll);
				}
		}
			}
		}
	}
	}
	static int Stop = 0;
	public static void StopF(){
		Bukkit.getServer().getScheduler().cancelTask(Stop);
	}
}
