package com.me.sly.CrucibleEmpire.MobSpawns;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftLivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.me.sly.CrucibleEmpire.Main.ceMain;
import com.me.sly.CrucibleEmpire.YMLfiles.PlayerYML;
import com.me.sly.resources.Hologram;


public class GradeDMobSpawns implements Listener {
	public static ceMain plugin;
    public GradeDMobSpawns(ceMain instance) {
        plugin = instance;
    }
	@EventHandler
	public void NormalDrops(final EntityDeathEvent event){
		int ChestHP = plugin.rand(1, 101);
		int LegHP = plugin.rand(1, 101);
		int BootHP = plugin.rand(1, 101);
		int HelmHP = plugin.rand(1, 1011);
		int SwordDMG = plugin.rand(1, 101);
		int AxeDMG = plugin.rand(1, 101);
		int BowDMG = plugin.rand(1, 101);
		int SpearDMG = plugin.rand(1, 101);
		ItemStack Boots = new ItemStack(Material.CHAINMAIL_BOOTS);
		ItemStack Leggings = new ItemStack(Material.CHAINMAIL_LEGGINGS);
		ItemStack Chestplate = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
		ItemStack Helmet = new ItemStack(Material.CHAINMAIL_HELMET);
		ItemStack Sword = new ItemStack(Material.STONE_SWORD );
		ItemStack Axe = new ItemStack(Material.STONE_AXE);
		ItemStack Bow = new ItemStack(Material.BOW);
		ItemStack Spear = new ItemStack(Material.STONE_SPADE);
		ItemStack Stave = new ItemStack(Material.STONE_HOE);
		ItemMeta BootsMeta = Boots.getItemMeta();
		ItemMeta LeggingsMeta = Leggings.getItemMeta();
		ItemMeta ChestplateMeta = Chestplate.getItemMeta();
		ItemMeta HelmetMeta = Helmet.getItemMeta();
		ItemMeta SwordMeta = Sword.getItemMeta();
		ItemMeta AxeMeta = Axe.getItemMeta();
		ItemMeta BowMeta = Bow.getItemMeta();
		ItemMeta SpearMeta = Spear.getItemMeta();
		ItemMeta StaveMeta = Stave.getItemMeta();
		ArrayList<String> BootsLore = new ArrayList<String>();
		ArrayList<String> LeggingsLore = new ArrayList<String>();
		ArrayList<String> ChestplateLore = new ArrayList<String>();
		ArrayList<String> HelmetLore = new ArrayList<String>();
		ArrayList<String> SwordLore = new ArrayList<String>();
		ArrayList<String> AxeLore = new ArrayList<String>();
		ArrayList<String> BowLore = new ArrayList<String>();
		ArrayList<String> SpearLore = new ArrayList<String>();
		ArrayList<String> StaveLore = new ArrayList<String>();
		BootsMeta.setDisplayName(ChatColor.GRAY + "Dark Boots");
		LeggingsMeta.setDisplayName(ChatColor.GRAY + "Dark Leggings");
		ChestplateMeta.setDisplayName(ChatColor.GRAY + "Dark Chestplate");
		HelmetMeta.setDisplayName(ChatColor.GRAY + "Dark Helmet");
		SwordMeta.setDisplayName(ChatColor.GRAY + "Dark Short Sword");
		AxeMeta.setDisplayName(ChatColor.GRAY + "Dark Broad Axe");
		BowMeta.setDisplayName(ChatColor.GRAY + "Dark Reflex Bow");
		SpearMeta.setDisplayName(ChatColor.GRAY + "Dark Pike");
		StaveMeta.setDisplayName(ChatColor.GRAY + "Dark Stave");
		if(BootHP < 60){
		BootsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(80, 140));
		BootsLore.add("");
		BootsLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(BootHP > 60 && BootHP < 91){
		BootsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(140, 200));
		BootsLore.add("");
		BootsLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(BootHP > 90 && BootHP < 97){
		BootsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(200, 240));
		BootsLore.add("");
		BootsLore.add(ChatColor.GOLD + "�oRare");
		}else{
		BootsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(240, 300));
		BootsLore.add("");
		BootsLore.add("�b�oLegendary");
		}
		if(LegHP < 60){
		LeggingsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(200, 260));
		LeggingsLore.add("");
		LeggingsLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(LegHP > 60 && LegHP < 91){
		LeggingsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(260, 320));
		LeggingsLore.add("");
		LeggingsLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(LegHP > 90 && LegHP < 97){
		LeggingsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(320, 390));
		LeggingsLore.add("");
		LeggingsLore.add(ChatColor.GOLD + "�oRare");
		}else{
		LeggingsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(390, 440));
		LeggingsLore.add("");
		LeggingsLore.add("�b�oLegendary");
		}
		if(ChestHP < 60){
		ChestplateLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(200, 260));
		ChestplateLore.add("");
		ChestplateLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(ChestHP > 60 && ChestHP < 91){
		ChestplateLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(260, 320));
		ChestplateLore.add("");
		ChestplateLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(ChestHP > 90 && ChestHP < 97){
		ChestplateLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(320, 390));
		ChestplateLore.add("");
		ChestplateLore.add(ChatColor.GOLD + "�oRare");
		}else{
		ChestplateLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(390, 440));
		ChestplateLore.add("");
		ChestplateLore.add("�b�oLegendary");
		}
		if(HelmHP < 60){
		HelmetLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(80, 140));
		HelmetLore.add("");
		HelmetLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(HelmHP > 60 && HelmHP < 91){
		HelmetLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(140, 200));
		HelmetLore.add("");
		HelmetLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(HelmHP > 90 && HelmHP < 97){
		HelmetLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(200, 240));
		HelmetLore.add("");
		HelmetLore.add(ChatColor.GOLD + "�oRare");
		}else{
		HelmetLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(240, 300));
		HelmetLore.add("");
		HelmetLore.add("�b�oLegendary");
		}
		if(SwordDMG < 60){
			SwordLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(5, 10) + " - " + plugin.rand(15, 20) + " Damage");
			SwordLore.add("");
			SwordLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(SwordDMG > 60 && SwordDMG < 91){
			SwordLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(10, 20) + " - " + plugin.rand(20, 40) + " Damage");
			SwordLore.add("");
			SwordLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(SwordDMG > 90 && SwordDMG < 97){
			SwordLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(15, 25) + " - " + plugin.rand(25, 50) + " Damage");
			SwordLore.add("");
			SwordLore.add(ChatColor.GOLD + "�oRare");
		}else{
			SwordLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(25, 35) + " - " + plugin.rand(35, 60) + " Damage");
			SwordLore.add("");
			SwordLore.add("�b�oLegendary");
		}
		if(AxeDMG < 60){
			AxeLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(10, 15) + " - " + plugin.rand(20, 30) + " Damage");
			AxeLore.add("");
			AxeLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(AxeDMG > 60 && AxeDMG < 91){
			AxeLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(15, 25) + " - " + plugin.rand(30, 55) + " Damage");
			AxeLore.add("");
			AxeLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(AxeDMG > 90 && AxeDMG < 97){
			AxeLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(20, 35) + " - " + plugin.rand(35, 60) + " Damage");
			AxeLore.add("");
			AxeLore.add(ChatColor.GOLD + "�oRare");
		}else{
			AxeLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(30, 40) + " - " + plugin.rand(45, 70) + " Damage");
			AxeLore.add("");
			AxeLore.add("�b�oLegendary");
		}
		if(BowDMG < 60){
			BowLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(15, 25) + " - " + plugin.rand(25, 35) + " Damage");
			BowLore.add("");
			BowLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(BowDMG > 60 && BowDMG < 91){
			BowLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(20, 30) + " - " + plugin.rand(30, 40) + " Damage");
			BowLore.add("");
			BowLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(BowDMG > 90 && BowDMG < 97){
			BowLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(35, 45) + " - " + plugin.rand(45, 55) + " Damage");
			BowLore.add("");
			BowLore.add(ChatColor.GOLD + "�oRare");
		}else{
			BowLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(50, 65) + " - " + plugin.rand(65, 80) + " Damage");
			BowLore.add("");
			BowLore.add("�b�oLegendary");
		}
		if(SpearDMG < 60){
			SpearLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(2, 5) + " - " + plugin.rand(5, 10) + " Damage");
			SpearLore.add("");
			SpearLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(SpearDMG > 60 && SpearDMG < 91){
			SpearLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(7, 12) + " - " + plugin.rand(12, 20) + " Damage");
			SpearLore.add("");
			SpearLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(SpearDMG > 90 && SpearDMG < 97){
			SpearLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(10, 15) + " - " + plugin.rand(15, 30) + " Damage");
			SpearLore.add("");
			SpearLore.add(ChatColor.GOLD + "�oRare");
		}else{
			SpearLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(20, 30) + " - " + plugin.rand(30, 45) + " Damage");
			SpearLore.add("");
			SpearLore.add("�b�oLegendary");
		}
		BootsMeta.setLore(BootsLore);
		SwordMeta.setLore(SwordLore);
		ChestplateMeta.setLore(ChestplateLore);
		LeggingsMeta.setLore(LeggingsLore);
		HelmetMeta.setLore(HelmetLore);
		AxeMeta.setLore(AxeLore);
		BowMeta.setLore(BowLore);
		SpearMeta.setLore(SpearLore);
		StaveMeta.setLore(StaveLore);
		Boots.setItemMeta(BootsMeta);
		Sword.setItemMeta(SwordMeta);
		Axe.setItemMeta(AxeMeta);
		Chestplate.setItemMeta(ChestplateMeta);
		Leggings.setItemMeta(LeggingsMeta);
		Helmet.setItemMeta(HelmetMeta);
		Spear.setItemMeta(SpearMeta);
		Bow.setItemMeta(BowMeta);
		Stave.setItemMeta(StaveMeta);
		event.setDroppedExp(0);
		if(event.getEntity().getKiller() instanceof Player){
			if(event.getEntity() instanceof Player){
				plugin.getPlayerConfig(event.getEntity().getKiller().getName()).set(event.getEntity().getKiller().getName() + ".Murdering.Kills", plugin.getPlayerConfig(event.getEntity().getKiller().getName()).getInt(event.getEntity().getKiller().getName() + ".Murdering.Kills") + 1);
				PlayerYML.saveCustomConfig(event.getEntity().getKiller().getName()); 
				PlayerYML.reloadCustomConfig(event.getEntity().getKiller().getName());
			}else{
			plugin.mxb.addXp(event.getEntity().getKiller(), ((int)((CraftLivingEntity)event.getEntity()).getMaxHealth())/250);
			plugin.getPlayerConfig(event.getEntity().getKiller().getName()).set(event.getEntity().getKiller().getName() + ".Slaying.Kills", plugin.getPlayerConfig(event.getEntity().getKiller().getName()).getInt(event.getEntity().getKiller().getName() + ".Slaying.Kills") + 1);
			PlayerYML.saveCustomConfig(event.getEntity().getKiller().getName()); 
			PlayerYML.reloadCustomConfig(event.getEntity().getKiller().getName());
			if(((int)((CraftLivingEntity)event.getEntity()).getMaxHealth())/250 != 0){
			new Hologram("�c�l+" + ((int)((CraftLivingEntity)event.getEntity()).getMaxHealth())/250 + " XP").show(event.getEntity().getLocation().add(0, 0.5, 0), 50);
			}else{
				new Hologram("�c�l+1 XP").show(event.getEntity().getLocation().add(0, 0.5, 0), 50);				
			}
			}
		if(!(event.getEntity() instanceof Player)){
			if(event.getEntity().isCustomNameVisible() == true){
		if(event.getEntity().getMetadata("Grade").get(0).value() == "D"){
			int Drop = plugin.rand(1, 51);
			int Stat = plugin.rand(1, 12);
			int Gold = plugin.rand(1, 21);
			int Block = plugin.rand(1, 26);
			int HPRegen = plugin.rand(1, 9);
			ItemStack GoldDrop = new ItemStack(Material.DOUBLE_PLANT, plugin.rand(1, 8));
			ItemMeta GoldMeta = GoldDrop.getItemMeta();
			ArrayList<String> GoldLore = new ArrayList<String>();
			GoldMeta.setDisplayName("�6�lGold Coin");
			GoldLore.add("�eUsed as currency in");
			GoldLore.add("�eall of Tranomia");
			GoldMeta.setLore(GoldLore);
			GoldDrop.setItemMeta(GoldMeta);
		event.getDrops().clear();
		if(Gold <= 3){	
		}else{
		event.getDrops().add(GoldDrop);
		}
		if(event.getEntity().getEquipment().getItemInHand().getType() == Material.BOW){
			ItemStack Arrow = new ItemStack(Material.ARROW);
			ItemMeta ArrowMeta = Arrow.getItemMeta();
			List<String> ArrowLore = new ArrayList<String>();
			ArrowMeta.setDisplayName("�7Dark Arrow");
			ArrowLore.add("�eThis arrow is shot");
			ArrowLore.add("�eby the �7Old Reflex Bow"); 
			ArrowLore.add("�9Grade D Arrow");
			ArrowMeta.setLore(ArrowLore);
			Arrow.setItemMeta(ArrowMeta);
				Arrow.setAmount(plugin.rand(1, 6));
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Arrow);
		}
				event.setDroppedExp(0);
				if(Drop == 6){ // HELMET
				if(Block == 1){
					HelmetLore.add(HelmetLore.get(2));
					HelmetLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 5));
					HelmetMeta.setLore(HelmetLore);
					Helmet.setItemMeta(HelmetMeta);
				}
				if(HPRegen == 1){
					HelmetLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(5, 15) + "/s");
					HelmetMeta.setLore(HelmetLore);
					Helmet.setItemMeta(HelmetMeta);
				}else if(HPRegen == 2){
					HelmetLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(1, 6) + "/s");
					HelmetMeta.setLore(HelmetLore);
					Helmet.setItemMeta(HelmetMeta);
				}
				String rsrs = HelmetLore.get(2);
				HelmetLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				HelmetLore.add(rsrs);
				HelmetMeta.setLore(HelmetLore);
				Helmet.setItemMeta(HelmetMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Helmet);
			}else if(Drop == 12){ // CHESTPLATE
				if(Block == 1){
					ChestplateLore.add(ChestplateLore.get(2));
					ChestplateLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 5));
					ChestplateMeta.setLore(ChestplateLore);
					Chestplate.setItemMeta(ChestplateMeta);
				}
				if(HPRegen == 1){
					ChestplateLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(10, 20) + "/s");
					ChestplateMeta.setLore(ChestplateLore);
					Chestplate.setItemMeta(ChestplateMeta);
				}else if(HPRegen == 2){
					ChestplateLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(1, 6) + "/s");
					ChestplateMeta.setLore(ChestplateLore);
					Chestplate.setItemMeta(ChestplateMeta);
				}
				String rsrs = ChestplateLore.get(2);
				ChestplateLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				ChestplateLore.add(rsrs);
				ChestplateMeta.setLore(ChestplateLore);
				Chestplate.setItemMeta(ChestplateMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Chestplate);
			}else if(Drop == 14){ // LEGS
				if(Block == 1){
					LeggingsLore.add(LeggingsLore.get(2));
					LeggingsLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 5));
					LeggingsMeta.setLore(LeggingsLore);
					Leggings.setItemMeta(LeggingsMeta);
				}
				if(HPRegen == 1){
					LeggingsLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(10, 20) + "/s");
					LeggingsMeta.setLore(LeggingsLore);
					Leggings.setItemMeta(LeggingsMeta);
				}else if(HPRegen == 2){
					LeggingsLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(1, 6) + "/s");
					LeggingsMeta.setLore(LeggingsLore);
					Leggings.setItemMeta(LeggingsMeta);
				}
				String rsrs = LeggingsLore.get(2);
				LeggingsLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				LeggingsLore.add(rsrs);
				LeggingsMeta.setLore(LeggingsLore);
				Leggings.setItemMeta(LeggingsMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Leggings);
			}else if(Drop == 11){ // BOOTS
				if(Block == 1){
					BootsLore.add(BootsLore.get(2));
					BootsLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 5));
					BootsMeta.setLore(BootsLore);
					Boots.setItemMeta(BootsMeta);
				}
				if(HPRegen == 1){
					BootsLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(5, 10) + "/s");
					BootsMeta.setLore(BootsLore);
					Boots.setItemMeta(BootsMeta);
				}else if(HPRegen == 2){
					BootsLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(1, 6) + "/s");
					BootsMeta.setLore(BootsLore);
					Boots.setItemMeta(BootsMeta);
				}
				String rsrs = BootsLore.get(2);
				BootsLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				BootsLore.add(rsrs);
				BootsMeta.setLore(BootsLore);
				Boots.setItemMeta(BootsMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Boots);
			}else if(Drop == 10 || Drop == 1 || Drop == 2){ // SWORD
				if(event.getEntity().getEquipment().getItemInHand().getType() == Material.STONE_SWORD){
				//Fire Damage
				if(Stat == 1){
					SwordLore.set(1, ChatColor.RED + "Fire DMG " + ChatColor.DARK_RED + "+" + plugin.rand(5, 17));
					SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �4Fire");
					SwordMeta.setLore(SwordLore);
					Sword.setItemMeta(SwordMeta);
					//Ice Damage
				}else if(Stat == 2){
					SwordLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.DARK_BLUE + "+" + plugin.rand(5, 17));
					SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �9Ice");
					SwordMeta.setLore(SwordLore);
					Sword.setItemMeta(SwordMeta);
					//Soul Siphon
				}else if(Stat == 3){
					SwordLore.set(1, ChatColor.YELLOW + "Soul Siphon " + ChatColor.GOLD + "" + plugin.rand(1, 4) + "%");
					SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �6Vampirism");
					SwordMeta.setLore(SwordLore);
					Sword.setItemMeta(SwordMeta);
				}else if(Stat == 4){
					SwordLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(8, 20));
					SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �2Poison");
					SwordMeta.setLore(SwordLore);
					Sword.setItemMeta(SwordMeta);
					//Soul Siphon
				}	
				String rsrs = SwordLore.get(2);
				SwordLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				SwordLore.add(rsrs);
				SwordMeta.setLore(SwordLore);
				Sword.setItemMeta(SwordMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Sword);
				}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.STONE_AXE){
					//Fire Damage
					if(Stat == 5){
						AxeLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.BLUE + "+" + plugin.rand(2, 10));
						AxeMeta.setDisplayName(AxeMeta.getDisplayName() + " of �9Ice");
						AxeMeta.setLore(AxeLore);
						Axe.setItemMeta(AxeMeta);
						//Soul Siphon
					}else if(Stat == 4){
						AxeLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(5, 14));
						AxeMeta.setDisplayName(AxeMeta.getDisplayName() + " of �2Poison");
						AxeMeta.setLore(AxeLore);
						Axe.setItemMeta(AxeMeta);
						//Soul Siphon
					}
					String rsrs = AxeLore.get(2);
					AxeLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
					AxeLore.add(rsrs);
					AxeMeta.setLore(AxeLore);
					Axe.setItemMeta(AxeMeta);
					event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Axe);	
				}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.STONE_SPADE){
					//Fire Damage
					if(Stat == 5){
						SpearLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.BLUE+ "+" + plugin.rand(2, 10));
						SpearMeta.setDisplayName(SpearMeta.getDisplayName() + " of �9Ice");
						SpearMeta.setLore(SpearLore);
						Spear.setItemMeta(SpearMeta);
						//Soul Siphon
					}else if(Stat == 4){
						SpearLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(5, 14));
						SpearMeta.setDisplayName(SpearMeta.getDisplayName() + " of �2Poison");
						SpearMeta.setLore(SpearLore);
						Spear.setItemMeta(SpearMeta);
						//Soul Siphon
					}else if(Stat == 3){
						SpearLore.set(1, ChatColor.RED + "Fire DMG " + ChatColor.DARK_RED + "+" + plugin.rand(5, 17));
						SpearMeta.setDisplayName(SpearMeta.getDisplayName() + " of �4Fire");
						SpearMeta.setLore(SpearLore);
						Spear.setItemMeta(SpearMeta);
						//Ice Damage
					}
					String rsrs = SpearLore.get(2);
					SpearLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
					SpearLore.add(rsrs);
					SpearMeta.setLore(SpearLore);
					Spear.setItemMeta(SpearMeta);
					event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Spear);	
				}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.BOW){
					//Fire Damage
					if(Stat == 5){
						BowLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.BLUE + "+" + plugin.rand(2, 10));
						BowMeta.setDisplayName(BowMeta.getDisplayName() + "of �9Ice");
						BowMeta.setLore(BowLore);
						Bow.setItemMeta(BowMeta);
						//Soul Siphon
					}else if(Stat == 4){
						BowLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(5, 14));
						BowMeta.setDisplayName(BowMeta.getDisplayName() + " of �2Poison");
						BowMeta.setLore(BowLore);
						Bow.setItemMeta(BowMeta);
						//Soul Siphon
					}
					String rsrs = BowLore.get(2);
					BowLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
					BowLore.add(rsrs);
					BowMeta.setLore(BowLore);
					Bow.setItemMeta(BowMeta);
					event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Bow);	
				}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.STONE_HOE){
					StaveLore.add("");
						StaveLore.add("�4Unbound");
					String rsrs = StaveLore.get(2);
					StaveLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
					StaveLore.add(rsrs);
					StaveMeta.setLore(StaveLore);
					Stave.setItemMeta(StaveMeta);
					event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Stave);	
				}
			}
		}
			}
		}
	}
	}
	static int Stop = 0;
	public static void StopF(){
		Bukkit.getServer().getScheduler().cancelTask(Stop);
	}
}
