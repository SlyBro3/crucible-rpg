package com.me.sly.CrucibleEmpire.MobSpawns;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.me.sly.CrucibleEmpire.Main.ceMain;
import com.me.sly.resources.MyMetadata;

public class GradeBMobSpawns implements Listener {
	public static ceMain plugin;
    public GradeBMobSpawns(ceMain instance) {
        plugin = instance;
    }
    public static Object s(){
    	return plugin;
    }
	static int Stop = 0;
	static int Stop2 = 0;
	@EventHandler
    public static void AllMobSpawns(final CreatureSpawnEvent event){
		event.getEntity().setMetadata("LastHit", new MyMetadata(plugin, false));
			/*final Location loc = event.getEntity().getLocation();
			if(!event.getEntity().hasMetadata("Custom")){
			Stop = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){
				public void run(){
					if(event.getEntity().isDead()){
						StopF(Stop);
					}else{
						if(!event.getEntity().hasMetadata("Custom")){
					if(loc.distance(event.getEntity().getLocation()) > 50){
						event.getEntity().remove();
						StopF(Stop);
						System.out.println("Killed");
					}
					}
					}
				}
			}, 150L, 150L);
			}*/
			if(!event.getEntity().hasMetadata("Custom")){
			Stop2 = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){
				public void run(){
					if(event.getEntity().isDead() == false){
					if(event.getEntity().getMetadata("LastHit").get(0).value().equals(true)){
						event.getEntity().setMetadata("LastHit", new MyMetadata(plugin, false));
					}else{
						for(Entity entity : event.getEntity().getNearbyEntities(5, 5, 5)){
							if(entity instanceof Player){
						    	boolean IsInSpawn = false;
						    	int distance = 40;
						    	if(plugin.wg.getRegionManager(Bukkit.getWorld("world")).hasRegion("Spawns")){
						            if (plugin.wg.getRegionManager(Bukkit.getWorld("world")).getRegion("Spawns").contains(entity.getLocation().getBlockX(), entity.getLocation().getBlockY(), event.getEntity().getLocation().getBlockZ())){
						             	IsInSpawn = true;
						            }
						    	}
						    	if(!(((Player) entity).getGameMode() == GameMode.CREATIVE)){
						    	if(!IsInSpawn){
						    		if(distance > event.getEntity().getLocation().distance(entity.getLocation())){
						    			distance = (int) event.getEntity().getLocation().distance(entity.getLocation());
								if(event.getEntity().getLocation().getY() < entity.getLocation().getY() ){
								event.getEntity().teleport(entity.getLocation());
								}

						    	}
						    	}
						    	}
							}
						}
					}
					}else{
						StopF(Stop2);
					}
				}
			}, 100L, 100L);
			}
		}
	@EventHandler
	public void NormalDrops(final EntityDeathEvent event){
		int ChestHP = plugin.rand(1, 101);
		int LegHP = plugin.rand(1, 101);
		int BootHP = plugin.rand(1, 101);
		int HelmHP = plugin.rand(1, 101);
		int SwordDMG = plugin.rand(1, 101);
		int AxeDMG = plugin.rand(1, 101);
		int BowDMG = plugin.rand(1, 101);
		int SpearDMG = plugin.rand(1, 101);
		ItemStack Boots = new ItemStack(Material.GOLD_BOOTS);
		ItemStack Leggings = new ItemStack(Material.GOLD_LEGGINGS);
		ItemStack Chestplate = new ItemStack(Material.GOLD_CHESTPLATE);
		ItemStack Helmet = new ItemStack(Material.GOLD_HELMET);
		ItemStack Sword = new ItemStack(Material.GOLD_SWORD );
		ItemStack Axe = new ItemStack(Material.GOLD_AXE);
		ItemStack Bow = new ItemStack(Material.BOW);
		ItemStack Spear = new ItemStack(Material.GOLD_SPADE);
		ItemStack Stave = new ItemStack(Material.GOLD_HOE);
		ItemMeta BootsMeta = Boots.getItemMeta();
		ItemMeta LeggingsMeta = Leggings.getItemMeta();
		ItemMeta ChestplateMeta = Chestplate.getItemMeta();
		ItemMeta HelmetMeta = Helmet.getItemMeta();
		ItemMeta SwordMeta = Sword.getItemMeta();
		ItemMeta AxeMeta = Axe.getItemMeta();
		ItemMeta BowMeta = Bow.getItemMeta();
		ItemMeta SpearMeta = Spear.getItemMeta();
		ItemMeta StaveMeta = Stave.getItemMeta();
		ArrayList<String> BootsLore = new ArrayList<String>();
		ArrayList<String> LeggingsLore = new ArrayList<String>();
		ArrayList<String> ChestplateLore = new ArrayList<String>();
		ArrayList<String> HelmetLore = new ArrayList<String>();
		ArrayList<String> SwordLore = new ArrayList<String>();
		ArrayList<String> AxeLore = new ArrayList<String>();
		ArrayList<String> BowLore = new ArrayList<String>();
		ArrayList<String> SpearLore = new ArrayList<String>();
		ArrayList<String> StaveLore = new ArrayList<String>();
		BootsMeta.setDisplayName(ChatColor.YELLOW + "Promising Boots");
		LeggingsMeta.setDisplayName(ChatColor.YELLOW + "Promising Leggings");
		ChestplateMeta.setDisplayName(ChatColor.YELLOW + "Promising Chestplate");
		HelmetMeta.setDisplayName(ChatColor.YELLOW + "Promising Helmet");
		SwordMeta.setDisplayName(ChatColor.YELLOW + "Promising Long Sword");
		AxeMeta.setDisplayName(ChatColor.YELLOW + "Promising Axe");
		BowMeta.setDisplayName(ChatColor.YELLOW + "Promising Compound Bow");
		SpearMeta.setDisplayName(ChatColor.YELLOW + "Promising Trident");
		StaveMeta.setDisplayName(ChatColor.YELLOW + "Promising Stave");
		if(BootHP < 60){
		BootsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(400, 900));
		BootsLore.add("");
		BootsLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(BootHP > 60 && BootHP < 91){
		BootsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(900, 1150));
		BootsLore.add("");
		BootsLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(BootHP > 90 && BootHP < 96){
		BootsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(1150, 1300));
		BootsLore.add("");
		BootsLore.add(ChatColor.GOLD + "�oRare");
		}else{
		BootsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(1300, 1500));
		BootsLore.add("");
		BootsLore.add("�b�oLegendary");
		}
		if(LegHP < 60){
		LeggingsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(1200, 1400));
		LeggingsLore.add("");
		LeggingsLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(LegHP > 60 && LegHP < 91){
		LeggingsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(1400, 1700));
		LeggingsLore.add("");
		LeggingsLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(LegHP > 90 && LegHP < 96){
		LeggingsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(1700, 2100));
		LeggingsLore.add("");
		LeggingsLore.add(ChatColor.GOLD + "�oRare");
		}else{
		LeggingsLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(2100, 2300));
		LeggingsLore.add("");
		LeggingsLore.add("�b�oLegendary");
		}
		if(ChestHP < 60){
		ChestplateLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(1200, 1400));
		ChestplateLore.add("");
		ChestplateLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(ChestHP > 60 && ChestHP < 91){
		ChestplateLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(1400, 1700));
		ChestplateLore.add("");
		ChestplateLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(ChestHP > 90 && ChestHP < 96){
		ChestplateLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(1700, 2100));
		ChestplateLore.add("");
		ChestplateLore.add(ChatColor.GOLD + "�oRare");
		}else{
		ChestplateLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(2100, 2300));
		ChestplateLore.add("");
		ChestplateLore.add("�b�oLegendary");
		}
		if(HelmHP < 60){
		HelmetLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(400, 900));
		HelmetLore.add("");
		HelmetLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(HelmHP > 60 && HelmHP < 91){
		HelmetLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(900, 1150));
		HelmetLore.add("");
		HelmetLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(HelmHP > 90 && HelmHP < 96){
		HelmetLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(1150, 1300));
		HelmetLore.add("");
		HelmetLore.add(ChatColor.GOLD + "�oRare");
		}else{
		HelmetLore.add(ChatColor.DARK_AQUA + "HP: " + plugin.rand(1300, 1500));
		HelmetLore.add("");
		HelmetLore.add("�b�oLegendary");
		}
		if(SwordDMG < 60){
			SwordLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(40, 60) + " - " + plugin.rand(70, 90) + " Damage");
			SwordLore.add("");
			SwordLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(SwordDMG > 60 && SwordDMG < 91){
			SwordLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(70, 90) + " - " + plugin.rand(90, 120) + " Damage");
			SwordLore.add("");
			SwordLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(SwordDMG > 90 && SwordDMG < 96){
			SwordLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(90, 110) + " - " + plugin.rand(110, 140) + " Damage");
			SwordLore.add("");
			SwordLore.add(ChatColor.GOLD + "�oRare");
		}else{
			SwordLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(110, 150) + " - " + plugin.rand(150, 170) + " Damage");
			SwordLore.add("");
			SwordLore.add("�b�oLegendary");
		}
		if(AxeDMG < 60){
			AxeLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(60, 80) + " - " + plugin.rand(80, 120) + " Damage");
			AxeLore.add("");
			AxeLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(AxeDMG > 60 && AxeDMG < 91){
			AxeLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(100, 130) + " - " + plugin.rand(130, 140) + " Damage");
			AxeLore.add("");
			AxeLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(AxeDMG > 90 && AxeDMG < 96){
			AxeLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(110, 140) + " - " + plugin.rand(140, 170) + " Damage");
			AxeLore.add("");
			AxeLore.add(ChatColor.GOLD + "�oRare");
		}else{
			AxeLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(140, 180) + " - " + plugin.rand(180, 200) + " Damage");
			AxeLore.add("");
			AxeLore.add("�b�oLegendary");
		}
		if(BowDMG < 60){
			BowLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(55, 65) + " - " + plugin.rand(65, 75) + " Damage");
			BowLore.add("");
			BowLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(BowDMG > 60 && BowDMG < 91){
			BowLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(65, 70) + " - " + plugin.rand(70, 90) + " Damage");
			BowLore.add("");
			BowLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(BowDMG > 90 && BowDMG < 96){
			BowLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(70, 90) + " - " + plugin.rand(90, 110) + " Damage");
			BowLore.add("");
			BowLore.add(ChatColor.GOLD + "�oRare");
		}else{
			BowLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(100, 130) + " - " + plugin.rand(130, 160) + " Damage");
			BowLore.add("");
			BowLore.add("�b�oLegendary");
		}
		if(SpearDMG < 60){
			SpearLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(30, 40) + " - " + plugin.rand(40, 55) + " Damage");
			SpearLore.add("");
			SpearLore.add(ChatColor.LIGHT_PURPLE + "�oCommon");
		}else if(SpearDMG > 60 && SpearDMG < 91){
			SpearLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(45, 55) + " - " + plugin.rand(55, 65) + " Damage");
			SpearLore.add("");
			SpearLore.add(ChatColor.GREEN + "�oUncommon");
		}else if(SpearDMG > 90 && SpearDMG < 96){
			SpearLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(55, 65) + " - " + plugin.rand(65, 70) + " Damage");
			SpearLore.add("");
			SpearLore.add(ChatColor.GOLD + "�oRare");
		}else{
			SpearLore.add(ChatColor.DARK_AQUA + "" + plugin.rand(70, 83) + " - " + plugin.rand(83, 95) + " Damage");
			SpearLore.add("");
			SpearLore.add("�b�oLegendary");
		}
		BootsMeta.setLore(BootsLore);
		SwordMeta.setLore(SwordLore);
		ChestplateMeta.setLore(ChestplateLore);
		LeggingsMeta.setLore(LeggingsLore);
		HelmetMeta.setLore(HelmetLore);
		AxeMeta.setLore(AxeLore);
		BowMeta.setLore(BowLore);
		SpearMeta.setLore(SpearLore);
		StaveMeta.setLore(StaveLore);
		Boots.setItemMeta(BootsMeta);
		Sword.setItemMeta(SwordMeta);
		Axe.setItemMeta(AxeMeta);
		Chestplate.setItemMeta(ChestplateMeta);
		Leggings.setItemMeta(LeggingsMeta);
		Helmet.setItemMeta(HelmetMeta);
		Spear.setItemMeta(SpearMeta);
		Bow.setItemMeta(BowMeta);
		Stave.setItemMeta(StaveMeta);
		event.setDroppedExp(0);
		if(event.getEntity().getKiller() instanceof Player){
		if(!(event.getEntity() instanceof Player)){
			if(event.getEntity().isCustomNameVisible() == true){
		if(((String)event.getEntity().getMetadata("Grade").get(0).value()).contains("B") && !event.getEntity().getCustomName().contains("Bitch")){
			int Drop = plugin.rand(1, 37);
			int Stat = plugin.rand(1, 12);
			int Gold = plugin.rand(1, 21);
			int Block = plugin.rand(1, 26);
			int HPRegen = plugin.rand(1, 9);
			ItemStack GoldDrop = new ItemStack(Material.DOUBLE_PLANT, plugin.rand(30, 70));
			ItemMeta GoldMeta = GoldDrop.getItemMeta();
			ArrayList<String> GoldLore = new ArrayList<String>();
			GoldMeta.setDisplayName("�6�lGold Coin");
			GoldLore.add("�eUsed as currency in");
			GoldLore.add("�eall of Tranomia");
			GoldMeta.setLore(GoldLore);
			GoldDrop.setItemMeta(GoldMeta);
		event.getDrops().clear();
		if(Gold <= 3){	
		}else{
		event.getDrops().add(GoldDrop);
		}
		if(event.getEntity().getEquipment().getItemInHand().getType() == Material.BOW){
			ItemStack Arrow = new ItemStack(Material.ARROW);
			ItemMeta ArrowMeta = Arrow.getItemMeta();
			List<String> ArrowLore = new ArrayList<String>();
			ArrowMeta.setDisplayName("�ePromising Arrow");
			ArrowLore.add("�eThis arrow is shot by");
			ArrowLore.add("�ethe �ePromising Compound Bow"); 
			ArrowLore.add("�9Grade B Arrow");
			ArrowMeta.setLore(ArrowLore);
			Arrow.setItemMeta(ArrowMeta);
				Arrow.setAmount(plugin.rand(1, 6));
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Arrow);
		}
				event.setDroppedExp(0);
				if(Drop == 6){ // HELMET
				if(Block == 1){
					HelmetLore.add(HelmetLore.get(2));
					HelmetLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 15));
					HelmetMeta.setLore(HelmetLore);
					Helmet.setItemMeta(HelmetMeta);
				}
				if(HPRegen == 1){
					HelmetLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(50, 100) + "/s");
					HelmetMeta.setLore(HelmetLore);
					Helmet.setItemMeta(HelmetMeta);
				}else if(HPRegen == 2){
					HelmetLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(25, 45) + "/s");
					HelmetMeta.setLore(HelmetLore);
					Helmet.setItemMeta(HelmetMeta);
				}
				String rsrs = HelmetLore.get(2);
				HelmetLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				HelmetLore.add(rsrs);
				HelmetMeta.setLore(HelmetLore);
				Helmet.setItemMeta(HelmetMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Helmet);
			}else if(Drop == 12){ // CHESTPLATE
				if(Block == 1){
					ChestplateLore.add(ChestplateLore.get(2));
					ChestplateLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 15));
					ChestplateMeta.setLore(ChestplateLore);
					Chestplate.setItemMeta(ChestplateMeta);
				}
				if(HPRegen == 1){
					ChestplateLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(75, 125) + "/s");
					ChestplateMeta.setLore(ChestplateLore);
					Chestplate.setItemMeta(ChestplateMeta);
				}else if(HPRegen == 2){
					ChestplateLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(25, 45) + "/s");
					ChestplateMeta.setLore(ChestplateLore);
					Chestplate.setItemMeta(ChestplateMeta);
				}
				String rsrs = ChestplateLore.get(2);
				ChestplateLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				ChestplateLore.add(rsrs);
				ChestplateMeta.setLore(ChestplateLore);
				Chestplate.setItemMeta(ChestplateMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Chestplate);
			}else if(Drop == 14){ // LEGS
				if(Block == 1){
					LeggingsLore.add(LeggingsLore.get(2));
					LeggingsLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 15));
					LeggingsMeta.setLore(LeggingsLore);
					Leggings.setItemMeta(LeggingsMeta);
				}
				if(HPRegen == 1){
					LeggingsLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(75, 125) + "/s");
					LeggingsMeta.setLore(LeggingsLore);
					Leggings.setItemMeta(LeggingsMeta);
				}else if(HPRegen == 2){
					LeggingsLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(25, 45) + "/s");
					LeggingsMeta.setLore(LeggingsLore);
					Leggings.setItemMeta(LeggingsMeta);
				}
				String rsrs = LeggingsLore.get(2);
				LeggingsLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				LeggingsLore.add(rsrs);
				LeggingsMeta.setLore(LeggingsLore);
				Leggings.setItemMeta(LeggingsMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Leggings);
			}else if(Drop == 11){ // BOOTS
				if(Block == 1){
					BootsLore.add(BootsLore.get(2));
					BootsLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 15));
					BootsMeta.setLore(BootsLore);
					Boots.setItemMeta(BootsMeta);
				}
				if(HPRegen == 1){
					BootsLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(50, 100) + "/s");
					BootsMeta.setLore(BootsLore);
					Boots.setItemMeta(BootsMeta);
				}else if(HPRegen == 2){
					BootsLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(25, 45) + "/s");
					BootsMeta.setLore(BootsLore);
					Boots.setItemMeta(BootsMeta);
				}
				String rsrs = BootsLore.get(2);
				BootsLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				BootsLore.add(rsrs);
				BootsMeta.setLore(BootsLore);
				Boots.setItemMeta(BootsMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Boots);
			}else if(Drop == 10 || Drop == 1 || Drop == 2){ // SWORD
				if(event.getEntity().getEquipment().getItemInHand().getType() == Material.GOLD_SWORD){
				//Fire Damage
				if(Stat == 1){
					SwordLore.set(1, ChatColor.RED + "Fire DMG " + ChatColor.DARK_RED + "+" + plugin.rand(5, 17));
					SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �4Fire");
					SwordMeta.setLore(SwordLore);
					Sword.setItemMeta(SwordMeta);
					//Ice Damage
				}else if(Stat == 2){
					SwordLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.DARK_BLUE + "+" + plugin.rand(5, 17));
					SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �1Ice");
					SwordMeta.setLore(SwordLore);
					Sword.setItemMeta(SwordMeta);
					//Soul Siphon
				}else if(Stat == 3){
					SwordLore.set(1, ChatColor.YELLOW + "Soul Siphon " + ChatColor.GOLD + "" + plugin.rand(1, 4) + "%");
					SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �6Vampirism");
					SwordMeta.setLore(SwordLore);
					Sword.setItemMeta(SwordMeta);
				}else if(Stat == 4){
					SwordLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(8, 20));
					SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �2Poison");
					SwordMeta.setLore(SwordLore);
					Sword.setItemMeta(SwordMeta);
					//Soul Siphon
				}	
				String rsrs = SwordLore.get(2);
				SwordLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				SwordLore.add(rsrs);
				SwordMeta.setLore(SwordLore);
				Sword.setItemMeta(SwordMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Sword);
				}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.GOLD_AXE){
					//Fire Damage
					if(Stat == 5){
						AxeLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.DARK_BLUE + "+" + plugin.rand(2, 10));
						AxeMeta.setDisplayName(AxeMeta.getDisplayName() + " of �1Ice");
						AxeMeta.setLore(AxeLore);
						Axe.setItemMeta(AxeMeta);
						//Soul Siphon
					}else if(Stat == 4){
						AxeLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(5, 14));
						AxeMeta.setDisplayName(AxeMeta.getDisplayName() + " of �2Poison");
						AxeMeta.setLore(AxeLore);
						Axe.setItemMeta(AxeMeta);
						//Soul Siphon
					}
					String rsrs = AxeLore.get(2);
					AxeLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
					AxeLore.add(rsrs);
					AxeMeta.setLore(AxeLore);
					Axe.setItemMeta(AxeMeta);
					event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Axe);	
				}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.GOLD_SPADE){
					//Fire Damage
					if(Stat == 5){
						SpearLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.DARK_BLUE+ "+" + plugin.rand(2, 10));
						SpearMeta.setDisplayName(SpearMeta.getDisplayName() + " of �2Ice");
						SpearMeta.setLore(SpearLore);
						Spear.setItemMeta(SpearMeta);
						//Soul Siphon
					}else if(Stat == 4){
						SpearLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(5, 14));
						SpearMeta.setDisplayName(SpearMeta.getDisplayName() + " of �2Poison");
						SpearMeta.setLore(SpearLore);
						Spear.setItemMeta(SpearMeta);
						//Soul Siphon
					}else if(Stat == 3){
						SpearLore.set(1, ChatColor.RED + "Fire DMG " + ChatColor.DARK_RED + "+" + plugin.rand(5, 17));
						SpearMeta.setDisplayName(SpearMeta.getDisplayName() + " of �4Fire");
						SpearMeta.setLore(SpearLore);
						Spear.setItemMeta(SpearMeta);
						//Ice Damage
					}
					String rsrs = SpearLore.get(2);
					SpearLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
					SpearLore.add(rsrs);
					SpearMeta.setLore(SpearLore);
					Spear.setItemMeta(SpearMeta);
					event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Spear);	
				}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.BOW){
					//Fire Damage
					if(Stat == 5){
						BowLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.DARK_BLUE + "+" + plugin.rand(2, 10));
						BowMeta.setDisplayName(BowMeta.getDisplayName() + "of �1Ice");
						BowMeta.setLore(BowLore);
						Bow.setItemMeta(BowMeta);
						//Soul Siphon
					}else if(Stat == 4){
						BowLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(5, 14));
						BowMeta.setDisplayName(BowMeta.getDisplayName() + " of �2Poison");
						BowMeta.setLore(BowLore);
						Bow.setItemMeta(BowMeta);
						//Soul Siphon
					}
					String rsrs = BowLore.get(2);
					BowLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
					BowLore.add(rsrs);
					BowMeta.setLore(BowLore);
					Bow.setItemMeta(BowMeta);
					event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Bow);	
				}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.GOLD_HOE){
						StaveLore.set(1, "�4Unbound");
					String rsrs = StaveLore.get(2);
					StaveLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
					StaveLore.add(rsrs);
					StaveMeta.setLore(StaveLore);
					Stave.setItemMeta(StaveMeta);
					event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Stave);	
				}
			}
			}else if(event.getEntity().getCustomName().contains("Ambient King")){
				int Drop = plugin.rand(1, 11);
				int Stat = plugin.rand(1, 12);
				int Gold = plugin.rand(1, 21);
				int Block = plugin.rand(1, 26);
				int HPRegen = plugin.rand(1, 6);
				ItemStack GoldDrop = new ItemStack(Material.GOLD_INGOT, plugin.rand(10, 20));
				ItemMeta GoldMeta = GoldDrop.getItemMeta();
				ArrayList<String> GoldLore = new ArrayList<String>();
				GoldMeta.setDisplayName("�6�lGold Bar");
				GoldLore.add("�7�nWorth 10 nuggets each");
				GoldMeta.setLore(GoldLore);
				GoldDrop.setItemMeta(GoldMeta);
			event.getDrops().clear();
			if(Gold <= 3){	
			}else{
			event.getDrops().add(GoldDrop);
			}
			event.setDroppedExp(0);
			if(Drop == 6){ // HELMET
			if(Block == 1){
				HelmetLore.add(HelmetLore.get(2));
				HelmetLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 15));
				HelmetMeta.setLore(HelmetLore);
				Helmet.setItemMeta(HelmetMeta);
			}
			if(HPRegen == 1){
				HelmetLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(50, 100) + "/s");
				HelmetMeta.setLore(HelmetLore);
				Helmet.setItemMeta(HelmetMeta);
			}else if(HPRegen == 2){
				HelmetLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(25, 45) + "/s");
				HelmetMeta.setLore(HelmetLore);
				Helmet.setItemMeta(HelmetMeta);
			}
			String rsrs = HelmetLore.get(2);
			HelmetLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
			HelmetLore.add(rsrs);
			HelmetMeta.setLore(HelmetLore);
			Helmet.setItemMeta(HelmetMeta);
			event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Helmet);
		}else if(Drop == 12){ // CHESTPLATE
			if(Block == 1){
				ChestplateLore.add(ChestplateLore.get(2));
				ChestplateLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 15));
				ChestplateMeta.setLore(ChestplateLore);
				Chestplate.setItemMeta(ChestplateMeta);
			}
			if(HPRegen == 1){
				ChestplateLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(75, 125) + "/s");
				ChestplateMeta.setLore(ChestplateLore);
				Chestplate.setItemMeta(ChestplateMeta);
			}else if(HPRegen == 2){
				ChestplateLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(25, 45) + "/s");
				ChestplateMeta.setLore(ChestplateLore);
				Chestplate.setItemMeta(ChestplateMeta);
			}
			String rsrs = ChestplateLore.get(2);
			ChestplateLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
			ChestplateLore.add(rsrs);
			ChestplateMeta.setLore(ChestplateLore);
			Chestplate.setItemMeta(ChestplateMeta);
			event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Chestplate);
		}else if(Drop == 14){ // LEGS
			if(Block == 1){
				LeggingsLore.add(LeggingsLore.get(2));
				LeggingsLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 15));
				LeggingsMeta.setLore(LeggingsLore);
				Leggings.setItemMeta(LeggingsMeta);
			}
			if(HPRegen == 1){
				LeggingsLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(75, 125) + "/s");
				LeggingsMeta.setLore(LeggingsLore);
				Leggings.setItemMeta(LeggingsMeta);
			}else if(HPRegen == 2){
				LeggingsLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(25, 45) + "/s");
				LeggingsMeta.setLore(LeggingsLore);
				Leggings.setItemMeta(LeggingsMeta);
			}
			String rsrs = LeggingsLore.get(2);
			LeggingsLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
			LeggingsLore.add(rsrs);
			LeggingsMeta.setLore(LeggingsLore);
			Leggings.setItemMeta(LeggingsMeta);
			event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Leggings);
		}else if(Drop == 11){ // BOOTS
			if(Block == 1){
				BootsLore.add(BootsLore.get(2));
				BootsLore.set(2, ChatColor.DARK_GREEN + "Agility: " + plugin.rand(1, 15));
				BootsMeta.setLore(BootsLore);
				Boots.setItemMeta(BootsMeta);
			}
			if(HPRegen == 1){
				BootsLore.set(1, ChatColor.RED + "Health Regen +" + plugin.rand(50, 100) + "/s");
				BootsMeta.setLore(BootsLore);
				Boots.setItemMeta(BootsMeta);
			}else if(HPRegen == 2){
				BootsLore.set(1, ChatColor.DARK_PURPLE + "Mana Regen +" + plugin.rand(25, 45) + "/s");
				BootsMeta.setLore(BootsLore);
				Boots.setItemMeta(BootsMeta);
			}
			String rsrs = BootsLore.get(2);
			BootsLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
			BootsLore.add(rsrs);
			BootsMeta.setLore(BootsLore);
			Boots.setItemMeta(BootsMeta);
			event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Boots);
		}else if(Drop == 10){ // SWORD
			if(event.getEntity().getEquipment().getItemInHand().getType() == Material.GOLD_SWORD){
			//Fire Damage
			if(Stat == 1){
				SwordLore.set(1, ChatColor.RED + "Fire DMG " + ChatColor.DARK_RED + "+" + plugin.rand(5, 17));
				SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �4Fire");
				SwordMeta.setLore(SwordLore);
				Sword.setItemMeta(SwordMeta);
				//Ice Damage
			}else if(Stat == 2){
				SwordLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.DARK_BLUE + "+" + plugin.rand(5, 17));
				SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �1Ice");
				SwordMeta.setLore(SwordLore);
				Sword.setItemMeta(SwordMeta);
				//Soul Siphon
			}else if(Stat == 3){
				SwordLore.set(1, ChatColor.YELLOW + "Soul Siphon " + ChatColor.GOLD + "" + plugin.rand(1, 4) + "%");
				SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �6Vampirism");
				SwordMeta.setLore(SwordLore);
				Sword.setItemMeta(SwordMeta);
			}else if(Stat == 4){
				SwordLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(8, 20));
				SwordMeta.setDisplayName(SwordMeta.getDisplayName() + " of �2Poison");
				SwordMeta.setLore(SwordLore);
				Sword.setItemMeta(SwordMeta);
				//Soul Siphon
			}	
			String rsrs = SwordLore.get(2);
			SwordLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
			SwordLore.add(rsrs);
			SwordMeta.setLore(SwordLore);
			Sword.setItemMeta(SwordMeta);
			event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Sword);
			}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.GOLD_AXE){
				//Fire Damage
				if(Stat == 5){
					AxeLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.DARK_BLUE + "+" + plugin.rand(2, 10));
					AxeMeta.setDisplayName(AxeMeta.getDisplayName() + " of �1Ice");
					AxeMeta.setLore(AxeLore);
					Axe.setItemMeta(AxeMeta);
					//Soul Siphon
				}else if(Stat == 4){
					AxeLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(5, 14));
					AxeMeta.setDisplayName(AxeMeta.getDisplayName() + " of �2Poison");
					AxeMeta.setLore(AxeLore);
					Axe.setItemMeta(AxeMeta);
					//Soul Siphon
				}
				String rsrs = AxeLore.get(2);
				AxeLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				AxeLore.add(rsrs);
				AxeMeta.setLore(AxeLore);
				Axe.setItemMeta(AxeMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Axe);	
			}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.GOLD_SPADE){
				//Fire Damage
				if(Stat == 5){
					SpearLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.DARK_BLUE+ "+" + plugin.rand(2, 10));
					SpearMeta.setDisplayName(SpearMeta.getDisplayName() + " of �2Ice");
					SpearMeta.setLore(SpearLore);
					Spear.setItemMeta(SpearMeta);
					//Soul Siphon
				}else if(Stat == 4){
					SpearLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(5, 14));
					SpearMeta.setDisplayName(SpearMeta.getDisplayName() + " of �2Poison");
					SpearMeta.setLore(SpearLore);
					Spear.setItemMeta(SpearMeta);
					//Soul Siphon
				}else if(Stat == 3){
					SpearLore.set(1, ChatColor.RED + "Fire DMG " + ChatColor.DARK_RED + "+" + plugin.rand(5, 17));
					SpearMeta.setDisplayName(SpearMeta.getDisplayName() + " of �4Fire");
					SpearMeta.setLore(SpearLore);
					Spear.setItemMeta(SpearMeta);
					//Ice Damage
				}
				String rsrs = SpearLore.get(2);
				SpearLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				SpearLore.add(rsrs);
				SpearMeta.setLore(SpearLore);
				Spear.setItemMeta(SpearMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Spear);	
			}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.BOW){
				//Fire Damage
				if(Stat == 5){
					BowLore.set(1, ChatColor.AQUA + "Ice DMG " + ChatColor.DARK_BLUE + "+" + plugin.rand(2, 10));
					BowMeta.setDisplayName(BowMeta.getDisplayName() + "of �1Ice");
					BowMeta.setLore(BowLore);
					Bow.setItemMeta(BowMeta);
					//Soul Siphon
				}else if(Stat == 4){
					BowLore.set(1, ChatColor.GREEN + "Poison DMG " + ChatColor.DARK_GREEN + "+" + plugin.rand(5, 14));
					BowMeta.setDisplayName(BowMeta.getDisplayName() + " of �2Poison");
					BowMeta.setLore(BowLore);
					Bow.setItemMeta(BowMeta);
					//Soul Siphon
				}
				String rsrs = BowLore.get(2);
				BowLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				BowLore.add(rsrs);
				BowMeta.setLore(BowLore);
				Bow.setItemMeta(BowMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Bow);	
			}else if(event.getEntity().getEquipment().getItemInHand().getType() == Material.GOLD_HOE){
					StaveLore.set(1, "�4Unbound");
				String rsrs = StaveLore.get(2);
				StaveLore.set(2, "�7Charge: " + "�9" + plugin.rand(500, 1001) + "�8�l / " + "�91000");
				StaveLore.add(rsrs);
				StaveMeta.setLore(StaveLore);
				Stave.setItemMeta(StaveMeta);
				event.getEntity().getWorld().dropItemNaturally(event.getEntity().getLocation(), Stave);	
			}
		}
			}
		}
		}
	}
	}
	public static void StopF(int Task){
		Bukkit.getServer().getScheduler().cancelTask(Task);
	}
}
