package com.me.sly.CrucibleEmpire.Main;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.me.sly.CrucibleEmpire.YMLfiles.SpawnerYML;
import com.me.sly.resources.BetterItems;
import com.me.sly.resources.MyMetadata;

public class MobSpawning implements Listener{
	
	public static ceMain plugin;
	public MobSpawning(ceMain s){
		plugin = s;
	}
	public HashMap<String, Integer> MobChoosing = new HashMap<String, Integer>();
	public HashMap<String, Integer> MobNaming = new HashMap<String, Integer>();
	public HashMap<String, Integer> MobGrading = new HashMap<String, Integer>();
	@SuppressWarnings("deprecation")
	public static void tryMobSpawn(){
		for(int i = 0; i < 10000; i++){
			if(plugin.getSpawnerConfig().contains("Spawner-" + i + ".W")){
			Location loc = new Location(Bukkit.getWorld("world"), plugin.getSpawnerConfig().getInt("Spawner-" + i + ".X"), plugin.getSpawnerConfig().getInt("Spawner-" + i + ".Y"), plugin.getSpawnerConfig().getInt("Spawner-" + i + ".Z"));
			int radius = 5;
			for(int x = loc.getBlockX() - radius; x <= loc.getBlockX() + radius; x++){
			    for(int y = loc.getBlockY() - radius; y <= loc.getBlockY() + radius; y++){
			        for(int z = loc.getBlockZ() - radius; z <= loc.getBlockZ() + radius; z++){
			            if(Bukkit.getWorld("world").getBlockAt(x, y, z).getType() == Material.AIR && Bukkit.getWorld("world").getBlockAt(x, y - 1, z).getType() == Material.AIR){
				            	if(plugin.rand(0, 42) == 7){
				            		if(Bukkit.getWorld("world").getChunkAt(loc).isLoaded()){
				            			ArrayList<Entity> Entities = (ArrayList<Entity>) Bukkit.getWorld("world").getEntities();
				            			ArrayList<Entity> Entities2 =  new ArrayList<Entity>();
				            			for(Entity E : Entities){
				            				if((E instanceof Player)){
				            				}else{
				            					if(E instanceof LivingEntity){
				            				if(E.getLocation().distance(loc) < 20){
				            					if(E.hasMetadata("ID")){
				            					if((int) E.getMetadata("ID").get(0).value() == i){
				            					Entities2.add(E);
				            					}
				            				}
				            				}
				            					}
				            				}
				            			}
				            			if(Entities2.size() < 1){
				            		final LivingEntity Entity = (LivingEntity) Bukkit.getWorld("world").spawnEntity(Bukkit.getWorld("world").getBlockAt(x, y - 1, z).getLocation(), EntityType.fromName(plugin.getSpawnerConfig().getString("Spawner-" + i + ".ET")));
				            		Entity.setCustomNameVisible(true);
				            		Entity.setCustomName(plugin.getSpawnerConfig().getString("Spawner-" + i + ".N"));
				            		Entity.setMetadata("Grade", new MyMetadata(plugin, plugin.getSpawnerConfig().getString("Spawner-" + i + ".G")));
				            		Entity.setMetadata("Name", new MyMetadata(plugin, plugin.getSpawnerConfig().getString("Spawner-" + i + ".N")));
				            		Entity.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100000, 0));
				            		if(plugin.getSpawnerConfig().getString("Spawner-" + i + ".G").contains("A")){
				            			final ItemStack GABoots = new ItemStack(Material.DIAMOND_BOOTS);
				            			final ItemStack GALeggings = new ItemStack(Material.DIAMOND_LEGGINGS);
				            			final ItemStack GAChestplate = new ItemStack(Material.DIAMOND_CHESTPLATE);
				            			final ItemStack GAHelmet = new ItemStack(Material.DIAMOND_HELMET);
				            			final ItemStack GASword = new ItemStack(Material.DIAMOND_SWORD );
				            			final ItemStack GAAxe = new ItemStack(Material.DIAMOND_AXE);
				            			final ItemStack GASpear = new ItemStack(Material.DIAMOND_SPADE);
				            			new BukkitRunnable(){
				            				public void run(){
				        				Entity.getEquipment().setBoots(GABoots);
				        				Entity.getEquipment().setLeggings(GALeggings);
				        				Entity.getEquipment().setChestplate(GAChestplate);
				        				Entity.getEquipment().setHelmet(GAHelmet);
				            				}
				            				}.runTaskLater(plugin, 1);

				        				if(Entity.getType() == EntityType.SKELETON){
					        				int r = plugin.rand(0, 11);
					        				if(r < 3){
						        				Entity.getEquipment().setItemInHand(GASword);
						        				}else if(r < 5){
						        				Entity.getEquipment().setItemInHand(GAAxe);
						        				}else if(r < 9){
						        				Entity.getEquipment().setItemInHand(GASpear);
						        				}else if(r < 11){
						        				Entity.getEquipment().setItemInHand(BetterItems.Lore(Material.BOW, 1, "�385 - 130 Damage"));
						        				}
				        				}else{
				        					int r = plugin.rand(0, 9);
				        					if(r < 3){
				        						Entity.getEquipment().setItemInHand(GASword);
				        						}else if(r < 5){
				        						Entity.getEquipment().setItemInHand(GAAxe);
				        						}else if(r < 9){
				        						Entity.getEquipment().setItemInHand(GASpear);
				        						}
				        				}
				        				Entity.getEquipment().setBootsDropChance(0F);
				        				Entity.getEquipment().setHelmetDropChance(0F);
				        				Entity.getEquipment().setChestplateDropChance(0F);
				        				Entity.getEquipment().setLeggingsDropChance(0F);
				        				Entity.getEquipment().setItemInHandDropChance(0F);
				        				Entity.setCanPickupItems(false);
				        				int s = plugin.rand(6000, 12000);
				        				Entity.setMaxHealth((double)s);
				        				Entity.setHealth((double)s);	
				            		}else if(plugin.getSpawnerConfig().getString("Spawner-" + i + ".G").contains("B")){
				            			final ItemStack GBBoots = new ItemStack(Material.GOLD_BOOTS);
				            			final ItemStack GBLeggings = new ItemStack(Material.GOLD_LEGGINGS);
				            			final ItemStack GBChestplate = new ItemStack(Material.GOLD_CHESTPLATE);
				            			final ItemStack GBHelmet = new ItemStack(Material.GOLD_HELMET);
				            			final ItemStack GBSword = new ItemStack(Material.GOLD_SWORD );
				            			final ItemStack GBAxe = new ItemStack(Material.GOLD_AXE);
				            			final ItemStack GBSpear = new ItemStack(Material.GOLD_SPADE);
				            			new BukkitRunnable(){
				            				public void run(){
				        				Entity.getEquipment().setBoots(GBBoots);
				        				Entity.getEquipment().setLeggings(GBLeggings);
				        				Entity.getEquipment().setChestplate(GBChestplate);
				        				Entity.getEquipment().setHelmet(GBHelmet);
				            				}
				            				}.runTaskLater(plugin, 1);
					        				if(Entity.getType() == EntityType.SKELETON){
						        				int r = plugin.rand(0, 11);
						        				if(r < 3){
							        				Entity.getEquipment().setItemInHand(GBSword);
							        				}else if(r < 5){
							        				Entity.getEquipment().setItemInHand(GBAxe);
							        				}else if(r < 9){
							        				Entity.getEquipment().setItemInHand(GBSpear);
							        				}else if(r < 11){
							        				Entity.getEquipment().setItemInHand(BetterItems.Lore(Material.BOW, 1, "�355 - 80 Damage"));
							        				}
					        				}else{
					        					int r = plugin.rand(0, 9);
					        					if(r < 3){
					        						Entity.getEquipment().setItemInHand(GBSword);
					        						}else if(r < 5){
					        						Entity.getEquipment().setItemInHand(GBAxe);
					        						}else if(r < 9){
					        						Entity.getEquipment().setItemInHand(GBSpear);
					        						}
					        				}
				        				Entity.getEquipment().setBootsDropChance(0F);
				        				Entity.getEquipment().setHelmetDropChance(0F);
				        				Entity.getEquipment().setChestplateDropChance(0F);
				        				Entity.getEquipment().setLeggingsDropChance(0F);
				        				Entity.getEquipment().setItemInHandDropChance(0F);
				        				Entity.setCanPickupItems(false);
				        				int s = plugin.rand(1500, 4000);
				        				Entity.setMaxHealth((double)s);
				        				Entity.setHealth((double)s);
				            		}else if(plugin.getSpawnerConfig().getString("Spawner-" + i + ".G").contains("C")){
				            			final ItemStack GCBoots = new ItemStack(Material.IRON_BOOTS);
				            			final ItemStack GCLeggings = new ItemStack(Material.IRON_LEGGINGS);
				            			final ItemStack GCChestplate = new ItemStack(Material.IRON_CHESTPLATE);
				            			final ItemStack GCHelmet = new ItemStack(Material.IRON_HELMET);
				            			final ItemStack GCSword = new ItemStack(Material.IRON_SWORD );
				            			final ItemStack GCAxe = new ItemStack(Material.IRON_AXE);
				            			final ItemStack GCSpear = new ItemStack(Material.IRON_SPADE);
				            			new BukkitRunnable(){
				            				public void run(){
				        				Entity.getEquipment().setBoots(GCBoots);
				        				Entity.getEquipment().setLeggings(GCLeggings);
				        				Entity.getEquipment().setChestplate(GCChestplate);
				        				Entity.getEquipment().setHelmet(GCHelmet);
				            				}
				            				}.runTaskLater(plugin, 1);
					        				if(Entity.getType() == EntityType.SKELETON){
						        				int r = plugin.rand(0, 11);
						        				if(r < 3){
							        				Entity.getEquipment().setItemInHand(GCSword);
							        				}else if(r < 5){
							        				Entity.getEquipment().setItemInHand(GCAxe);
							        				}else if(r < 9){
							        				Entity.getEquipment().setItemInHand(GCSpear);
							        				}else if(r < 11){
							        				Entity.getEquipment().setItemInHand(BetterItems.Lore(Material.BOW, 1, "�340 - 60 Damage"));
							        				}
					        				}else{
					        					int r = plugin.rand(0, 9);
					        					if(r < 3){
					        						Entity.getEquipment().setItemInHand(GCSword);
					        						}else if(r < 5){
					        						Entity.getEquipment().setItemInHand(GCAxe);
					        						}else if(r < 9){
					        						Entity.getEquipment().setItemInHand(GCSpear);
					        						}
					        				}
				        				Entity.getEquipment().setBootsDropChance(0F);
				        				Entity.getEquipment().setHelmetDropChance(0F);
				        				Entity.getEquipment().setChestplateDropChance(0F);
				        				Entity.getEquipment().setLeggingsDropChance(0F);
				        				Entity.getEquipment().setItemInHandDropChance(0F);
				        				Entity.setCanPickupItems(false);
				        				int s = plugin.rand(4000, 4800);
				        				Entity.setMaxHealth((double)s);
				        				Entity.setHealth((double)s);
				            		}else if(plugin.getSpawnerConfig().getString("Spawner-" + i + ".G").contains("D")){
				            			final ItemStack GDBoots = new ItemStack(Material.CHAINMAIL_BOOTS);
				            			final ItemStack GDLeggings = new ItemStack(Material.CHAINMAIL_LEGGINGS);
				            			final ItemStack GDChestplate = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
				            			final ItemStack GDHelmet = new ItemStack(Material.CHAINMAIL_HELMET);
				            			final ItemStack GDSword = new ItemStack(Material.STONE_SWORD );
				            			final ItemStack GDAxe = new ItemStack(Material.STONE_AXE);
				            			final ItemStack GDSpear = new ItemStack(Material.STONE_SPADE);
				            			new BukkitRunnable(){
				            				public void run(){
				        				Entity.getEquipment().setBoots(GDBoots);
				        				Entity.getEquipment().setLeggings(GDLeggings);
				        				Entity.getEquipment().setChestplate(GDChestplate);
				        				Entity.getEquipment().setHelmet(GDHelmet);
				            				}
				            				}.runTaskLater(plugin, 1);
					        				if(Entity.getType() == EntityType.SKELETON){
						        				int r = plugin.rand(0, 11);
						        				if(r < 3){
							        				Entity.getEquipment().setItemInHand(GDSword);
							        				}else if(r < 5){
							        				Entity.getEquipment().setItemInHand(GDAxe);
							        				}else if(r < 9){
							        				Entity.getEquipment().setItemInHand(GDSpear);
							        				}else if(r < 11){
							        				Entity.getEquipment().setItemInHand(BetterItems.Lore(Material.BOW, 1, "�315 - 40 Damage"));
							        				}
					        				}else{
					        					int r = plugin.rand(0, 9);
					        					if(r < 3){
					        						Entity.getEquipment().setItemInHand(GDSword);
					        						}else if(r < 5){
					        						Entity.getEquipment().setItemInHand(GDAxe);
					        						}else if(r < 9){
					        						Entity.getEquipment().setItemInHand(GDSpear);
					        						}
					        				}
				        				Entity.getEquipment().setBootsDropChance(0F);
				        				Entity.getEquipment().setHelmetDropChance(0F);
				        				Entity.getEquipment().setChestplateDropChance(0F);
				        				Entity.getEquipment().setLeggingsDropChance(0F);
				        				Entity.getEquipment().setItemInHandDropChance(0F);
				        				Entity.setCanPickupItems(false);
				        				int s = plugin.rand(4000, 4800);
				        				Entity.setMaxHealth((double)s);
				        				Entity.setHealth((double)s);
				            		}
				            		Entity.setMetadata("ID", new MyMetadata(plugin, i));
				            			}
				            			}
				            	}else{
				            	}
			            }
			        }
			    }
			}
		}	
		}
	}
	@SuppressWarnings("deprecation")
	public void sendBChange(final Player player){
		for(int i = 0; i < 1000; i++){
			Location loc = new Location(player.getWorld(), plugin.getSpawnerConfig().getInt("Spawner-" + i + ".X"), plugin.getSpawnerConfig().getInt("Spawner-" + i + ".Y"), plugin.getSpawnerConfig().getInt("Spawner-" + i + ".Z"));
			player.sendBlockChange(loc, Material.AIR, (byte) 1);
		}
	}
	@EventHandler
	public void SpawnerRegister(BlockPlaceEvent event){
		if(event.getPlayer().isOp()){
			if(event.getBlockPlaced().getType() == Material.MOB_SPAWNER){
				event.setCancelled(true);
				for(int i = 0; i < 10000; i++){
					if(!plugin.getSpawnerConfig().contains("Spawner-" + i + ".X")){
						plugin.getSpawnerConfig().set("Spawner-" + i + ".W", event.getBlockPlaced().getLocation().getWorld().getName());
						plugin.getSpawnerConfig().set("Spawner-" + i + ".X", event.getBlockPlaced().getLocation().getBlockX());
						plugin.getSpawnerConfig().set("Spawner-" + i + ".Y", event.getBlockPlaced().getLocation().getBlockY());
						plugin.getSpawnerConfig().set("Spawner-" + i + ".Z", event.getBlockPlaced().getLocation().getBlockZ());
						MobChoosing.put(event.getPlayer().getName(), i);
						plugin.saveConfig();
						plugin.reloadConfig();
						event.getPlayer().sendMessage(ChatColor.GREEN + "Spawner created!" + " Spawner ID: " + i);
						event.getPlayer().sendMessage("�dWhat type of mob would you like to spawn from this spawner?");
						break;
					}
				}
			}
			}
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void MobNaming(AsyncPlayerChatEvent event){
		if(MobChoosing.containsKey(event.getPlayer().getName())){
			event.setCancelled(true);
			if(EntityType.fromName(event.getMessage()) != null){
				plugin.getSpawnerConfig().set("Spawner-" + MobChoosing.get(event.getPlayer().getName()) + ".ET", event.getMessage().toUpperCase());
				MobNaming.put(event.getPlayer().getName(), MobChoosing.get(event.getPlayer().getName()));
				event.getPlayer().sendMessage("�bEntity �c" + event.getMessage().toUpperCase() + " �bselected!");
				event.getPlayer().sendMessage("�dWhat do you want the mobs name to be when it spawns?");
				MobChoosing.remove(event.getPlayer().getName());
				SpawnerYML.saveCustomConfig(); 
				SpawnerYML.reloadCustomConfig();
				
			}else{
			event.getPlayer().sendMessage("�4NOT �ca valid mob type! �bPlease enter a valid mob.");
			}
		}else if(MobNaming.containsKey(event.getPlayer().getName())){
			event.setCancelled(true);
			plugin.getSpawnerConfig().set("Spawner-" + MobNaming.get(event.getPlayer().getName()) + ".N", event.getMessage());
			MobGrading.put(event.getPlayer().getName(), MobNaming.get(event.getPlayer().getName()));
			event.getPlayer().sendMessage("�bName �c" + event.getMessage() + " �bselected!");
			event.getPlayer().sendMessage("�dWhat do you want the mobs grade to be when it spawns? �e[A, B, C, D]");
			MobNaming.remove(event.getPlayer().getName());
			SpawnerYML.saveCustomConfig(); 
			SpawnerYML.reloadCustomConfig();
			
		}else if(MobGrading.containsKey(event.getPlayer().getName())){
			event.setCancelled(true);
			plugin.getSpawnerConfig().set("Spawner-" + MobGrading.get(event.getPlayer().getName()) + ".G", event.getMessage().toUpperCase());
			event.getPlayer().sendMessage("�bGrade �c" + event.getMessage().toUpperCase() + " �bselected!");
			event.getPlayer().sendMessage("�aSpawner Registration Complete!");
			MobGrading.remove(event.getPlayer().getName());
			SpawnerYML.saveCustomConfig(); 
			SpawnerYML.reloadCustomConfig();
			
		}
	}
}
