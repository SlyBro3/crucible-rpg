package com.me.sly.CrucibleEmpire.Main;

import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.me.sly.resources.BetterItems;
import com.me.sly.resources.ParticleEffects;

public class Repair implements Listener{
	public static ceMain plugin;
    public Repair(ceMain instance) {
        plugin = instance;
    }
    @EventHandler(priority = EventPriority.MONITOR)
    public void DamageCharge(EntityDamageByEntityEvent event){
    	if(!event.isCancelled()){
    	if(event.getEntity() instanceof Player){
    		Player player = (Player) event.getEntity();
    		if(player.getInventory().getBoots() != null && player.getInventory().getBoots().hasItemMeta() && player.getInventory().getBoots().getItemMeta().hasLore()){
    			boolean hasCharge = false;
    			ItemMeta meta = player.getInventory().getBoots().getItemMeta();
    			List<String> Lore = meta.getLore();
    			int rand = plugin.rand(1, 5);
    			String dura = "";
    			for(String s : player.getInventory().getBoots().getItemMeta().getLore()){
    				if(s.contains("Charge")){
    					dura = s;
    					hasCharge = true;
    	    			break;
    				}
    			}
    			if(hasCharge){
    			if(rand == 2){
    				dura = dura.split("/")[0];
    				dura = dura.replaceAll("�7Charge: �9", "");
    				dura = dura.replaceAll("�8�l", "");
    				dura = dura.trim();
    				int Chargeb = Integer.parseInt(dura);
    				if(Chargeb == 0){
    					if(!Lore.contains("�4Needs Charge!")){
    				Lore.add("�4Needs Charge!");	
    				meta.setLore(Lore);
    				player.getInventory().getBoots().setItemMeta(meta);
    					}
    				}else{
        				Chargeb = Chargeb - 1;
    				}
    				dura = "�7Charge: �9" + Chargeb + " �8�l/ �91000";
    				for(String s : player.getInventory().getBoots().getItemMeta().getLore()){
    					if(s.contains("Charge")){
    	    				Lore.set(2, dura);	
    	    				meta.setLore(Lore);
    	    				player.getInventory().getBoots().setItemMeta(meta);
    	        			break;
    					}
    				}
    			}
    			}
    		}
    		if(player.getInventory().getLeggings() != null && player.getInventory().getLeggings().hasItemMeta() && player.getInventory().getLeggings().getItemMeta().hasLore()){
    			boolean hasCharge = false;
    			ItemMeta meta = player.getInventory().getLeggings().getItemMeta();
    			List<String> Lore = meta.getLore();
    			int rand = plugin.rand(1, 5);
    			String dura = "";
    			for(String s : player.getInventory().getLeggings().getItemMeta().getLore()){
    				if(s.contains("Charge")){
    					dura = s;
    					hasCharge = true;
    	    			break;
    				}
    			}
    			if(hasCharge){
    			if(rand == 2){
    				dura = dura.split("/")[0];
    				dura = dura.replaceAll("�7Charge: �9", "");
    				dura = dura.replaceAll("�8�l", "");
    				dura = dura.trim();
    				int Chargeb = Integer.parseInt(dura);
    				if(Chargeb == 0){
    					if(!Lore.contains("�4Needs Charge!")){
    				Lore.add("�4Needs Charge!");	
    				meta.setLore(Lore);
    				player.getInventory().getLeggings().setItemMeta(meta);
    				}
    				}else{
        				Chargeb = Chargeb - 1;
    				}
    				dura = "�7Charge: �9" + Chargeb + " �8�l/ �91000";
    				for(String s : player.getInventory().getLeggings().getItemMeta().getLore()){
    					if(s.contains("Charge")){
    	    				Lore.set(2, dura);	
    	    				meta.setLore(Lore);
    	    				player.getInventory().getLeggings().setItemMeta(meta);
    	        			break;
    					}
    				}
    			}
    		}
    		}
    		if(player.getInventory().getChestplate() != null && player.getInventory().getChestplate().hasItemMeta() && player.getInventory().getChestplate().getItemMeta().hasLore()){
    		boolean hasCharge = false;
    			ItemMeta meta = player.getInventory().getChestplate().getItemMeta();
    			List<String> Lore = meta.getLore();
    			int rand = plugin.rand(1, 5);
    			String dura = "";
    			for(String s : player.getInventory().getChestplate().getItemMeta().getLore()){
    				if(s.contains("Charge")){
    					dura = s;
    					hasCharge = true;
    	    			break;
    				}
    			}
    			if(hasCharge){
    			if(rand == 2){
    				dura = dura.split("/")[0];
    				dura = dura.replaceAll("�7Charge: �9", "");
    				dura = dura.replaceAll("�8�l", "");
    				dura = dura.trim();
    				int Chargeb = Integer.parseInt(dura);
    				if(Chargeb == 0){
    					if(!Lore.contains("�4Needs Charge!")){
    				Lore.add("�4Needs Charge!");	
    				meta.setLore(Lore);
    				player.getInventory().getChestplate().setItemMeta(meta);
    				}
    				}else{
        				Chargeb = Chargeb - 1;
    				}
    				dura = "�7Charge: �9" + Chargeb + " �8�l/ �91000";
    				for(String s : player.getInventory().getChestplate().getItemMeta().getLore()){
    					if(s.contains("Charge")){
    	    				Lore.set(2, dura);	
    	    				meta.setLore(Lore);
    	    				player.getInventory().getChestplate().setItemMeta(meta);
    	        			break;
    					}
    				}
    			}
    		}
    		}
    		if(player.getInventory().getHelmet() != null && player.getInventory().getHelmet().hasItemMeta() && player.getInventory().getHelmet().getItemMeta().hasLore()){
    			boolean hasCharge = false;
    			ItemMeta meta = player.getInventory().getHelmet().getItemMeta();
    			List<String> Lore = meta.getLore();
    			int rand = plugin.rand(1, 5);
    			String dura = "";
    			for(String s : player.getInventory().getHelmet ().getItemMeta().getLore()){
    				if(s.contains("Charge")){
    					dura = s;
    	    			hasCharge = true;
    	    			break;
    				}
    			}
    			if(hasCharge){
    			if(rand == 2){
    				dura = dura.split("/")[0];
    				dura = dura.replaceAll("�7Charge: �9", "");
    				dura = dura.replaceAll("�8�l", "");
    				dura = dura.trim();
    				int Chargeb = Integer.parseInt(dura);
    				if(Chargeb == 0){
    					if(!Lore.contains("�4Needs Charge!")){
    					Lore.add("�4Needs Charge!");	
    					meta.setLore(Lore);
    					player.getInventory().getHelmet().setItemMeta(meta);
    				}
    				}else{
        				Chargeb = Chargeb - 1;
    				}
    				dura = "�7Charge: �9" + Chargeb + " �8�l/ �91000";
    				for(String s : player.getInventory().getHelmet().getItemMeta().getLore()){
    					if(s.contains("Charge")){
    	    				Lore.set(2, dura);	
    	    				meta.setLore(Lore);
    	    				player.getInventory().getHelmet().setItemMeta(meta);
    	        			break;
    					}
    				}
    			}
    			}
    		}
    		}
    	if(event.getDamager() instanceof Player){
    		Player player = (Player) event.getDamager();
    		if(player.getInventory().getItemInHand() != null && player.getInventory().getItemInHand().hasItemMeta() && player.getInventory().getItemInHand().getItemMeta().hasLore() && player.getInventory().getItemInHand().getItemMeta().getLore().get(0).contains("Damage")){
    			boolean hasCharge = false;
    			ItemMeta meta = player.getInventory().getItemInHand().getItemMeta();
    			List<String> Lore = meta.getLore();
    			int rand = plugin.rand(1, 5);
    			String dura = "";
    			for(String s : player.getInventory().getItemInHand().getItemMeta().getLore()){
    				if(s.contains("Charge")){
    					dura = s;
    					hasCharge = true;
    	    			break;
    				}
    			}
    			if(hasCharge){
    			if(rand == 2){
    				dura = dura.split("/")[0];
    				dura = dura.replaceAll("�7Charge: �9", "");
    				dura = dura.replaceAll("�8�l", "");
    				dura = dura.trim();
    				int Chargeb = Integer.parseInt(dura);
    				if(Chargeb == 0){
    					if(!Lore.contains("�4Needs Charge!")){
    					Lore.add("�4Needs Charge!");	
    					meta.setLore(Lore);
    					player.getInventory().getItemInHand().setItemMeta(meta);
    					}
    				}else{
        				Chargeb = Chargeb - 1;
    				}
    				dura = "�7Charge: �9" + Chargeb + " �8�l/ �91000";
    				for(String s : player.getInventory().getItemInHand().getItemMeta().getLore()){
    					if(s.contains("Charge")){
    	    				Lore.set(2, dura);	
    	    				meta.setLore(Lore);
    	    				player.getInventory().getItemInHand().setItemMeta(meta);
    	        			break;
    					}
    				}
    			}
    		}	
    	}
    	}
    }
    }
    public int getCharge(ItemStack item){
    	if(item != null && item.hasItemMeta() && item.getItemMeta().hasLore()){
    		if(item.getItemMeta().getLore().get(0).contains("Damage") || item.getItemMeta().getLore().get(0).contains("HP: ")){
    	String dura = "";
    	for(String s : item.getItemMeta().getLore()){
    		if(s.contains("Charge")){
    			dura = s;
    			break;
    		}
    	}
    		dura = dura.split("/")[0];
    		dura = dura.replaceAll("�7Charge: �9", "");
    		dura = dura.replaceAll("�8�l", "");
    		dura = dura.trim();
    		int Durab = Integer.parseInt(dura);
    		return Durab;
    	}else{
    		return 0;
    	}
    	}else{
    		return 0;	
    	}
    }
    public Inventory RepairScreen;
	@EventHandler(priority = EventPriority.MONITOR)
    public void ANVILREPAIR(final PlayerInteractEntityEvent event){
    	Player player = event.getPlayer();
    	ItemStack item = event.getPlayer().getItemInHand();
    	ItemStack Repair = BetterItems.DisplayName(Material.EMERALD_BLOCK, 1, "�a�lCharge your item");
    	ItemStack NoRepair = BetterItems.DisplayName(Material.REDSTONE_BLOCK, 1, "�c�lDont charge your item");
    	if(event.getRightClicked() instanceof Player && ((Player)event.getRightClicked()).getDisplayName().contains("Artisan")){
    		if(item != null && item.hasItemMeta() && item.getItemMeta().hasLore()){
    		if(item.getItemMeta().getLore().get(0).contains("Damage") || item.getItemMeta().getLore().get(0).contains("HP: ")){
    			event.setCancelled(true);
    			if(getCharge(item) != 1000){
    		    	ItemStack Cost = BetterItems.DisplayName(Material.ANVIL, 1, "�e�lCost: ");
    			if(item.getItemMeta().getLore().get(0).contains("Damage") ){
    			String koo = player.getItemInHand().getItemMeta().getLore().get(0);
    			koo = koo.split("-")[1];
    			koo = koo.replaceAll(" Damage", "");
    			int ko = Integer.parseInt(koo.trim().replace("gold", ""));
    			RepairScreen = Bukkit.createInventory(player, 18, "Charge your item");
    	    	Cost = BetterItems.DisplayName(Material.ANVIL, 1, "�6�lCost: " + ((((ko * (1000 - getCharge(item)))/100)*2)/3));
    			}else if(item.getItemMeta().getLore().get(0).contains("HP: ")){
    			String koo = item.getItemMeta().getLore().get(0).split(":")[1].trim().replace("gold", "");
    			int ko = Integer.parseInt(koo);      
    			int rko = 1000 - getCharge(item);
    			System.out.println(getCharge(item));
    			ko = rko * ko;
    			ko = ko/100;
    			ko = ko/4;
    			ko = ko/3;
    			RepairScreen = Bukkit.createInventory(player, 18, "Charge your item");
    	    	Cost = BetterItems.DisplayName(Material.ANVIL, 1, "�6�lCost: " + ko);
    			}
    			player.openInventory(RepairScreen);
    			RepairScreen.setItem(4, Cost);
    			RepairScreen.setItem(11, NoRepair);
    			RepairScreen.setItem(13, item);
    			RepairScreen.setItem(15, Repair);
    			player.updateInventory();
    		}
    	}
    		}
    	}
    }
    @SuppressWarnings("deprecation")
	@EventHandler
    public void RepairClick(InventoryClickEvent event){
    	if(event.getInventory().getTitle().contains("Repair")){
    		event.setCancelled(true);
    		if(event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.REDSTONE_BLOCK){
    			//NO REPAIR
    			event.getWhoClicked().closeInventory();
    		}else if(event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.EMERALD_BLOCK){
    			if(plugin.invBalance((Player) event.getWhoClicked()) >= Integer.parseInt(event.getInventory().getItem(4).getItemMeta().getDisplayName().substring(10).trim())){
    				plugin.setBalance((Player) event.getWhoClicked(), plugin.invBalance((Player) event.getWhoClicked()) - Integer.parseInt(event.getInventory().getItem(4).getItemMeta().getDisplayName().substring(10).trim()));
    				ItemStack item = event.getInventory().getItem(13);	
    				event.getWhoClicked().getInventory().remove(event.getInventory().getItem(13));
    				List<String> Lore = event.getInventory().getItem(13).getItemMeta().getLore();
    						if(Lore.contains("�4Needs Charge!")){
    						Lore.remove(Lore.indexOf("�4Needs Charge!"));
    						}
    						
    						ItemMeta meta = item.getItemMeta();
    						if(Lore.get(2).contains("Charge")){
    							Lore.set(2, "�7Charge: �9" + 1000 + " �8�l/ �91000");	
    							meta.setLore(Lore);
    							item.setItemMeta(meta);
    							}else if(Lore.get(3).contains("Charge")){
    							Lore.set(3, "�7Charge: �9" + 1000 + " �8�l/ �91000");	
    							meta.setLore(Lore);
    							item.setItemMeta(meta);
    							}else if(Lore.get(4).contains("Charge")){
    							Lore.set(4, "�7Charge: �9" + 1000 + " �8�l/ �91000");	
    							meta.setLore(Lore);
    							item.setItemMeta(meta);
    							}
    						event.getWhoClicked().getInventory().addItem(item);
    						event.getWhoClicked().closeInventory();
    						((Player)event.getWhoClicked()).playSound(event.getWhoClicked().getLocation(), Sound.ANVIL_USE, 1F, 1F);
    						for(Player s : Bukkit.getOnlinePlayers()){
    							try {
    								ParticleEffects.CRIT.sendToPlayer(s, event.getWhoClicked().getLocation(), new Random().nextFloat(), new Random().nextFloat(), new Random().nextFloat(), 1, 50);
    							} catch (Exception e) {
    							}
    						}
    			}else{
    				//NOT ENOUGH MONEY
    				((Player)event.getWhoClicked()).sendMessage("�cYou do not have enough money in your �6�lInventory �cto complete this transaction!");
    				((Player)event.getWhoClicked()).playSound(event.getWhoClicked().getLocation(), Sound.ITEM_BREAK, 1, 1);
    				for(Player s : Bukkit.getOnlinePlayers()){
    					try {
    						ParticleEffects.LAVA.sendToPlayer(s, event.getWhoClicked().getLocation(), new Random().nextFloat(), new Random().nextFloat(), new Random().nextFloat(), 1, 100);
    					} catch (Exception e) {
    					}
    				}
    			}
    		}
    	}
    }
}
