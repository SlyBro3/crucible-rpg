package com.me.sly.CrucibleEmpire.Main;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Scrolls implements Listener {
	public static ceMain plugin;
    public Scrolls(ceMain instance) {
        plugin = instance;
    }
	@SuppressWarnings({ "static-access", "deprecation" })
	@EventHandler
	public void EnchantScrollClick(InventoryClickEvent event){
		if(event.getCursor() != null && event.getCurrentItem() != null){
		if(event.getCursor().hasItemMeta() && event.getCurrentItem().hasItemMeta()){
			ItemMeta Meta = event.getCurrentItem().getItemMeta();
			if(event.getCursor().getItemMeta().hasDisplayName() && event.getCurrentItem().getItemMeta().hasLore()){
				List<String> Lore = Meta.getLore();
		if(event.getCursor().getItemMeta().getDisplayName().contains("Armor Enchant Scroll") && event.getCurrentItem().getItemMeta().getLore().get(0).contains("HP: ")){
		Player player = (Player) event.getWhoClicked();
			event.setCancelled(true);
		if(Lore.get(0).length() == 9){
			int s = (Integer.parseInt((String) event.getCurrentItem().getItemMeta().getLore().get(0).subSequence(6, 9)) + (Integer.parseInt((String) event.getCurrentItem().getItemMeta().getLore().get(0).subSequence(6, 9)) / 20));
			Lore.set(0, ChatColor.DARK_AQUA + "HP: " + s);
			Meta.setLore(Lore);
			event.getCurrentItem().setItemMeta(Meta);
		}else if(Lore.get(0).length() == 10){
			int s = (Integer.parseInt((String) event.getCurrentItem().getItemMeta().getLore().get(0).subSequence(6, 10)) + (Integer.parseInt((String) event.getCurrentItem().getItemMeta().getLore().get(0).subSequence(6, 10)) / 20));
			Lore.set(0, ChatColor.DARK_AQUA + "HP: " + s);
			Meta.setLore(Lore);
			event.getCurrentItem().setItemMeta(Meta);
		}else if(Lore.get(0).length() == 11){
			int s = (Integer.parseInt((String) event.getCurrentItem().getItemMeta().getLore().get(0).subSequence(6, 11)) + (Integer.parseInt((String) event.getCurrentItem().getItemMeta().getLore().get(0).subSequence(6, 11)) / 20));
			Lore.set(0, ChatColor.DARK_AQUA + "HP: " + s);
			Meta.setLore(Lore);
			event.getCurrentItem().setItemMeta(Meta);
		}
		if(event.getCursor().getAmount() != 1){
			event.getCursor().setAmount(event.getCursor().getAmount() - 1);
		}else{
			event.setCursor(new ItemStack(0, 1));
		}
		if(Meta.getDisplayName().contains("�f�l[+2]")){
			Meta.setDisplayName("�f�l[+3]" + Meta.getDisplayName().substring(8, Meta.getDisplayName().length()));
			event.getCurrentItem().setItemMeta(Meta);
		}else if(Meta.getDisplayName().contains("�f�l[+3]")){
			if(plugin.rand(1, 5) == 1){
				event.setCurrentItem(new ItemStack(0, 1));
				player.sendMessage(ChatColor.RED + "The Enchant has " + ChatColor.DARK_RED + "FAILED");
			}else{
				Meta.setDisplayName("�f�l[+4]" + Meta.getDisplayName().substring(8, Meta.getDisplayName().length()));
				event.getCurrentItem().setItemMeta(Meta);
				event.setCurrentItem(plugin.addGlow(event.getCurrentItem()));
			}
			event.getCurrentItem().setItemMeta(Meta);
		}else if(Meta.getDisplayName().contains("�f�l[+4]")){
			if(plugin.rand(1, 4) == 1){
				event.setCurrentItem(new ItemStack(0, 1));
				player.sendMessage(ChatColor.RED + "The Enchant has " + ChatColor.DARK_RED + "FAILED");
			}else{
				Meta.setDisplayName("�f�l[+5]" + Meta.getDisplayName().substring(8, Meta.getDisplayName().length()));
				event.getCurrentItem().setItemMeta(Meta);
			}
			event.getCurrentItem().setItemMeta(Meta);
		}else if(Meta.getDisplayName().contains("�f�l[+5]")){
			if(plugin.rand(1, 3) == 1){
				event.setCurrentItem(new ItemStack(0, 1));
				player.sendMessage(ChatColor.RED + "The Enchant has " + ChatColor.DARK_RED + "FAILED");
			}else{
				Meta.setDisplayName("�f�l[+6]" + Meta.getDisplayName().substring(8, Meta.getDisplayName().length()));
				event.getCurrentItem().setItemMeta(Meta);
			}
			event.getCurrentItem().setItemMeta(Meta);
		}else if(Meta.getDisplayName().contains("�f�l[+6]")){
				if(Lore.get(0).length() == 9){
					int s = (Integer.parseInt((String) Meta.getLore().get(0).subSequence(6, 9)) - (Integer.parseInt((String) Meta.getLore().get(0).subSequence(6, 9)) / 20));
					Lore.set(0, ChatColor.DARK_AQUA + "HP: " + s);
					Meta.setLore(Lore);
					event.getCurrentItem().setItemMeta(Meta);
				}else if(Lore.get(0).length() == 10){
					int s = (Integer.parseInt((String) Meta.getLore().get(0).subSequence(6, 10)) - (Integer.parseInt((String) Meta.getLore().get(0).subSequence(6, 10)) / 20));
					Lore.set(0, ChatColor.DARK_AQUA + "HP: " + s);
					Meta.setLore(Lore);
					event.getCurrentItem().setItemMeta(Meta);
				}else if(Lore.get(0).length() == 11){
					int s = (Integer.parseInt((String) Meta.getLore().get(0).subSequence(6, 11)) - (Integer.parseInt((String) Meta.getLore().get(0).subSequence(6, 11)) / 20));
					Lore.set(0, ChatColor.DARK_AQUA + "HP: " + s);
					Meta.setLore(Lore);
					event.getCurrentItem().setItemMeta(Meta);
				}
				event.getCurrentItem().setItemMeta(Meta);
		}else if(Meta.getDisplayName().contains("�f�l[+1]")){
			Meta.setDisplayName("�f�l[+2]" + Meta.getDisplayName().substring(8, Meta.getDisplayName().length()));
			event.getCurrentItem().setItemMeta(Meta);
		}else{
		Meta.setDisplayName("�f�l[+1]" + Meta.getDisplayName());
		event.getCurrentItem().setItemMeta(Meta);
		}
	}else if(event.getCursor().getItemMeta().getDisplayName().contains("Weapon Enchant Scroll") && event.getCurrentItem().getItemMeta().getLore().get(0).contains("Damage")){
		Player player = (Player) event.getWhoClicked();
		String poo = event.getCurrentItem().getItemMeta().getLore().get(0);
		poo = poo.split("-")[0];
		poo = poo.replaceAll("�3", "");
		int po = Integer.parseInt(poo.trim());
		String koo = event.getCurrentItem().getItemMeta().getLore().get(0);
		koo = koo.split("-")[1];
		koo = koo.replaceAll(" Damage", "");
		int ko = Integer.parseInt(koo.trim());
		event.setCancelled(true);
		int r = po + po / 20;
		int f = ko + ko / 20;
		if(f < 1){
			f = 2;
		}
		if(r < 1){
			r = 2;
		}
		Lore.set(0, ChatColor.DARK_AQUA + "" + r + " - " + f + " Damage");
		Meta.setLore(Lore);
		event.getCurrentItem().setItemMeta(Meta);
	if(event.getCursor().getAmount() != 1){
		event.getCursor().setAmount(event.getCursor().getAmount() - 1);
	}else{
		event.setCursor(new ItemStack(0, 1));
	}
	if(Meta.getDisplayName().contains("�f�l[+2]")){
		Meta.setDisplayName("�f�l[+3]" + Meta.getDisplayName().substring(8, Meta.getDisplayName().length()));
		event.getCurrentItem().setItemMeta(Meta);
	}else if(Meta.getDisplayName().contains("�f�l[+3]")){
		if(plugin.rand(1, 5) == 1){
			event.setCurrentItem(new ItemStack(0, 1));
			player.sendMessage(ChatColor.RED + "The Enchant has " + ChatColor.DARK_RED + "FAILED");
		}else{
			Meta.setDisplayName("�f�l[+4]" + Meta.getDisplayName().substring(8, Meta.getDisplayName().length()));
			event.getCurrentItem().setItemMeta(Meta);
			event.setCurrentItem(plugin.addGlow(event.getCurrentItem()));
		}
		event.getCurrentItem().setItemMeta(Meta);
	}else if(Meta.getDisplayName().contains("�f�l[+4]")){
		if(plugin.rand(1, 4) == 1){
			event.setCurrentItem(new ItemStack(0, 1));
			player.sendMessage(ChatColor.RED + "The Enchant has " + ChatColor.DARK_RED + "FAILED");
		}else{
			Meta.setDisplayName("�f�l[+5]" + Meta.getDisplayName().substring(8, Meta.getDisplayName().length()));
			event.getCurrentItem().setItemMeta(Meta);
		}
		event.getCurrentItem().setItemMeta(Meta);
	}else if(Meta.getDisplayName().contains("�f�l[+5]")){
		if(plugin.rand(1, 3) == 1){
			event.setCurrentItem(new ItemStack(0, 1));
			player.sendMessage(ChatColor.RED + "The Enchant has " + ChatColor.DARK_RED + "FAILED");
		}else{
			Meta.setDisplayName("�f�l[+6]" + Meta.getDisplayName().substring(8, Meta.getDisplayName().length()));
			event.getCurrentItem().setItemMeta(Meta);
		}
		event.getCurrentItem().setItemMeta(Meta);
	}else if(Meta.getDisplayName().contains("�f�l[+6]")){
		String loo = event.getCurrentItem().getItemMeta().getLore().get(0);
		loo = loo.split("-")[0];
		loo = loo.replaceAll("�3", "");
		int lo = Integer.parseInt(poo.trim());
		String hoo = event.getCurrentItem().getItemMeta().getLore().get(0);
		hoo = hoo.split("-")[1];
		hoo = hoo.replaceAll("Damage", "");
		int ho = Integer.parseInt(koo.trim());
		event.setCancelled(true);
		int g = lo + lo / 20;
		int h = ho + ho / 20;
		if(g < 1){
			g = 2;
		}
		if(h < 1){
			h = 2;
		}
		g = (Integer.parseInt((String) event.getCurrentItem().getItemMeta().getLore().get(0).subSequence(2, 5)) - (Integer.parseInt((String) event.getCurrentItem().getItemMeta().getLore().get(0).subSequence(2, 5)) / 20));
		h = (Integer.parseInt((String) event.getCurrentItem().getItemMeta().getLore().get(0).subSequence(8, 11)) - (Integer.parseInt((String) event.getCurrentItem().getItemMeta().getLore().get(0).subSequence(8, 11)) / 20));
		Lore.set(0, ChatColor.DARK_AQUA + "" + g + " - " + h + " Damage");
		Meta.setLore(Lore);
		event.getCurrentItem().setItemMeta(Meta);
	}else if(Meta.getDisplayName().contains("�f�l[+1]")){
		Meta.setDisplayName("�f�l[+2]" + Meta.getDisplayName().substring(8, Meta.getDisplayName().length()));
		event.getCurrentItem().setItemMeta(Meta);
	}else{
	Meta.setDisplayName("�f�l[+1]" + Meta.getDisplayName());
	event.getCurrentItem().setItemMeta(Meta);
	}
	}else if(event.getCursor().getItemMeta().getDisplayName().contains("Attribute Scroll") && event.getCurrentItem().getItemMeta().getLore().get(0).contains("Damage")){
		event.setCancelled(true);
		if(event.getCursor().getItemMeta().getDisplayName().contains("Ice")){
			int s = plugin.rand(1, 7);
			if(s != 2){
			if(event.getCursor().getAmount() != 1){
				event.getCursor().setAmount(event.getCursor().getAmount() - 1);
			}else{
				event.setCursor(new ItemStack(0, 1));
			}
			Lore.set(1, ChatColor.DARK_BLUE + "Ice-Damage " + ChatColor.AQUA + "+" + plugin.rand(5, 17));
			Meta.setLore(Lore);
			event.getCurrentItem().setItemMeta(Meta);
		}
	}else if(event.getCursor().getItemMeta().getDisplayName().contains("Fire")){
		int s = plugin.rand(1, 7);
		if(s != 2){
		if(event.getCursor().getAmount() != 1){
			event.getCursor().setAmount(event.getCursor().getAmount() - 1);
		}else{
			event.setCursor(new ItemStack(0, 1));
		}
		Lore.set(1, ChatColor.DARK_RED + "Fire-Damage " + ChatColor.RED + "+" + plugin.rand(5, 17));
		Meta.setLore(Lore);
		event.getCurrentItem().setItemMeta(Meta);
	}
		
	}else if(event.getCursor().getItemMeta().getDisplayName().contains("Poison")){
		int s = plugin.rand(1, 7);
		if(s != 2){
		if(event.getCursor().getAmount() != 1){
			event.getCursor().setAmount(event.getCursor().getAmount() - 1);
		}else{
			event.setCursor(new ItemStack(0, 1));
		}
		Lore.set(1, ChatColor.DARK_GREEN + "Poison DMG " + ChatColor.GREEN + "+" + plugin.rand(5, 14));
		Meta.setLore(Lore);
		event.getCurrentItem().setItemMeta(Meta);
	}
		
	}
	}
			}
		}
	}
	}
}
