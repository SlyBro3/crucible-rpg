package com.me.sly.CrucibleEmpire.Main;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.me.sly.resources.MyMetadata;

public class Damages implements Listener {
	public static ceMain plugin;
    public Damages(ceMain instance) {
        plugin = instance;
    }
    public final HashMap<String, ItemStack[]> inv = new HashMap<String, ItemStack[]>();
    List<String> neutral = new ArrayList<String>();
    List<String> chaotic = new ArrayList<String>();
    public static ArrayList<String> inCombat = new ArrayList<String>();
	@EventHandler
    public void CombatLog(PlayerQuitEvent event){
    	if(inCombat.contains(event.getPlayer().getName())){
    		event.getPlayer().setHealth(0.0);
    		if(event.getPlayer().getInventory().getContents() != null){
    		for(ItemStack e : event.getPlayer().getInventory().getContents()){
    			if(e != null && e.getType() != Material.AIR){
    			}
    		}
    	}
    		if(event.getPlayer().getInventory().getArmorContents() != null){
        		for(ItemStack e : event.getPlayer().getInventory().getArmorContents()){
        			if(e != null && e.getType() != Material.AIR){
            		event.getPlayer().getWorld().dropItem(event.getPlayer().getLocation(), e);
            		}	
        		}
    		}
    	}
    }
    @SuppressWarnings({ "deprecation", "static-access" })
	@EventHandler
    public void Damaging(final EntityDamageByEntityEvent event){
    	if(!event.getEntity().hasMetadata("Invulnerable")){
    	boolean Dodged = false;
    	boolean IsInSpawn = false;
    	if(plugin.wg.getRegionManager(Bukkit.getWorld("world")).hasRegion("Spawns")){
            if (plugin.wg.getRegionManager(Bukkit.getWorld("world")).getRegion("Spawns").contains(event.getEntity().getLocation().getBlockX(), event.getEntity().getLocation().getBlockY(), event.getEntity().getLocation().getBlockZ())) {
             	IsInSpawn = true;
            }
    	}
    	if(!IsInSpawn){
    	if(!event.getEntity().hasMetadata("NPC")){
    	/*PUTTING THE PLAYER IN COMBAT*/
		if(event.getEntity() instanceof Player){
			if(!event.isCancelled()){
			inCombat.add(((Player) event.getEntity()).getName());
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
				public void run(){
					inCombat.remove(((Player) event.getEntity()).getName());
				}}, 250);
			}
		}
		if(event.getDamager() instanceof Player){
			if(!event.isCancelled()){
			inCombat.add(((Player) event.getDamager()).getName());
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
				public void run(){
					inCombat.remove(((Player) event.getDamager()).getName());
				}}, 250);
		}
		}
		/*PUTTING THE PLAYER IN COMBAT*/
    	double Damage = 1;
		if(event.getEntity() instanceof Player){
			Player player = (Player) event.getEntity();
		int A = plugin.Agility((Player) event.getEntity());
		int R = plugin.rand(0, 101);
		if(R < A + (plugin.getPlayerConfig(player.getName()).getInt(((Player) event.getEntity()).getName().toUpperCase() + ".DEXTERITY")/5)){	
			Dodged = true;
			Damage = 0;
			}
		}
		if(event.getDamager() instanceof Player){
			Player player = (Player) event.getDamager();
		if(Dodged == true){
			int Acc = plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".STABILITY");
			Acc = Acc / 5;
			if(plugin.rand(0, 101) < Acc){
				Dodged = false;
				player.sendMessage("§a§l*ACCURATE SHOT*");
				((Player) event.getEntity()).sendMessage("§2" +  player.getName() + " §a§l*ACCURATE SHOT*");
			}
		}
		}
    	if(event.getDamager() instanceof Player){
    		if(!event.getDamager().hasMetadata("HITSOMEONEP")){
    			if(!Dodged){
    		event.getDamager().setMetadata("HITSOMEONEP", new MyMetadata(plugin, "S"));
    		new BukkitRunnable(){
    			public void run(){
    				event.getDamager().removeMetadata("HITSOMEONEP", plugin);
    			}
    		}.runTaskLater(plugin, 3);
    		final CraftPlayer player = (CraftPlayer) event.getDamager();
    		final CraftLivingEntity entity = (CraftLivingEntity) event.getEntity();
		if(player.getItemInHand() != null && player.getItemInHand().hasItemMeta() && ((Player)event.getDamager()).getItemInHand().getItemMeta().hasLore() && player.getItemInHand().getType() != Material.BOW && player.getItemInHand().getItemMeta().getLore().get(0).contains("Damage")){
			if(player.getItemInHand().getItemMeta().getLore().contains("§4Needs Charge!")){
				event.setCancelled(true);
				return;
			}else{
			String poo = player.getItemInHand().getItemMeta().getLore().get(0);
			poo = poo.split("-")[0];
			poo = poo.replaceAll("§3", "");
			int po = Integer.parseInt(poo.trim());
			String koo = player.getItemInHand().getItemMeta().getLore().get(0);
			koo = koo.split("-")[1];
			koo = koo.replaceAll(" Damage", "");
			int ko = Integer.parseInt(koo.trim());
			int Min = 1;
			int Max = 10;
			Min = po;
			Max = ko;
			Damage = plugin.rand(Min, Max + 1);
			if(entity.getHealth() - Damage < 0){
				entity.damage(2147000.0);
				event.getEntity().setMetadata("Dead", new MyMetadata(plugin, " "));
			}else{
			entity.setHealth(entity.getHealth() - Damage);
			}
				}
			}
    			}
	    		/*ATTRIBUTE DAMAGE*/
				if(event.getDamager() instanceof Player){
		    		final CraftPlayer player = (CraftPlayer) event.getDamager();
					if(player.getItemInHand() != null && player.getItemInHand().hasItemMeta() && player.getItemInHand().getItemMeta().hasLore()){
					if(!Dodged){
					if(!event.getEntity().hasMetadata("Dead")){
					for(String lore : player.getItemInHand().getItemMeta().getLore()){
				if(lore.contains(("Ice"))){
				event.getEntity().getWorld().playEffect(event.getEntity().getLocation().add(.5, .5, .5), Effect.POTION_BREAK, (byte) 8194);
				((LivingEntity)event.getEntity()).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 7, 1));
				double r = Integer.parseInt(lore.replaceAll("§bIce DMG §1+", "").replaceAll("§9","").trim());
				if(((CraftLivingEntity)event.getEntity()).getHealth() - r < 0){
					((CraftLivingEntity)event.getEntity()).damage(2147000.0);
					event.getEntity().setMetadata("Dead", new MyMetadata(plugin, " "));
				}else{
					((CraftLivingEntity)event.getEntity()).setHealth(((CraftLivingEntity)event.getEntity()).getHealth() - r);
		}
				}else if(lore.contains(("Poison"))){
					event.getEntity().getWorld().playEffect(event.getEntity().getLocation().add(.5, .5, .5), Effect.POTION_BREAK, (byte) 16452);
					double r = Integer.parseInt(lore.replaceAll("§aPoison DMG §2+", "").trim());
					if(((CraftLivingEntity)event.getEntity()).getHealth() - r < 0){
						((CraftLivingEntity)event.getEntity()).damage(2147000.0);
						event.getEntity().setMetadata("Dead", new MyMetadata(plugin, " "));
					}else{
						((CraftLivingEntity)event.getEntity()).setHealth(((CraftLivingEntity)event.getEntity()).getHealth() - r);
			}
				}else if(lore.contains(("Siphon"))){
						if(lore.contains("1")){
							double o = event.getDamage() / 25;
							int s = (int) o;
							if((((CraftPlayer)event.getDamager()).getHealth() + o) > ((CraftPlayer)event.getDamager()).getMaxHealth()){
								((Player)event.getDamager()).setHealth(((CraftPlayer)event.getDamager()).getMaxHealth());
							}else{
							((Player)event.getDamager()).setHealth(((CraftPlayer)event.getDamager()).getHealth() + o);
							}
							player.sendMessage("§a§l+ §a[" + s + "§a§lHP§a]");
						}else if(lore.contains("2")){
							double o = event.getDamage() / 35;
							int s = (int) o;
							if((((CraftPlayer)event.getDamager()).getHealth() + o) > ((CraftPlayer)event.getDamager()).getMaxHealth()){
								((Player)event.getDamager()).setHealth(((CraftPlayer)event.getDamager()).getMaxHealth());
							}else{
							((Player)event.getDamager()).setHealth(((CraftPlayer)event.getDamager()).getHealth() + o);
							}
							player.sendMessage("§a§l+ §a[" + s + "§a§lHP§a]");
						}else if(lore.contains("3")){
							double o = event.getDamage() / 50;
							int s = (int) Math.ceil(o);
							if((((CraftPlayer)event.getDamager()).getHealth() + o) > ((CraftPlayer)event.getDamager()).getMaxHealth()){
								((Player)event.getDamager()).setHealth(((CraftPlayer)event.getDamager()).getMaxHealth());
							}else{
							((Player)event.getDamager()).setHealth(((CraftPlayer)event.getDamager()).getHealth() + o);
							}
							player.sendMessage("§a§l+ §a[" + s + "§a§lHP§a]");
						}
							event.getEntity().getWorld().playEffect(event.getEntity().getLocation(), Effect.STEP_SOUND, 152);
				}else if(lore.contains(("Fire"))){
					event.getEntity().setFireTicks(5);
					double r = Integer.parseInt(lore.replaceAll("§cFire DMG §4+", "").trim());
					if(((CraftLivingEntity)event.getEntity()).getHealth() - r < 0){
						((CraftLivingEntity)event.getEntity()).damage(2147000.0);
						event.getEntity().setMetadata("Dead", new MyMetadata(plugin, " "));
					}else{
						((CraftLivingEntity)event.getEntity()).setHealth(((CraftLivingEntity)event.getEntity()).getHealth() - r);
			}
		}
		}
		}
		}
		}
		}
	    		/*ATTRIBUTE DAMAGE*/
		/*SKILLS*/
				if(event.getDamager() instanceof Player){
		    		final CraftPlayer player = (CraftPlayer) event.getDamager();
		    		final CraftLivingEntity entity = (CraftLivingEntity) event.getEntity();
		if(player.getItemInHand().getTypeId() == 276 || player.getItemInHand().getTypeId() == 267 ||player.getItemInHand().getTypeId() == 283 ||player.getItemInHand().getTypeId() == 272 ||player.getItemInHand().getTypeId() == 268){
			int damage = (int) Damage;
			int Stabget = plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".STABILITY");
			double Stab = Stabget / 4;
			Stab = Stab / 100;
			Stab = 1.0 - Stab;
			Stab = damage / Stab;
			Stab = Stab - damage;
			if(!Dodged){
				if(!entity.hasMetadata("Dead")){
				if(entity.getHealth() - Stab < 0){
					entity.damage(2147000.0);
					entity.setMetadata("Dead", new MyMetadata(plugin, " "));
				}else{
				entity.setHealth(entity.getHealth() - Stab);
				Damage += Stab;
		}
				}
		}
		}
		if(player.getItemInHand().getTypeId() == 279 || player.getItemInHand().getTypeId() == 275 ||player.getItemInHand().getTypeId() == 286 ||player.getItemInHand().getTypeId() == 258 ||player.getItemInHand().getTypeId() == 271){
			int damage = (int) Damage;
			int Strget = plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".STRENGTH");
			double Str = Strget / 4;
			Str = Str / 100;
			Str = 1.0 - Str;
			Str = damage / Str;
			Str = Str - damage;
			if(!Dodged){
				if(!entity.hasMetadata("Dead")){
				if(entity.getHealth() - Str < 0){
					entity.damage(2147000.0);
					entity.setMetadata("Dead", new MyMetadata(plugin, " "));
				}else{
				entity.setHealth(entity.getHealth() - Str);
				Damage += Str;
			}
			}
			}
		}
		if(player.getItemInHand().getTypeId() == 269 || player.getItemInHand().getTypeId() == 273 ||player.getItemInHand().getTypeId() == 256 ||player.getItemInHand().getTypeId() == 284 ||player.getItemInHand().getTypeId() == 277){
			int damage = (int) Damage;
			int Stabget = plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".PRECISION");
			double Stab = Stabget / 4;
			Stab = Stab / 100;
			Stab = 1.0 - Stab;
			Stab = damage / Stab;
			Stab = Stab - damage;
			if(!Dodged){
				if(!entity.hasMetadata("Dead")){
				if(entity.getHealth() - Stab < 0){
					entity.damage(2147000.0);
					entity.setMetadata("Dead", new MyMetadata(plugin, " "));
				}else{
				entity.setHealth(entity.getHealth() - Stab);
				Damage += Stab;
		}
			}
			}
		}
		int CritChance = 0;
		int Strget = plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".STRENGTH");
		double Str = Strget / 15;
		CritChance = plugin.rand(0, 100);
		if(CritChance < Str){
			if(!Dodged){
				if(!entity.hasMetadata("Dead")){
				if(entity.getHealth() - (Damage/2) < 0){
					entity.damage(2147000.0);
					entity.setMetadata("DEAD", new MyMetadata(plugin, " "));
				}else{
				entity.setHealth(entity.getHealth() - (Damage/2));
				Damage += Damage / 2;
			}
			}
			player.sendMessage("§4§lCRITICAL HIT");
			if(event.getEntity() instanceof Player){
			((Player) event.getEntity()).sendMessage("§c" + player.getName() + " §4§lCRITICAL HIT");
			}
		}
		}
				}
		/*SKILLS*/	
    		}else{
    			event.setCancelled(true);
    			event.setDamage(0.0);
    			return;
    		}
    		/*DEBUGGING*/
    		final CraftPlayer player = (CraftPlayer) event.getDamager();
    		final CraftLivingEntity entity = (CraftLivingEntity) event.getEntity();
    				if(entity instanceof Player){
    					if(Dodged){
    						((Player) event.getDamager()).sendMessage(ChatColor.DARK_GREEN + ((Player) event.getEntity()).getName() + " §e§l*DODGED*");
    						((Player) event.getEntity()).sendMessage("§e§l*DODGED*");
    					}else{
    					CraftPlayer r = (CraftPlayer) entity;
    			r.sendMessage("     §c-" + (int)Math.ceil(Damage) + "§c§lHP §c➟ §a[" + (int)entity.getHealth() + "§a§lHP§a]");
    		player.sendMessage("     §c" + (int)Math.ceil(Damage) + " §c§lDMG" + " §c➟ " + r.getName() + "§a [" + ((int)r.getHealth()) + "§a§lHP§a]");
    					}
    					}else{
    					entity.setCustomName(this.getNameHealth(entity.getHealth(), entity.getMaxHealth(), (String) entity.getMetadata("Name").get(0).value()));
    					if(entity.hasMetadata("80 Armor")){
    	  					player.sendMessage("     §c" + (int)Damage + " §c§lDMG" + " §c➟ " + entity.getMetadata("Name").get(0).value() + "§3§l[80% A]" + "§a [" + ((int)entity.getHealth()) + "§a§lHP§a]");
						
    					}else if(entity.hasMetadata("50 Armor")){
    	  					player.sendMessage("     §c" + (int)Damage + " §c§lDMG" + " §c➟ " + entity.getMetadata("Name").get(0).value() + "§3§l[50% A]" + "§a [" + ((int)entity.getHealth()) + "§a§lHP§a]");
  						
    					}else{
    					player.sendMessage("     §c" + (int)Damage + " §c§lDMG" + " §c➟ " + entity.getMetadata("Name").get(0).value() + "§a [" + ((int)entity.getHealth()) + "§a§lHP§a]");
    				}
    				}
    		/*DEBUGGING*/
    	}else if(event.getDamager() instanceof LivingEntity){
    		/*MOB DAMAGING*/
    		if(!event.getDamager().hasMetadata("HITSOMEONE")){
    		event.getDamager().setMetadata("HITSOMEONE", new MyMetadata(plugin, "S"));
    		new BukkitRunnable(){
    			public void run(){
    				event.getDamager().removeMetadata("HITSOMEONE", plugin);
    			}
    		}.runTaskLater(plugin, 25);
    		if(((String)event.getDamager().getMetadata("Grade").get(0).value()).contains("A")){
    			int d = plugin.rand(120, 170);
    			if(((CraftLivingEntity)event.getEntity()).getHealth() - d < 0){
    				((CraftLivingEntity)event.getEntity()).damage(2147000.0);
    			}else{
    			((CraftLivingEntity)event.getEntity()).setHealth(((CraftLivingEntity)event.getEntity()).getHealth() - d);
    			}
    			if(event.getEntity() instanceof Player){
					CraftPlayer player = (CraftPlayer)event.getEntity();
		player.sendMessage("     §c-" + d + "§c§lHP §c➟ §a[" + (int)player.getHealth() + "§a§lHP§a]");
				}
    		}else if(((String)event.getDamager().getMetadata("Grade").get(0).value()).contains("B")){
    			int d = plugin.rand(40, 75);
    			if(((CraftLivingEntity)event.getEntity()).getHealth() - d < 0){
    				((CraftLivingEntity)event.getEntity()).damage(2147000.0);
    			}else{
    			((CraftLivingEntity)event.getEntity()).setHealth(((CraftLivingEntity)event.getEntity()).getHealth() - d);
    			}
    			if(event.getEntity() instanceof Player){
					CraftPlayer player = (CraftPlayer)event.getEntity();
		player.sendMessage("     §c-" + d + "§c§lHP §c➟ §a[" + (int)player.getHealth() + "§a§lHP§a]");
    			}
    		}else if(((String)event.getDamager().getMetadata("Grade").get(0).value()).contains("C")){
    			int d = plugin.rand(40, 70);
    			if(((CraftLivingEntity)event.getEntity()).getHealth() - d < 0){
    				((CraftLivingEntity)event.getEntity()).damage(2147000.0);
    			}else{
    			((CraftLivingEntity)event.getEntity()).setHealth(((CraftLivingEntity)event.getEntity()).getHealth() - d);
    			}
    			if(event.getEntity() instanceof Player){
					CraftPlayer player = (CraftPlayer)event.getEntity();
		player.sendMessage("     §c-" + d + "§c§lHP §c➟ §a[" + (int)player.getHealth() + "§a§lHP§a]");
				}
    		}else if(((String)event.getDamager().getMetadata("Grade").get(0).value()).contains("D")){
    			int d = plugin.rand(10, 20);
    			if(((CraftLivingEntity)event.getEntity()).getHealth() - d < 0){
    				((CraftLivingEntity)event.getEntity()).damage(2147000.0);
    			}else{
    			((CraftLivingEntity)event.getEntity()).setHealth(((CraftLivingEntity)event.getEntity()).getHealth() - d);
    			}
    			if(event.getEntity() instanceof Player){
					CraftPlayer player = (CraftPlayer)event.getEntity();
		player.sendMessage("     §c-" + d + "§c§lHP §c➟ §a[" + (int)player.getHealth() + "§a§lHP§a]");
				}
    		}
    		}else{
    		
    		}
    		/*MOB DAMAGING*/
			/*DAMAGE CAUSE CHANGING*/
			if(event.getCause() == DamageCause.FALL){
				event.setCancelled(true);
			}
			/*DAMAGE CAUSE CHANGING*/
    	}
    	}
    	}
    	}else{
    		event.setCancelled(true);
    		event.setDamage(0.0);
    	}
    }
	/*@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.MONITOR)
	public void LoreDamage(final EntityDamageByEntityEvent event){

		boolean Agility = false;
		double DDamage = 0;
		if(event.getEntity() instanceof Player){
		int A = plugin.Agility((Player) event.getEntity());
		int R = plugin.rand(0, 101);
		if(R < A + (plugin.getConfig().getInt(((Player) event.getEntity()).getName() + ".DEXTERITY")/5)){	
			Agility = true;
			event.setCancelled(true);
			}
		}
		//STOP HERE
		if(event.getDamager() instanceof Player){
			Player player = (Player) event.getDamager();
		if(Agility == true){
			if(event.isCancelled() == false){
			int Acc = plugin.getConfig().getInt(player.getName().toUpperCase() + ".STABILITY");
			Acc = Acc / 4;
			if(plugin.rand(0, 101) < Acc){
				event.setCancelled(false);
				Agility = false;
				player.sendMessage("§a§l*ACCURATE SHOT*");
				((Player) event.getEntity()).sendMessage("§2" +  player.getName() + " §a§l*ACCURATE SHOT*");
			}
			}
		}
		}
		//STOP HERE
			if(event.getDamager() instanceof Player){
				double LDamage = event.getDamage();
				Player player = (Player) event.getDamager();
				if(Agility == false){
			if(player.getItemInHand() != null && player.getItemInHand().hasItemMeta() && ((Player)event.getDamager()).getItemInHand().getItemMeta().hasLore() && player.getItemInHand().getItemMeta().getLore().get(0).contains("Damage")){
				if(player.getItemInHand().getItemMeta().getLore().contains("§4Needs Charge!")){
				event.setCancelled(true);
				event.setDamage(0.0);
				}else{
				String poo = player.getItemInHand().getItemMeta().getLore().get(0);
				poo = poo.split("-")[0];
				poo = poo.replaceAll("§3", "");
				int po = Integer.parseInt(poo.trim());
				String koo = player.getItemInHand().getItemMeta().getLore().get(0);
				koo = koo.split("-")[1];
				koo = koo.replaceAll(" Damage", "");
				int ko = Integer.parseInt(koo.trim());
				int Min = 1;
				int Max = 10;
				Min = po;
				Max = ko;
				LDamage = plugin.rand(Min, Max);
			}
				DDamage = LDamage;
				if(event.getEntity() instanceof LivingEntity){
					event.setDamage(plugin.getDamageReducedMob((LivingEntity) event.getEntity(), DDamage) * 2);
				}
			}
				}

			String d = "s";
			if(event.getEntity() instanceof Player){
				int Damage = (int ) DDamage;
				d = "§c" + Damage + " §c§lDMG" + " §c-> " + ((Player)event.getEntity()).getDisplayName() + "[" + ((int)((CraftLivingEntity)event.getEntity()).getHealth()) + "HP]";
				Player s = (Player) event.getDamager();
				if(Agility == false){
				s.sendMessage(d);
					Player players = (Player) event.getEntity();
					double Fort = plugin.getConfig().getInt(players.getName().toUpperCase() + ".FORTITUDE") * 0.005;
					Fort = 1 - Fort;
					event.setDamage(event.getDamage() * Fort);
				}else{
					int o = plugin.rand(1, 3);
					if(o == 1){
				((Player) event.getDamager()).sendMessage(ChatColor.DARK_GREEN + ((Player) event.getEntity()).getName() + " §e§l*BLOCKED*");
				((Player) event.getEntity()).sendMessage("§e§l*BLOCKED*");
					}else{
						((Player) event.getDamager()).sendMessage(ChatColor.DARK_GREEN + ((Player) event.getEntity()).getName() + " §e§l*DODGED*");
						((Player) event.getEntity()).sendMessage("§e§l*DODGED*");
					
					}
				}
				
			}else{
				int Damage = (int ) DDamage;
			d = "§c" + Damage + " §c§lDMG" + " §c-> " + ((CraftLivingEntity)event.getEntity()).getCustomName() + "[" + ((int)((CraftLivingEntity)event.getEntity()).getHealth()) + "HP]";
		Player s = (Player) event.getDamager();
		if(Agility == false){
		s.sendMessage(d);
		}else{
			int o = plugin.rand(1, 3);
			if(o == 1){
		((Player) event.getDamager()).sendMessage(ChatColor.DARK_GREEN + ((Player) event.getEntity()).getName() + " §e§l*BLOCKED*");
		((Player) event.getEntity()).sendMessage("§e§l*BLOCKED*");
			}else{
				((Player) event.getDamager()).sendMessage(ChatColor.DARK_GREEN + ((Player) event.getEntity()).getName() + " §e§l*DODGED*");
				((Player) event.getEntity()).sendMessage("§e§l*DODGED*");
			
			}
		}
			}
			}
			*/
	@EventHandler
	public void BowShoot(EntityShootBowEvent event){
		if(event.getEntity() instanceof Player){
			if(event.getBow().hasItemMeta() && event.getBow().getItemMeta().hasLore()){
				if(!event.getBow().getItemMeta().getLore().contains("§4Needs Charge!")){
					for(String lore : event.getBow().getItemMeta().getLore()){
				if(lore.contains("Ice")){
					double r = Integer.parseInt(event.getBow().getItemMeta().getLore().get(1).replaceAll("§bIce DMG §9", "").trim());
					event.getProjectile().setMetadata("Ice", new MyMetadata(plugin, r));					
				}else if(lore.contains("Poison")){
					double r = Integer.parseInt(event.getBow().getItemMeta().getLore().get(1).replaceAll("§aPoison DMG §2", "").trim());
					event.getProjectile().setMetadata("Poison", new MyMetadata(plugin, r));		
				}
					}

				String line = event.getBow().getItemMeta().getLore().get(0);
				String minS = line.split("-")[0];
				String maxS = line.split("-")[1];
				maxS = maxS.replaceAll("Damage", "");
				minS = minS.replaceAll("§3", "");
			int MinR = Integer.parseInt(minS.trim());
			int MaxR = Integer.parseInt(maxS.trim());
			int Dex = plugin.getConfig().getInt(((Player)event.getEntity()).getName().toUpperCase() + ".DEXTERITY");
			double r = Dex / 300;
			r = 1.0 - r;
			int Min = (int) (MinR / r);
			int Max = (int) (MaxR / r);
			event.getProjectile().setMetadata("DamageMin", new MyMetadata(plugin, Min));
			event.getProjectile().setMetadata("DamageMax", new MyMetadata(plugin, Max));
		}
			}
		}else{
			if(event.getBow().hasItemMeta() && event.getBow().getItemMeta().hasLore()){
				if(!event.getBow().getItemMeta().getLore().contains("§4Needs Charge!")){
					for(String lore : event.getBow().getItemMeta().getLore()){
				if(lore.contains("Ice")){
					double r = Integer.parseInt(event.getBow().getItemMeta().getLore().get(1).replaceAll("§bIce DMG §9", "").trim());
					event.getProjectile().setMetadata("Ice", new MyMetadata(plugin, r));					
				}else if(lore.contains("Poison")){
					double r = Integer.parseInt(event.getBow().getItemMeta().getLore().get(1).replaceAll("§aPoison DMG §2", "").trim());
					event.getProjectile().setMetadata("Poison", new MyMetadata(plugin, r));		
				}
					}
				String line = event.getBow().getItemMeta().getLore().get(0);
				String minS = line.split("-")[0];
				String maxS = line.split("-")[1];
				maxS = maxS.replaceAll("Damage", "");
				minS = minS.replaceAll("§3", "");
			int MinR = Integer.parseInt(minS.trim());
			int MaxR = Integer.parseInt(maxS.trim());
			int Min = (int) (MinR);
			int Max = (int) (MaxR);
			event.getProjectile().setMetadata("DamageMin", new MyMetadata(plugin, Min));
			event.getProjectile().setMetadata("DamageMax", new MyMetadata(plugin, Max));
		}
	}
		}
	}
	@SuppressWarnings({ "static-access", "deprecation" })
	@EventHandler
	public void BowHit(EntityDamageByEntityEvent event){
		int Damage = 1;
		if(event.getDamager() instanceof Arrow){
			if(((Projectile) event.getDamager()).getShooter() instanceof Player){
				if(event.getDamager().getMetadata("DamageMin").isEmpty() == false){
			int DMin = (int) event.getDamager().getMetadata("DamageMin").get(0).value();
			int DMax = (int) event.getDamager().getMetadata("DamageMax").get(0).value();
			System.out.println(DMin);
			System.out.println(DMax);
			Damage = plugin.rand(DMin, DMax);
				if(((CraftLivingEntity)event.getEntity()).getHealth() - Damage >= 0){
				((CraftLivingEntity)event.getEntity()).setHealth(((CraftLivingEntity)event.getEntity()).getHealth() - Damage);
				}else{
					((CraftLivingEntity)event.getEntity()).damage(2147000000);
				}
			if(event.getDamager().hasMetadata("Ice")){
				if(((CraftLivingEntity)event.getEntity()).getHealth() - (Double)event.getDamager().getMetadata("Ice").get(0).value() > 0){
				((CraftLivingEntity)event.getEntity()).setHealth(((CraftLivingEntity)event.getEntity()).getHealth() - (Double)event.getDamager().getMetadata("Ice").get(0).value());
				}else{
					((CraftLivingEntity)event.getEntity()).damage(2147000000);
				}
				event.getEntity().getWorld().playEffect(event.getEntity().getLocation().add(.5, .5, .5), Effect.POTION_BREAK, (byte) 8194);
			}else if(event.getDamager().hasMetadata("Poison")){
				if(((CraftLivingEntity)event.getEntity()).getHealth() - (int) event.getDamager().getMetadata("Poison").get(0).value() > 0){
				((CraftLivingEntity)event.getEntity()).setHealth(((CraftLivingEntity)event.getEntity()).getHealth() - (int) event.getDamager().getMetadata("Poison").get(0).value());
				}else{
					((CraftLivingEntity)event.getEntity()).damage(2147000000);
				}				event.getEntity().getWorld().playEffect(event.getEntity().getLocation().add(.5, .5, .5), Effect.POTION_BREAK, (byte) 16452);
			}
				}else{
					System.out.println("No metadata");
				}
	    		final CraftPlayer player = (CraftPlayer) ((Projectile) event.getDamager()).getShooter();
	    		final CraftLivingEntity entity = (CraftLivingEntity) event.getEntity();
	    				if(entity instanceof Player){
	    					CraftPlayer r = (CraftPlayer) entity;
	    			r.sendMessage("     §c-" + (int)Damage + "§c§lHP §c➟ §a[" + (int)entity.getHealth() + "§a§lHP§a]");
	    		player.sendMessage("     §c" + (int)Damage + " §c§lDMG" + " §c➟ " + r.getName() + "§a [" + ((int)r.getHealth()) + "§a§lHP§a]");
	    				}else{
	    					entity.setCustomName(this.getNameHealth(entity.getHealth(), entity.getMaxHealth(), (String) entity.getMetadata("Name").get(0).value()));
	    					player.sendMessage("     §c" + (int)Damage + " §c§lDMG" + " §c➟ " + entity.getMetadata("Name").get(0).value() + "§a [" + ((int)entity.getHealth()) + "§a§lHP§a]");
	    				}
			}else{
				if(event.getDamager().getMetadata("DamageMin").isEmpty() == false){
			int DMin = (int) event.getDamager().getMetadata("DamageMin").get(0).value();
			int DMax = (int) event.getDamager().getMetadata("DamageMax").get(0).value();
			System.out.println(DMin);
			System.out.println(DMax);
			Damage = plugin.rand(DMin, DMax);
				if(((CraftLivingEntity)event.getEntity()).getHealth() - Damage >= 0){
				((CraftLivingEntity)event.getEntity()).setHealth(((CraftLivingEntity)event.getEntity()).getHealth() - Damage);
				}else{
					((CraftLivingEntity)event.getEntity()).damage(2147000000);
				}
			if(event.getDamager().hasMetadata("Ice")){
				if(((CraftLivingEntity)event.getEntity()).getHealth() - (Double)event.getDamager().getMetadata("Ice").get(0).value() > 0){
				((CraftLivingEntity)event.getEntity()).setHealth(((CraftLivingEntity)event.getEntity()).getHealth() - (Double)event.getDamager().getMetadata("Ice").get(0).value());
				}else{
					((CraftLivingEntity)event.getEntity()).damage(2147000000);
				}
				event.getEntity().getWorld().playEffect(event.getEntity().getLocation().add(.5, .5, .5), Effect.POTION_BREAK, (byte) 8194);
			}else if(event.getDamager().hasMetadata("Poison")){
				if(((CraftLivingEntity)event.getEntity()).getHealth() - (int) event.getDamager().getMetadata("Poison").get(0).value() > 0){
				((CraftLivingEntity)event.getEntity()).setHealth(((CraftLivingEntity)event.getEntity()).getHealth() - (int) event.getDamager().getMetadata("Poison").get(0).value());
				}else{
					((CraftLivingEntity)event.getEntity()).damage(2147000000);
				}				event.getEntity().getWorld().playEffect(event.getEntity().getLocation().add(.5, .5, .5), Effect.POTION_BREAK, (byte) 16452);
			}
				}else{
					System.out.println("No metadata");
				}
	    		final CraftLivingEntity entity = (CraftLivingEntity) event.getEntity();
	    				if(entity instanceof Player){
	    					CraftPlayer r = (CraftPlayer) entity;
	    			r.sendMessage("     §c-" + (int)Damage + "§c§lHP §c➟ §a[" + (int)entity.getHealth() + "§a§lHP§a]");
	    				}
			}
		}
	}
	@EventHandler
	public void Custom(EntityDamageEvent event){
		if(event.getEntity().hasMetadata("Invulnerable")){
			event.setCancelled(true);
		}
	}
	public static String getNameHealth(double Health, double MaxHealth, String Name){
		Name = ChatColor.stripColor(Name);
		double percent = Health / MaxHealth;
		double length = Name.length();
		double scolor = length * percent;
		int color = (int) Math.ceil(scolor);
		String n = Name;
		if(color < length / 3){
			//YELLOW
			String g = n.substring(color);
			String h = n.substring(0, color);
			n = "§c" + h + "§7" + g;
		}else if(color < length / 1.5){
			//RED
			String g = n.substring(color);
			String h = n.substring(0, color);
			n = "§e" + h + "§7" + g;
		}else{
			//GREEN
			String g = n.substring(color);
			String h = n.substring(0, color);
			n = "§a" + h + "§7" + g;
		}
		return n;
		
	}
}
//TODO
//PLAYER METADATA MANA

