package com.me.sly.CrucibleEmpire.Main;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.me.sly.resources.BetterItems;

public class Money implements Listener {
	public Inventory Merchant;
	public static ceMain plugin;
    public Money(ceMain instance) {
        plugin = instance;
    }
    @SuppressWarnings("deprecation")
	@EventHandler
    public void MoneyPickUp(PlayerPickupItemEvent event){
    	if(event.getItem().getItemStack().getType() == Material.getMaterial(175)){
        event.getPlayer().sendMessage(ChatColor.GRAY + "You have picked up " + ChatColor.GOLD + event.getItem().getItemStack().getAmount() + " Gold");
        event.setCancelled(true);
        event.getItem().remove();
        event.getPlayer().getInventory().setItem(9, BetterItems.DisplayNameAndLore(Material.DOUBLE_PLANT, 1, "�e�lBalances:", "�c�lInventory: �b�o" + plugin.invBalance(event.getPlayer()) + "g", "�c�lBank: �b�o" + (int)plugin.economy.getBalance(event.getPlayer().getName()) + "g"));
        plugin.setBalance(event.getPlayer(), plugin.invBalance(event.getPlayer()) + event.getItem().getItemStack().getAmount());
    	}
    }
	@EventHandler
    public void MoneyJoin(PlayerDropItemEvent event){
		if(!event.getPlayer().hasPermission("Crucible.Staff")){
		if(event.getItemDrop().getItemStack().getType().equals(Material.DOUBLE_PLANT)){
			event.getItemDrop().remove();
			event.getPlayer().getInventory().setItem(9, BetterItems.DisplayNameAndLore(Material.DOUBLE_PLANT, 1, "�e�lBalances:", "�c�lInventory: �b�o" + plugin.invBalance(event.getPlayer()) + "g", "�c�lBank: �b�o" + (int)plugin.economy.getBalance(event.getPlayer().getName()) + "g"));
			event.getPlayer().updateInventory();
		}
    }
	}
	@EventHandler
	public void MoneyNoooo(InventoryClickEvent event){
		if(event.getCurrentItem() != null && (event.getCurrentItem().getType() == Material.DOUBLE_PLANT || event.getCursor().getType() == Material.DOUBLE_PLANT)){
			event.setCancelled(true);
		}
	}
	@EventHandler
	public void AcGUIClick(InventoryClickEvent event){
		if(event.getInventory().getTitle().equals("Bank Depositing and Withdrawing")){
			event.setCancelled(true);
		}
	}
	@SuppressWarnings("unchecked")
	@EventHandler
	public void BANK(PlayerInteractEvent event){
		if(event.getClickedBlock() != null){
	if(event.getClickedBlock().getType() == Material.ENDER_CHEST){
		event.setCancelled(true);
		if(plugin.getPlayerConfig(event.getPlayer().getName()).contains(event.getPlayer().getName() + ".BankSize") && plugin.getPlayerConfig(event.getPlayer().getName()).contains(event.getPlayer().getName() + ".BankContents")){
	Inventory Bank = Bukkit.createInventory(event.getPlayer(), ((int) plugin.getPlayerConfig(event.getPlayer().getName()).get(event.getPlayer().getName() + ".BankSize")), event.getPlayer().getName() + " Storage Crate");	
	event.getPlayer().openInventory(Bank);
    List<ItemStack> Inv = (List<ItemStack>) plugin.getPlayerConfig(event.getPlayer().getName()).getList(event.getPlayer().getName() + ".BankContents");
	Bank.setContents(Inv.toArray(new ItemStack[0]));
	event.getPlayer().updateInventory();
		}else if(plugin.getPlayerConfig(event.getPlayer().getName()).contains(event.getPlayer().getName() + ".BankSize")){
	    	Inventory Bank = Bukkit.createInventory(event.getPlayer(), plugin.getPlayerConfig(event.getPlayer().getName()).getInt(event.getPlayer().getName() + ".BankSize"), event.getPlayer().getName() + " Storage Crate");	
	    	event.getPlayer().openInventory(Bank);	
		}else{
	    	Inventory Bank = Bukkit.createInventory(event.getPlayer(), 18, event.getPlayer().getName() + " Storage Crate");	
	    	event.getPlayer().openInventory(Bank);
	        List<ItemStack> Inv = (List<ItemStack>)plugin.getPlayerConfig(event.getPlayer().getName()).getList(event.getPlayer().getName() + ".BankContents");
	    	Bank.setContents(Inv.toArray(new ItemStack[0]));
	    	event.getPlayer().updateInventory();
		}
	}
	}
	}
	@EventHandler
    public void MoneyNoClick(PlayerJoinEvent event){
        event.getPlayer().getInventory().setItem(9, BetterItems.DisplayNameAndLore(Material.DOUBLE_PLANT, 1, "�e�lBalances:", "�c�lInventory: �b�o" + plugin.invBalance(event.getPlayer()) + "g", "�c�lBank: �b�o" + plugin.economy.getBalance(event.getPlayer().getName()) + "g"));
    }
    @SuppressWarnings("unchecked")
	@EventHandler
    public void MerchantBanker(PlayerInteractEntityEvent event){
    	if(event.getRightClicked() instanceof Player){
    		if(event.getRightClicked().hasMetadata("NPC")){
if(((Player)event.getRightClicked()).getDisplayName().contains("Storage")){
    		if(plugin.getPlayerConfig(event.getPlayer().getName()).contains(event.getPlayer().getName() + ".BankSize") && plugin.getPlayerConfig(event.getPlayer().getName()).contains(event.getPlayer().getName() + ".BankContents")){
    	Inventory Bank = Bukkit.createInventory(event.getPlayer(), ((int) plugin.getPlayerConfig(event.getPlayer().getName()).get(event.getPlayer().getName() + ".BankSize")), event.getPlayer().getName() + " Storage Crate");	
    	event.getPlayer().openInventory(Bank);
        List<ItemStack> Inv = (List<ItemStack>) plugin.getPlayerConfig(event.getPlayer().getName()).getList(event.getPlayer().getName() + ".BankContents");
    	Bank.setContents(Inv.toArray(new ItemStack[0]));
    	event.getPlayer().updateInventory();
    		}else if(plugin.getPlayerConfig(event.getPlayer().getName()).contains(event.getPlayer().getName() + ".BankSize")){
    	    	Inventory Bank = Bukkit.createInventory(event.getPlayer(), plugin.getPlayerConfig(event.getPlayer().getName()).getInt(event.getPlayer().getName() + ".BankSize"), event.getPlayer().getName() + " Storage Crate");	
    	    	event.getPlayer().openInventory(Bank);	
    		}else{
    	    	Inventory Bank = Bukkit.createInventory(event.getPlayer(), 18, event.getPlayer().getName() + " Storage Crate");	
    	    	event.getPlayer().openInventory(Bank);
    	        List<ItemStack> Inv = (List<ItemStack>) plugin.getPlayerConfig(event.getPlayer().getName()).getList(event.getPlayer().getName() + ".BankContents");
    	    	Bank.setContents(Inv.toArray(new ItemStack[0]));
    	    	event.getPlayer().updateInventory();
    		}
    	}else if(((Player)event.getRightClicked()).getDisplayName().contains("Accountant")){
    		event.getPlayer().sendMessage("�7This feature is still under development!");
    	}else if(((Player)event.getRightClicked()).getDisplayName().contains("PvE Master")){
    		HashMap<String, Integer> Kills = plugin.getTopSlayersMonthly();
    		HashMap<Integer, String> BackKills = plugin.getBackTopSlayersMonthly();
    		String bestPlayer5 = "";
    		int bestScore5 = 0;
    		String bestPlayer4 = "";
    		int bestScore4 = 0;
    		String bestPlayer3 = "";
    		int bestScore3 = 0;
    		String bestPlayer2 = "";
    		int bestScore2 = 0;
    		String bestPlayer = "";
    		int bestScore = 0;
    		for (int i : Kills.values()) {
    			if (i > bestScore) {
    			bestScore = i;
    			bestPlayer = BackKills.get(i);
    			}
    			}
    			Kills.remove(bestPlayer);
    			for (int i : Kills.values()) {
    			if (i > bestScore2) {
    			bestScore2 = i;
    			bestPlayer2 = BackKills.get(i);
    			}
    			}
    			Kills.remove(bestPlayer2);
    			for (int i : Kills.values()) {
    			if (i > bestScore3) {
    			bestScore3 = i;
    			bestPlayer3 = BackKills.get(i);
    			}
    			}
    			Kills.remove(bestPlayer3);
    			for (int i : Kills.values()) {
    			if (i > bestScore4) {
    			bestScore4 = i;
    			bestPlayer4 = BackKills.get(i);
    			}
    			}
    			Kills.remove(bestPlayer4);
    			for (int i : Kills.values()) {
    			if (i > bestScore5) {
    			bestScore5 = i;
    			bestPlayer5 = BackKills.get(i);
    			}
    			}
    		event.getPlayer().sendMessage("�9---===---�bTop 5 Slayers�9---===---");
    		event.getPlayer().sendMessage("�a1: �e" + bestPlayer + " with " + bestScore + " kill(s)");
    		event.getPlayer().sendMessage("�a2: �e" + bestPlayer2 + " with " +  bestScore2 + " kill(s)");
    		event.getPlayer().sendMessage("�a3: �e" + bestPlayer3 + " with " +  bestScore3 + " kill(s)");
    		event.getPlayer().sendMessage("�a4: �e" + bestPlayer4 + " with " +  bestScore4 + " kill(s)");
    		event.getPlayer().sendMessage("�a5: �e" + bestPlayer5 + " with " +  bestScore5 + " kill(s)");
    	}else if(((Player) event.getRightClicked()).getDisplayName().contains("PvP Master")){
    		HashMap<String, Integer> Kills = plugin.getTopMurderersMonthly();
    		HashMap<Integer, String> BackKills = plugin.getBackTopMurderersMonthly();
    		String bestPlayer5 = "";
    		int bestScore5 = 0;
    		String bestPlayer4 = "";
    		int bestScore4 = 0;
    		String bestPlayer3 = "";
    		int bestScore3 = 0;
    		String bestPlayer2 = "";
    		int bestScore2 = 0;
    		String bestPlayer = "";
    		int bestScore = 0;
    		for (int i : Kills.values()) {
    			if (i > bestScore) {
    			bestScore = i;
    			bestPlayer = BackKills.get(i);
    			}
    			}
    			Kills.remove(bestPlayer);
    			for (int i : Kills.values()) {
    			if (i > bestScore2) {
    			bestScore2 = i;
    			bestPlayer2 = BackKills.get(i);
    			}
    			}
    			Kills.remove(bestPlayer2);
    			for (int i : Kills.values()) {
    			if (i > bestScore3) {
    			bestScore3 = i;
    			bestPlayer3 = BackKills.get(i);
    			}
    			}
    			Kills.remove(bestPlayer3);
    			for (int i : Kills.values()) {
    			if (i > bestScore4) {
    			bestScore4 = i;
    			bestPlayer4 = BackKills.get(i);
    			}
    			}
    			Kills.remove(bestPlayer4);
    			for (int i : Kills.values()) {
    			if (i > bestScore5) {
    			bestScore5 = i;
    			bestPlayer5 = BackKills.get(i);
    			}
    			}
    		event.getPlayer().sendMessage("�9---===---�bTop 5 Murderers�9---===---");
    		event.getPlayer().sendMessage("�a1: �e" + bestPlayer + " with " + bestScore + " kill(s)");
    		event.getPlayer().sendMessage("�a2: �e" + bestPlayer2 + " with " +  bestScore2 + " kill(s)");
    		event.getPlayer().sendMessage("�a3: �e" + bestPlayer3 + " with " +  bestScore3 + " kill(s)");
    		event.getPlayer().sendMessage("�a4: �e" + bestPlayer4 + " with " +  bestScore4 + " kill(s)");
    		event.getPlayer().sendMessage("�a5: �e" + bestPlayer5 + " with " +  bestScore5 + " kill(s)");
    	}
    	}
    	}
    }

}
