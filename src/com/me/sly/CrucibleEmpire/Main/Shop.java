package com.me.sly.CrucibleEmpire.Main;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Shop {
	private String title = "";
	private int slots = 0;
	private HashMap<ItemStack, Integer> ItemsAndSlots = new HashMap<ItemStack, Integer>();
	private HashMap<Integer, ItemStack> ItemsAndSlotsB = new HashMap<Integer, ItemStack>();
	private HashMap<ItemStack, Integer> ItemsAndPrices = new HashMap<ItemStack, Integer>();

	public Shop(String title, int slots, HashMap<ItemStack, Integer> ItemsAndSlots, HashMap<ItemStack, Integer> ItemsAndPrices){

	this.title = title;
	this.slots = slots;
	this.ItemsAndSlots = ItemsAndSlots;
	this.ItemsAndPrices = ItemsAndPrices;
	for(ItemStack i : this.ItemsAndSlots.keySet()){
	this.ItemsAndSlotsB.put(this.ItemsAndSlots.get(i), i);
	}
	}
	public void showShopToPlayer(Player p){
	Inventory i = Bukkit.createInventory(p, this.slots, this.title);
	Set<ItemStack> Items = ItemsAndPrices.keySet();
	HashMap<ItemStack, Integer> NewItems = new HashMap<ItemStack, Integer>();
	for(ItemStack is : Items){
	if(is.hasItemMeta() && is.getItemMeta().hasLore()){
		int slot = 0;
	ItemMeta im = is.getItemMeta();
	List<String> lore = im.getLore();
	lore.add("");
	lore.add("�7Price: �6" + ItemsAndPrices.get(is) + "g");
	im.setLore(lore);
	slot = ItemsAndSlots.get(is);
	is.setItemMeta(im);
	NewItems.put(is, slot);
	}else{
	}
	}
	for(ItemStack is : NewItems.keySet()){
	i.setItem(NewItems.get(is), is);
	}
	p.openInventory(i);
	}
}
