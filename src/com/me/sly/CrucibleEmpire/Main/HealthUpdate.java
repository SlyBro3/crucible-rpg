package com.me.sly.CrucibleEmpire.Main;

import me.confuser.barapi.BarAPI;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.me.sly.resources.BetterItems;

public class HealthUpdate implements Listener {
	public static ceMain plugin;
    public HealthUpdate(ceMain instance) {
        plugin = instance;
    }
	@EventHandler
	public void ArmorHP2(final PlayerJoinEvent event){
		Player player = event.getPlayer();
	        player.setMaxHealth(plugin.Health(event.getPlayer())); 
	        barHealth(player);
	}	@EventHandler
	public static void ArmorHP3(final PlayerRespawnEvent event){
		final Player player = event.getPlayer();
        event.getPlayer().getInventory().setItem(9, BetterItems.DisplayNameAndLore(Material.DOUBLE_PLANT, 1, "�e�lBalances:", "�c�lInventory: �b�o" + plugin.invBalance(event.getPlayer()), "�c�lBank: �b�o" + plugin.economy.getBalance(event.getPlayer().getName())));
		event.getPlayer().getInventory().addItem(BetterItems.DisplayNameAndLore(Material.IRON_SWORD, 1, "�bBasic Sword", "�3" + plugin.rand(20, 35) + " - " + plugin.rand(35, 55) + " Damage", "�7Untransferable"));
		event.getPlayer().getInventory().addItem(BetterItems.DisplayNameAndLore(Material.GLOWSTONE_DUST, 1, "�bAttribute Dust", "�7Untransferable"));
		event.getPlayer().getInventory().setBoots(BetterItems.DisplayNameAndLore(Material.IRON_BOOTS, 1, "�bBasic Boots", "�3HP: " + plugin.rand(400, 700), "�cHealth Regen: 15/s", "�7Untransferable"));
		event.getPlayer().getInventory().setLeggings(BetterItems.DisplayNameAndLore(Material.IRON_LEGGINGS, 1, "�bBasic Leggings", "�3HP: " + plugin.rand(600, 1000), "�cHealth Regen: 26/s", "�7Untransferable"));
		event.getPlayer().getInventory().setChestplate(BetterItems.DisplayNameAndLore(Material.IRON_CHESTPLATE, 1, "�bBasic Chestplate", "�3HP: " + plugin.rand(600, 1000), "�cHealth Regen: 29/s", "�7Untransferable"));
		event.getPlayer().getInventory().setHelmet(BetterItems.DisplayNameAndLore(Material.IRON_HELMET, 1, "�bBasic Helmet", "�3HP: " + plugin.rand(400, 700), "�cHealth Regen: 17/s", "�7Untransferable"));
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
			public void run(){
   	        player.setMaxHealth(plugin.Health(event.getPlayer())); 
   	        barHealth(player);
		}}, 2L);
	}

	
	public static void barHealth(Player player){
	        BarAPI.setMessage(player, "�c�lHP: " + ((int)((CraftPlayer)player).getHealth()) + " / " +  ((int)((CraftPlayer)player).getMaxHealth()));
	        BarAPI.setHealth(player,  (float) (((CraftPlayer)player).getHealth() / ((CraftPlayer)player).getMaxHealth())  * 100);
	}
}
