package com.me.sly.CrucibleEmpire.Main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.me.sly.CrucibleEmpire.YMLfiles.SpawnerYML;
import com.me.sly.resources.BetterItems;
@SuppressWarnings("deprecation")
public class MainCMD implements CommandExecutor, Listener{
	public static ceMain plugin;
    public MainCMD(ceMain instance) {
        plugin = instance;
    }
	@SuppressWarnings({ "static-access", "unchecked"})
	public boolean onCommand(final CommandSender sender, Command cmd, String cmdL, String[] args){
		if(sender instanceof Player){
			if(cmdL.equalsIgnoreCase("roll")){
				try{
				if(args.length == 1){
					if(Integer.parseInt(args[0]) < 1000000){
					Bukkit.broadcastMessage(ChatColor.DARK_GRAY + sender.getName() + ChatColor.GRAY + " has rolled a " + "�l�n" + plugin.rand(0, Integer.parseInt(args[0]) + 1) + ChatColor.GRAY + " out of " + "�l�n" + args[0]);
				}
				}
		}catch(NumberFormatException e){
		sender.sendMessage("�cYou can only roll numbers!");
		}
			}else if(cmdL.equalsIgnoreCase("FakeEnchant")){
			if(((Player)sender).hasPermission("cRPG.FakeEnchant")){
				((Player) sender).setItemInHand(plugin.addGlow(((Player) sender).getItemInHand()));
			}
		}else if(cmdL.equalsIgnoreCase("Untradeable")){
			if(sender.hasPermission("cRPG.Untradeable")){
			ItemStack item = ((Player) sender).getItemInHand();
			ItemMeta itemMeta = item.getItemMeta();
			List<String> itemLore = itemMeta.getLore();
			itemLore.add(ChatColor.GRAY + "Untradeable");
			itemMeta.setLore(itemLore);
			item.setItemMeta(itemMeta);
			sender.sendMessage(ChatColor.BLUE + "Your item is now untradeable.");
			}
		}else if(cmdL.equalsIgnoreCase("ShutUpAll")){
			if(sender.hasPermission("cRPG.ShutUpAll")){
				if(plugin.ShutUpAll == false){
					plugin.ShutUpAll = true;
					sender.sendMessage("�7You have �cmuted �7the entire server");
				}else{
					plugin.ShutUpAll = false;
					sender.sendMessage("�7You have �aunmuted �7the entire server");
				}
			}
		}else if(cmdL.equalsIgnoreCase("players")){
			int s = Bukkit.getServer().getOnlinePlayers().length;
			int r = Bukkit.getServer().getMaxPlayers();
			sender.sendMessage(ChatColor.BLUE + "Online: " + ChatColor.DARK_AQUA + "" + s + " / " + r);
		}else if(cmdL.equalsIgnoreCase("HideAll")){
			if(sender.hasPermission("cRPG.HideAll"))
			for(Player s : Bukkit.getOnlinePlayers()){
				((Player) sender).hidePlayer(s);
			}
			
		}else if(cmdL.equalsIgnoreCase("UpgradeBank")){
			if(plugin.getPlayerConfig(sender.getName()).getInt(sender.getName() + ".BankSize") == 18){
				if(!JumbledCrap.Upgrade1.contains(sender.getName())){
				sender.sendMessage("�9�lCurrent Bank Size: �918");
				sender.sendMessage("�c�lNew Bank Size: �c27");
				sender.sendMessage("�e�l�m=======================");
				sender.sendMessage("�aThis upgrade will cost you 5,000 gold");
				sender.sendMessage("�aWould you like to confirm?");
				sender.sendMessage("�aType Y to confirm or N to decline");
				JumbledCrap.Upgrade1.add(sender.getName());	
				}
				}else if(plugin.getPlayerConfig(sender.getName()).getInt(sender.getName() + ".BankSize") == 27){
					if(!JumbledCrap.Upgrade2.contains(sender.getName())){
					sender.sendMessage("�9�lCurrent Bank Size: �927");
					sender.sendMessage("�c�lNew Bank Size: �c36");
					sender.sendMessage("�e�l�m=======================");
					sender.sendMessage("�aThis upgrade will cost you 15,000 gold");
					sender.sendMessage("�aWould you like to confirm?");
					sender.sendMessage("�aType Y to confirm or N to decline");
					JumbledCrap.Upgrade2.add(sender.getName());	
					}
					}else if(plugin.getPlayerConfig(sender.getName()).getInt(sender.getName() + ".BankSize") == 36){
						if(!JumbledCrap.Upgrade3.contains(sender.getName())){
						sender.sendMessage("�9�lCurrent Bank Size: �936");
						sender.sendMessage("�c�lNew Bank Size: �c45");
						sender.sendMessage("�e�l�m=======================");
						sender.sendMessage("�aThis upgrade will cost you 25,000 gold");
						sender.sendMessage("�aWould you like to confirm?");
						sender.sendMessage("�aType Y to confirm or N to decline");
						JumbledCrap.Upgrade3.add(sender.getName());		
					}
						}else if(plugin.getPlayerConfig(sender.getName()).getInt(sender.getName() + ".BankSize") == 45){
							if(!JumbledCrap.Upgrade4.contains(sender.getName())){
						}
							sender.sendMessage("�9�lCurrent Bank Size: �945");
							sender.sendMessage("�c�lNew Bank Size: �c54");
							sender.sendMessage("�e�l�m=======================");
							sender.sendMessage("�aThis upgrade will cost you 50,000 gold");
							sender.sendMessage("�aWould you like to confirm?");
							sender.sendMessage("�aType Y to confirm or N to decline");
							JumbledCrap.Upgrade4.add(sender.getName());	
						}else{
							sender.sendMessage("�a---Bank Upgrade---");
							sender.sendMessage("�cNo more bank upgrades available!");
						}
		}else if(cmdL.equalsIgnoreCase("OpenBank")){
			if(sender.hasPermission("cRPG.OpenBank")){
			if(args.length == 0){
		    	Inventory Bank = Bukkit.createInventory(((Player)sender), ((int) plugin.getPlayerConfig(sender.getName()).get(sender.getName() + ".BankSize")), sender.getName() + " Bank Chest");	
		    	((Player) sender).openInventory(Bank);
		        List<ItemStack> Inv = (List<ItemStack>) plugin.getPlayerConfig(sender.getName()).getList(sender.getName() + ".BankContents");
		    	Bank.setContents(Inv.toArray(new ItemStack[0]));
		    	((Player) sender).updateInventory();
			}else if(args.length == 1){
				Player target = Bukkit.getServer().getPlayer(args[0]);
				if(target != null){
			    	Inventory Bank = Bukkit.createInventory(((Player)sender), ((int) plugin.getPlayerConfig(target.getName()).get(target.getName() + ".BankSize")), target.getName() + " Bank Chest");	
			    	((Player) sender).openInventory(Bank);
			        List<ItemStack> Inv = (List<ItemStack>) plugin.getPlayerConfig(target.getName()).getList(target.getName() + ".BankContents");
			    	Bank.setContents(Inv.toArray(new ItemStack[0]));
			    	((Player) sender).updateInventory();	
				}else if(plugin.getPlayerConfig(args[0]).contains(args[0] + ".BankContents")){
			    	Inventory Bank = Bukkit.createInventory(((Player)sender), ((int) plugin.getPlayerConfig(args[0]).get(args[0] + ".BankSize")), args[0] + " Bank Chest");	
			    	((Player) sender).openInventory(Bank);
			        List<ItemStack> Inv = (List<ItemStack>) plugin.getPlayerConfig(args[0]).getList(args[0] + ".BankContents");
			    	Bank.setContents(Inv.toArray(new ItemStack[0]));
			    	((Player) sender).updateInventory();

				}else{
					sender.sendMessage(ChatColor.GOLD + "Player not found!");
				}
			}
			}
		}else if(cmdL.equalsIgnoreCase("WipeBank")){
			ArrayList<ItemStack[]> Wipe = new ArrayList<ItemStack[]>();
			if(sender.isOp()){
			if(args.length == 0){
		    	((Player) sender).sendMessage("�4Not enough arguments!");
			}else if(args.length == 1){
				Player target = Bukkit.getServer().getPlayer(args[0]);
				if(target != null){
					plugin.getPlayerConfig(target.getName()).set(target.getName() + ".BankContents", Wipe);	
				}else if(plugin.getPlayerConfig(args[0]).contains(args[0] + ".BankContents")){
					plugin.getPlayerConfig(args[0]).set(args[0] + ".BankContents", Wipe);		
				}else{
					sender.sendMessage(ChatColor.GOLD + "Player not found!");
				}
			}
			}
		}else if(cmdL.equalsIgnoreCase("ceReload")){
			if(sender.isOp()){
				plugin.reloadConfig();
				SpawnerYML.reloadCustomConfig();
				
				for(String g : plugin.getConfig().getStringList("Server.AllPlayers")){
			        File playerFile = new File(plugin.getDataFolder() + File.separator + "Users" + File.separator + g + ".yml");
			        FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);
		            try {
						fc.save(playerFile);
					} catch (IOException e) {
					}
				}
			}
		}else if(cmdL.equalsIgnoreCase("Particles")){
			@SuppressWarnings("unused")
			Inventory toggler = Bukkit.createInventory((InventoryHolder) sender, 18, "Toggle Particles!");
			sender.sendMessage("�4�lComing Soon!");
		}else if(cmdL.equalsIgnoreCase("toggleDebug")){
			sender.sendMessage("�4�lComing Soon!");
			HashMap<ItemStack, Integer> IAP = new HashMap<ItemStack, Integer>();
			HashMap<ItemStack, Integer> IAS = new HashMap<ItemStack, Integer>();
			IAS.put(BetterItems.DisplayNameAndLore(Material.STONE, 1, "STONE", "SWAG"), 5);
			IAP.put(BetterItems.DisplayNameAndLore(Material.STONE, 1, "STONE", "SWAG"), 2150);
			Shop s = new Shop("SWAG", 9, IAS, IAP);
			s.showShopToPlayer((Player) sender);
		}else if(cmdL.equalsIgnoreCase("Trade")){
			sender.sendMessage("�4�lComing Soon!");
		}else if(cmdL.equalsIgnoreCase("Duel")){
			sender.sendMessage("�4�lComing Soon!");
		}else if(cmdL.equalsIgnoreCase("Color")){
			sender.sendMessage("�4�lComing Soon!");
		}else if(cmdL.equalsIgnoreCase("Morph")){
			sender.sendMessage("�4�lComing Soon!");
		}
			
		}
			if(cmdL.equalsIgnoreCase("sendMessage")){
				if(sender.hasPermission("cRPG.sendmessage") || sender instanceof ConsoleCommandSender){
				if(args.length > 1){
			        StringBuilder sb = new StringBuilder();
			        for(int i = 1; i < args.length; i++) {
			            if (i > 0) sb.append(" ");
			            sb.append(args[i]);
			        }
			        if(Bukkit.getPlayer(args[0]) != null){
			        	Player t = Bukkit.getPlayer(args[0]);
			        	t.sendMessage(sb.toString().replaceAll("&", "�"));
			        }
				}
				}
		}else if(cmdL.equalsIgnoreCase("ceHeal")){
			if(sender.isOp() || sender instanceof ConsoleCommandSender){
			if(args.length == 1){
				Player target = Bukkit.getServer().getPlayer(args[0]);
				if(target != null){
					target.setHealth(((CraftPlayer)target).getMaxHealth());
				}
			}
			}
		}else if(cmdL.equalsIgnoreCase("Wipe")){
				if(sender.getName().equals("SlyBro3") || sender instanceof ConsoleCommandSender){
					if(args.length == 1){
						Player pl = Bukkit.getPlayer(args[0]);
						if(pl != null){
					        File playerFile = new File(plugin.getDataFolder() + File.separator + "Users" + File.separator + pl.getName() + ".yml");
					        FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);
							pl.kickPlayer("�4�lCorrupted Data Files�c�o, Kicked from server to prevent further damage �e- �e�nCrucible");
					File pData = new File("world/players/" + pl.getName() + ".dat");
					pData.delete();
					fc.set(pl.getName(), "");
					fc.set(pl.getName().toUpperCase(), "");
						}else if(plugin.getConfig().contains(args[0] + ".BankContents")){
					        File playerFile = new File(plugin.getDataFolder() + File.separator + "Users" + File.separator + args[0] + ".yml");
					        FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);
							File pData = new File("world/players/" + args[0] + ".dat");
							pData.delete();
							fc.set(args[0], "");
							fc.set(args[0].toUpperCase(), "");
						}
					}
			        File playerFile = new File(plugin.getDataFolder() + File.separator + "Users" + File.separator + args[0]+ ".yml");
			        FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);
		            try {
						fc.save(playerFile);
					} catch (IOException e) {
					}
				}else{
					sender.sendMessage("Nope.");
				}
			}else if(cmdL.equalsIgnoreCase("PushWorld")){
				@SuppressWarnings("unused")
				File f = new File(plugin.getDataFolder() + ".WorldFiles", "r.0.0.mca");
				
			}
		return false;
	}
}
