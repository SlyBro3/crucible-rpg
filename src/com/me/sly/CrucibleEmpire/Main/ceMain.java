package com.me.sly.CrucibleEmpire.Main;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import me.confuser.barapi.BarAPI;
import net.milkbowl.vault.economy.Economy;
import net.minecraft.server.v1_7_R4.NBTTagCompound;
import net.minecraft.server.v1_7_R4.NBTTagList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_7_R4.inventory.CraftItemStack;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.me.sly.CrucibleEmpire.Mining.FatiugeAndBreaks;
import com.me.sly.CrucibleEmpire.MobSpawns.GradeAMobSpawns;
import com.me.sly.CrucibleEmpire.MobSpawns.GradeBMobSpawns;
import com.me.sly.CrucibleEmpire.MobSpawns.GradeCMobSpawns;
import com.me.sly.CrucibleEmpire.MobSpawns.GradeDMobSpawns;
import com.me.sly.CrucibleEmpire.YMLfiles.PlayerYML;
import com.me.sly.CrucibleEmpire.YMLfiles.SpawnerYML;
import com.me.sly.resources.BetterItems;
import com.me.sly.resources.MyXpBar;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

public class ceMain extends JavaPlugin{
	public static ceMain plugin;
	public final MainCMD Cmd = new MainCMD(this);
	public final Money M = new Money(this);
	public final JumbledCrap JC = new JumbledCrap(this);
	public final GradeAMobSpawns MS = new GradeAMobSpawns(this);
	public final Damages WG = new Damages(this);
	public final HealthUpdate HU = new HealthUpdate(this);
	public final Scrolls S = new Scrolls(this);
	public final GradeDMobSpawns GS = new GradeDMobSpawns(this);
	public final GradeCMobSpawns GC = new GradeCMobSpawns(this);
	public final GradeBMobSpawns GB = new GradeBMobSpawns(this);
	public final GradeAMobSpawns GA = new GradeAMobSpawns(this);
	public final LevelChecker LC = new LevelChecker(this);
	public final FatiugeAndBreaks  Mine = new FatiugeAndBreaks(this);
	public final Repair Repair = new Repair(this);
	public final SpawnerYML syml = new SpawnerYML(this);
	public final PlayerYML pyml = new PlayerYML(this);
	public Inventory ClassSelect;
	public HashMap<Integer, Integer> mxbh = new HashMap<Integer, Integer>();
	public static ceMain getPlugin(){
		return plugin;
	}
	@Override
	public void onDisable(){
		for(Entity e : Bukkit.getWorld("world").getEntities()){
			if(e instanceof LivingEntity && !(e instanceof Player)){
			e.remove();
			}
		}
}
	boolean ShutUpAll = false;
	public MyXpBar mxb; 
	public WorldGuardPlugin  wg = null;
	@SuppressWarnings("deprecation")
	public void updatePlayersHealth(){
		String format = plugin.getConfig().getString("HealthBarFormat");
		for(Player p : Bukkit.getOnlinePlayers()){
			int min = (int) ((CraftPlayer)p).getHealth();
			int max = (int) ((CraftPlayer)p).getMaxHealth();
			format = format.replaceAll("%min%", String.valueOf(min)).replaceAll("%max%", String.valueOf(max));
			BarAPI.setMessage(p, format);
			double percent = 0.0;
			percent = (min/max)*100;
			BarAPI.setHealth(p, (float) percent);
		}
	}
	@Override
	public void onEnable(){
		for(Entity e : Bukkit.getWorld("world").getEntities()){
			if(e instanceof LivingEntity && !(e instanceof Player)){
			e.remove();
			}
		}
		
		this.saveTimer();
		getCommand("Roll").setExecutor(new MainCMD(this));
		getCommand("UpgradeBank").setExecutor(new MainCMD(this));
		getCommand("FakeEnchant").setExecutor(new MainCMD(this));
		getCommand("Untradeable").setExecutor(new MainCMD(this));
		getCommand("ShutUpAll").setExecutor(new MainCMD(this));
		getCommand("Players").setExecutor(new MainCMD(this));
		getCommand("HideAll").setExecutor(new MainCMD(this));
		getCommand("OpenBank").setExecutor(new MainCMD(this));
		getCommand("WipeBank").setExecutor(new MainCMD(this));
		getCommand("sendMessage").setExecutor(new MainCMD(this));
		getCommand("ceHeal").setExecutor(new MainCMD(this));
		getCommand("ceRELOAD").setExecutor(new MainCMD(this));
		getCommand("WL").setExecutor(new MainCMD(this));
		getCommand("Particles").setExecutor(new MainCMD(this));
		getCommand("ToggleDebug").setExecutor(new MainCMD(this));
		getCommand("Wipe").setExecutor(new MainCMD(this));
		getCommand("Trade").setExecutor(new MainCMD(this));
		getCommand("Duel").setExecutor(new MainCMD(this));
		getCommand("Color").setExecutor(new MainCMD(this));
		getCommand("Morph").setExecutor(new MainCMD(this));
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(JC, this);
		pm.registerEvents(Cmd, this);
		pm.registerEvents(M, this);
		pm.registerEvents(MS, this);
		pm.registerEvents(WG, this);
		pm.registerEvents(HU, this);
		pm.registerEvents(S, this);
		pm.registerEvents(GS, this);
		pm.registerEvents(GC, this);
		pm.registerEvents(GB, this);
		pm.registerEvents(GA, this);
		pm.registerEvents(LC, this);
		pm.registerEvents(Mine, this);
		pm.registerEvents(Repair, this);
		pm.registerEvents(new MobSpawning(this), this);
		getConfig().options().copyDefaults(true);
		setupEconomy();
		ShutUpAll = false;
	    for(int i = 0; i < 400; i++){
	    	if(i != 0){
	    	if(i == 1){
	    		mxbh.put(1, 20);
	    	}else{
	    	mxbh.put(i, (int) (mxbh.get(i-1) / .95));
	    	}
	    	}
	    }
	    mxb = new MyXpBar(mxbh);
	    wg = getWorldGuard();
		tryMobSpawn();
		new BukkitRunnable(){
			public void run(){
				updatePlayersHealth();
			}
		}.runTaskTimer(this, 1, 1);
		
	    new BukkitRunnable(){
			@SuppressWarnings("deprecation")
			public void run(){
	    		if(Bukkit.getServer().getOnlinePlayers() != null){
	    		for(Player p : Bukkit.getServer().getOnlinePlayers()){
	    			File playerFile = new File(getDataFolder() + File.separator + "Users" + File.separator + p.getName() + ".yml");
	    			FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);
	    			if(p.hasMetadata("HITSOMEONEP")){
    				nub(p);
	    			}
	    			if(p.isOp()){
	    				for(@SuppressWarnings("unused") Player o : Bukkit.getServer().getOnlinePlayers()){
	    					try {
	    					} catch (Exception e) {
								e.printStackTrace();
							}
	    				}
	    			}
	    			if(p != null){
	    			p.setSaturation(1);
	    	        
	    			if(p.getInventory().getBoots() != null){
	    				p.getInventory().getBoots().setDurability((short) 0);
	    			}
	    			if(p.getInventory().getLeggings() != null){
	    				p.getInventory().getLeggings().setDurability((short)0);
	    			}
	    			if(p.getInventory().getItemInHand().hasItemMeta() && p.getInventory().getItemInHand().getItemMeta().hasLore() && p.getInventory().getItemInHand().getItemMeta().getLore().get(0).contains("Damage")){
	    				p.getItemInHand().setDurability((short)0);		
	    			}
	    			if(p.getInventory().getChestplate() != null){
	    				p.getInventory().getChestplate().setDurability((short)0);
	    			}
	    			if(p.getInventory().getHelmet() != null){
	    				p.getInventory().getHelmet().setDurability((short)0);
	    		}
	    			}
	    			regenMana(p);
					int r = ((int)Math.ceil(((roundTwoDecimals(fc.getInt(p.getName() + ".Mana.Mana")) / roundTwoDecimals(fc.getInt(p.getName() + ".Mana.MaxMana")) * 100) * .2)));
	    			p.setFoodLevel(r);
	    			regenerate(p);
		   	        p.setMaxHealth(Health(p));
	   	        	boolean isSpell6 = false;
	   	        	boolean isSpell7 = false;
	   	        	boolean isSpell8 = false;
		   	        if(p.getInventory().getItem(6) != null){
		   	        	if(p.getInventory().getItem(6).hasItemMeta() && p.getInventory().getItem(6).getItemMeta().hasLore()){
		   	        		if(p.getInventory().getItem(6).getItemMeta().getLore().contains("§9Spell")){
		   	        			isSpell6 = true;
		   	        		}
		   	        	}
		   	        }
		   	        	if(!isSpell6){
		   	        p.getInventory().setItem(6, BetterItems.DisplayName(Material.STAINED_GLASS_PANE, 1, "§9Spell Slot 1"));
		   	        	}
		   	        if(p.getInventory().getItem(7) != null){
		   	        	if(p.getInventory().getItem(7).hasItemMeta() && p.getInventory().getItem(7).getItemMeta().hasLore()){
		   	        		if(p.getInventory().getItem(7).getItemMeta().getLore().contains("§9Spell")){
		   	        			isSpell7 = true;
		   	        		}
		   	        	}
		   	        }
		   	        	if(!isSpell7){
		   	        p.getInventory().setItem(7, BetterItems.DisplayName(Material.STAINED_GLASS_PANE, 1, "§9Spell Slot 2"));
		   	        	}
		   	        if(p.getInventory().getItem(8) != null){
		   	        	if(p.getInventory().getItem(8).hasItemMeta() && p.getInventory().getItem(8).getItemMeta().hasLore()){
		   	        		if(p.getInventory().getItem(8).getItemMeta().getLore().contains("§9Spell")){
		   	        			isSpell8 = true;
		   	        		}
		   	        	}
		   	        }
		   	        	if(!isSpell8){
		   	        p.getInventory().setItem(8, BetterItems.DisplayName(Material.STAINED_GLASS_PANE, 1, "§9Spell Slot 3"));
		   	        	}
		   	        p.updateInventory();
		   	        if(p.getExp() > 1){
		   	        	p.giveExp(1);
		   	        	p.giveExp(-1);
		   	        }
	    		}
	    		}
	    	}
	    }.runTaskTimer(this, 20, 20);
	    }
	private WorldGuardPlugin getWorldGuard() {
	    Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");
	 
	    // WorldGuard may not be loaded
	    if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
	        return null; // Maybe you want throw an exception instead
	    }
	 
	    return (WorldGuardPlugin) plugin;
	}
	public void resetTimer(){
		Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
			public void run(){
				FiveMinuteResetter();
			}}, 138000);//138000
	}
	public void tryMobSpawn(){
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
			public void run(){
				if(rand(0, 21) == 10){
					MobSpawning.tryMobSpawn();
				}
			}}, 20, 20);
	}
	public void regenerate(Player p){
		if(!p.isDead()){
		if(!Damages.inCombat.contains(p.getName())){
			if(((CraftPlayer)p).getMaxHealth() < this.HPRegenAmount(p) + ((CraftPlayer)p).getHealth()){
				p.setHealth(((CraftPlayer)p).getMaxHealth());
			}else{
		
		p.setHealth(((CraftPlayer)p).getHealth() + this.HPRegenAmount(p));
			}}
	}}
	public void regenMana(Player p){
		File playerFile = new File(this.getDataFolder() + File.separator + "Users" + File.separator + p.getName() + ".yml");
		FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);
		fc.set(p.getName() + ".Mana.MaxMana", 100 + p.getLevel());
		if(fc.contains(p.getName() + ".Mana.Mana")){
			int Manas = fc.getInt(p.getName() + ".Mana.Mana");
			if(Manas + this.ManaRegenAmount(p) + 10 < fc.getInt(p.getName() + ".Mana.MaxMana")){
			fc.set(p.getName() + ".Mana.Mana", Manas + this.ManaRegenAmount(p) + 10);
		}else{
			fc.set(p.getName() + ".Mana.Mana", fc.getInt(p.getName() + ".Mana.MaxMana"));
		}
			}
        try {
			fc.save(playerFile);
		} catch (IOException e) {
		}
	}
	int sec = 0;
	public void FiveMinuteResetter(){
		sec = 300;
		Bukkit.broadcastMessage("§e§l[Reset] §b5 minutes until the next server §c§nrestart§b.");
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
			@SuppressWarnings("deprecation")
			public void run(){	
				sec--;
				if(sec == 60){
					Bukkit.broadcastMessage("§e§l[Reset] §b1 minute until the next server §c§nrestart§b.");
				}else if(sec == 10){
					Bukkit.broadcastMessage("§e§l[Reset] §b10 seconds until the next server §c§nrestart§b.");
				}else if(sec == 5){
					Bukkit.broadcastMessage("§e§l[Reset] §b5 seconds until the next server §c§nrestart§b.");
				}else if(sec == 4){
					Bukkit.broadcastMessage("§e§l[Reset] §b4 seconds until the next server §c§nrestart§b.");
				}else if(sec == 3){
					for(String g : Damages.inCombat){
						Damages.inCombat.remove(g);
					}
					Bukkit.broadcastMessage("§e§l[Reset] §b3 seconds until the next server §c§nrestart§b.");
				}else if(sec == 2){
					for(String g : Damages.inCombat){
						Damages.inCombat.remove(g);
					}
					Bukkit.broadcastMessage("§e§l[Reset] §b2 seconds until the next server §c§nrestart§b.");
				}else if(sec == 1){
					for(String g : Damages.inCombat){
						Damages.inCombat.remove(g);
					}
					Bukkit.broadcastMessage("§e§l[Reset] §b1 second until the next server §c§nrestart§b.");
					Bukkit.getWorld("world").save();
				}else if(sec == 0){
					for(String g : Damages.inCombat){
						Damages.inCombat.remove(g);
					}
					Bukkit.broadcastMessage("§e§l[Reset] §bThe server is restarting!");
					for(Player s : Bukkit.getOnlinePlayers()){
						s.kickPlayer("§e§l§o§n§m§k~~~~~~~~§c§l§o§n§mCOOL MESSAGE FOR RESTARTING THE SERVER§e§l§o§n§m§k~~~~~~~~");
					}
					Bukkit.getServer().shutdown();
				}
			}}, 20, 20);
	}
	public void saveTimer(){
		new BukkitRunnable(){
			public void run(){
		Bukkit.getWorld("world").save();
		}
	}.runTaskTimer(this, 20 * 30, 20 * 30);
	}
    public void nub(Player p){
		p.removeMetadata("HITSOMEONEP", this);
    }
	public FileConfiguration getSpawnerConfig(){
		return SpawnerYML.getCustomConfig();
	}
	public FileConfiguration getPlayerConfig(String PlayerName){
        File playerFile = new File(this.getDataFolder() + File.separator + "Users" + File.separator + PlayerName + ".yml");
        FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);
		return fc;
	}
	public void savePlayerConfig(String PlayerName){
        File playerFile = new File(this.getDataFolder() + File.separator + "Users" + File.separator + PlayerName + ".yml");
        FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);
        try {
			fc.save(playerFile);
		} catch (IOException e) {}
	}
	public void refreshWorld(){
		File f = new File(this.getDataFolder() + File.separator + "WorldCopy");
		File[] files = f.listFiles();
		System.out.println("REFRESHING");
		Bukkit.unloadWorld("world", false);
		for(File fi : new File("world" + File.separator + "region").listFiles()){
			fi.delete();
		}
		for(File fi : files){
			fi.renameTo(new File("world" + File.separator + "region" + File.separator + fi.getName()));
		}
	}
	 public static ItemStack removeGlow(ItemStack item) {
	        net.minecraft.server.v1_7_R4.ItemStack nmsStack = null;
	        CraftItemStack craftStack = null;
	       
	        craftStack = getCraftVersion(item);
	        nmsStack = CraftItemStack.asNMSCopy(craftStack);
	       
	        NBTTagCompound tag = null;
	        if (!nmsStack.hasTag()) return item;
	        tag = nmsStack.getTag();
	        tag.remove("ench");
	        nmsStack.setTag(tag);
	       
	        return craftStack;
	    }
	   
	    private static CraftItemStack getCraftVersion(ItemStack stack) {
	        if (stack instanceof CraftItemStack)
	            return (CraftItemStack) stack;
	        else if (stack != null)
	            return CraftItemStack.asCraftCopy(stack);
	        else
	            return null;
	    }
	public Economy economy = null;
	private boolean setupEconomy()
    {RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();}
        return (economy != null);}
	
	public int rand(int min, int max) {
        return min + (new Random()).nextInt(max-min);
    }
	public static ItemStack addGlow(ItemStack item){ 
		  net.minecraft.server.v1_7_R4.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
		  NBTTagCompound tag = null;
		  if (!nmsStack.hasTag()) {
		      tag = new NBTTagCompound();
		      nmsStack.setTag(tag);
		  }
		  if (tag == null) tag = nmsStack.getTag();
		  NBTTagList ench = new NBTTagList();
		  tag.set("ench", ench);
		  nmsStack.setTag(tag);
		  return CraftItemStack.asCraftMirror(nmsStack);
		}
	public double drand(double min, double max) {
        return min + (new Random()).nextDouble();
    }
    public Double roundTwoDecimals(double d) {
    	 
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Double.valueOf(twoDForm.format(d));
 
    }
    public int invBalance(Player player){
        File playerFile = new File(this.getDataFolder() + File.separator + "Users" + File.separator + player.getName() + ".yml");
        FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);
    	return fc.getInt(player.getName() + ".InventoryBalance");
    }
    public void setBalance(Player player, Integer newBal){
        File playerFile = new File(this.getDataFolder() + File.separator + "Users" + File.separator + player.getName() + ".yml");
        FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);
    	fc.set(player.getName() + ".InventoryBalance", newBal);
        try {
			fc.save(playerFile);
		} catch (IOException e) {
		}
        }
    public void addBalance(Player player, Integer newBal){
        File playerFile = new File(this.getDataFolder() + File.separator + "Users" + File.separator + player.getName() + ".yml");
        FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);
    	fc.set(player.getName() + ".InventoryBalance", this.invBalance(player) + newBal);
        try {
			fc.save(playerFile);
		} catch (IOException e) {
		}
        }
    public void withdrawBalance(Player player, Integer newBal){
        File playerFile = new File(this.getDataFolder() + File.separator + "Users" + File.separator + player.getName() + ".yml");
        FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);
    	fc.set(player.getName() + ".InventoryBalance", this.invBalance(player) - newBal);
        try {
			fc.save(playerFile);
		} catch (IOException e) {
		}
        }
	public HashMap<Integer, String> getBackTopMurderersMonthly(){
		HashMap<Integer, String> Top = new HashMap<Integer, String>();
		HashMap<Integer, String> BackKills = new HashMap<Integer, String>();
		HashMap<String, Integer> Kills = new HashMap<String, Integer>();
		for(String g : this.getConfig().getStringList("Server.AllPlayers")){
			Kills.put(g,this.getPlayerConfig(g).getInt(g + ".Murdering.Kills"));
			BackKills.put(this.getPlayerConfig(g).getInt(g + ".Murdering.Kills"), g);
		}
		String bestPlayer5 = "";
		int bestScore5 = -1;
		String bestPlayer4 = "";
		int bestScore4 = -1;
		String bestPlayer3 = "";
		int bestScore3 = -1;
		String bestPlayer2 = "";
		int bestScore2 = -1;
		String bestPlayer = "";
		int bestScore = -1;
		for (int i : Kills.values()) {
		if (i > bestScore) {
		bestScore = i;
		bestPlayer = BackKills.get(i);
		}
		}
		Kills.remove(bestPlayer);
		for (int i : Kills.values()) {
		if (i > bestScore2) {
		bestScore2 = i;
		bestPlayer2 = BackKills.get(i);
		}
		}
		Kills.remove(bestPlayer2);
		for (int i : Kills.values()) {
		if (i > bestScore3) {
		bestScore3 = i;
		bestPlayer3 = BackKills.get(i);
		}
		}
		Kills.remove(bestPlayer3);
		for (int i : Kills.values()) {
		if (i > bestScore4) {
		bestScore4 = i;
		bestPlayer4 = BackKills.get(i);
		}
		}
		Kills.remove(bestPlayer4);
		for (int i : Kills.values()) {
		if (i > bestScore5) {
		bestScore5 = i;
		bestPlayer5 = BackKills.get(i);
		}
		}
		Kills.remove(bestPlayer5);
		Top.put(bestScore, bestPlayer);
		Top.put(bestScore2, bestPlayer2);
		Top.put(bestScore3, bestPlayer3);
		Top.put(bestScore4, bestPlayer4);
		Top.put(bestScore5, bestPlayer5);
    	return Top;
    }
	public HashMap<String, Integer> getTopMurderersMonthly(){
		HashMap<String, Integer> Top = new HashMap<String, Integer>();
		final HashMap<Integer, String> BackKills = new HashMap<Integer, String>();
		final HashMap<String, Integer> Kills = new HashMap<String, Integer>();
		for(String g : this.getConfig().getStringList("Server.AllPlayers")){
			Kills.put(g, this.getPlayerConfig(g).getInt(g + ".Murdering.Kills"));
			BackKills.put(this.getPlayerConfig(g).getInt(g + ".Murdering.Kills"), g);
		}
		String bestPlayer5 = "";
		int bestScore5 = -1;
		String bestPlayer4 = "";
		int bestScore4 = -1;
		String bestPlayer3 = "";
		int bestScore3 = -1;
		String bestPlayer2 = "";
		int bestScore2 = -1;
		String bestPlayer = "";
		int bestScore = -1;
		for (int i : Kills.values()) {
		if (i > bestScore) {
		bestScore = i;
		bestPlayer = BackKills.get(i);
		}
		}
		Kills.remove(bestPlayer);
		for (int i : Kills.values()) {
		if (i > bestScore2) {
		bestScore2 = i;
		bestPlayer2 = BackKills.get(i);
		}
		}
		Kills.remove(bestPlayer2);
		for (int i : Kills.values()) {
		if (i > bestScore3) {
		bestScore3 = i;
		bestPlayer3 = BackKills.get(i);
		}
		}
		Kills.remove(bestPlayer3);
		for (int i : Kills.values()) {
		if (i > bestScore4) {
		bestScore4 = i;
		bestPlayer4 = BackKills.get(i);
		}
		}
		Kills.remove(bestPlayer4);
		for (int i : Kills.values()) {
		if (i > bestScore5) {
		bestScore5 = i;
		bestPlayer5 = BackKills.get(i);
		}
		}
		Kills.remove(bestPlayer5);
		Top.put(bestPlayer, bestScore);
		Top.put(bestPlayer2, bestScore2);
		Top.put(bestPlayer3, bestScore3);
		Top.put(bestPlayer4, bestScore4);
		Top.put(bestPlayer5, bestScore5);
    	return Top;
    }
	public HashMap<Integer, String> getBackTopSlayersMonthly(){
		HashMap<Integer, String> Top = new HashMap<Integer, String>();
		HashMap<Integer, String> BackKills = new HashMap<Integer, String>();
		HashMap<String, Integer> Kills = new HashMap<String, Integer>();
		for(String g : this.getConfig().getStringList("Server.AllPlayers")){
			Kills.put(g,this.getPlayerConfig(g).getInt(g + ".Slaying.Kills"));
			BackKills.put(this.getPlayerConfig(g).getInt(g + ".Slaying.Kills"), g);
		}
		String bestPlayer5 = "";
		int bestScore5 = 0;
		String bestPlayer4 = "";
		int bestScore4 = 0;
		String bestPlayer3 = "";
		int bestScore3 = 0;
		String bestPlayer2 = "";
		int bestScore2 = 0;
		String bestPlayer = "";
		int bestScore = 0;
		for (int i : Kills.values()) {
		if (i > bestScore) {
		bestScore = i;
		bestPlayer = BackKills.get(i);
		}
		}
		Kills.remove(bestPlayer);
		for (int i : Kills.values()) {
		if (i > bestScore2) {
			if(bestPlayer != BackKills.get(i) && bestPlayer2 != BackKills.get(i)
			&& bestPlayer3 != BackKills.get(i) && bestPlayer4 != BackKills.get(i)){
			bestScore2 = i;
		bestPlayer2 = BackKills.get(i);
		}
		}
		}
		Kills.remove(bestPlayer2);
		for (int i : Kills.values()) {
		if (i > bestScore3) {
			if(bestPlayer != BackKills.get(i) && bestPlayer2 != BackKills.get(i)
			&& bestPlayer3 != BackKills.get(i) && bestPlayer4 != BackKills.get(i)){
		bestScore3 = i;
		bestPlayer3 = BackKills.get(i);
		}
		}
		}
		Kills.remove(bestPlayer3);
		for (int i : Kills.values()) {
		if (i > bestScore4) {
			if(bestPlayer != BackKills.get(i) && bestPlayer2 != BackKills.get(i)
			&& bestPlayer3 != BackKills.get(i) && bestPlayer4 != BackKills.get(i)){
		bestScore4 = i;
		bestPlayer4 = BackKills.get(i);
		}
		}
		}
		Kills.remove(bestPlayer4);
		for (int i : Kills.values()) {
		if (i > bestScore5) {
			if(bestPlayer != BackKills.get(i) && bestPlayer2 != BackKills.get(i)
			&& bestPlayer3 != BackKills.get(i) && bestPlayer4 != BackKills.get(i)){
		bestScore5 = i;
		bestPlayer5 = BackKills.get(i);
		}
		}
		}
		Kills.remove(bestPlayer5);
		Top.put(bestScore, bestPlayer);
		Top.put(bestScore2, bestPlayer2);
		Top.put(bestScore3, bestPlayer3);
		Top.put(bestScore4, bestPlayer4);
		Top.put(bestScore5, bestPlayer5);
    	return Top;
    }
	public HashMap<String, Integer> getTopSlayersMonthly(){
		HashMap<String, Integer> Top = new HashMap<String, Integer>();
		final HashMap<Integer, String> BackKills = new HashMap<Integer, String>();
		final HashMap<String, Integer> Kills = new HashMap<String, Integer>();
		for(String g : this.getConfig().getStringList("Server.AllPlayers")){
			Kills.put(g, this.getPlayerConfig(g).getInt(g + ".Slaying.Kills"));
			BackKills.put(this.getPlayerConfig(g).getInt(g + ".Slaying.Kills"), g);
		}
		String bestPlayer5 = "";
		int bestScore5 = 0;
		String bestPlayer4 = "";
		int bestScore4 = 0;
		String bestPlayer3 = "";
		int bestScore3 = 0;
		String bestPlayer2 = "";
		int bestScore2 = 0;
		String bestPlayer = "";
		int bestScore = 0;
		for (int i : Kills.values()) {
		if (i > bestScore) {
		bestScore = i;
		bestPlayer = BackKills.get(i);
		}
		}
		Kills.remove(bestPlayer);
		for (int i : Kills.values()) {
		if (i > bestScore2) {
			if(bestPlayer != BackKills.get(i) && bestPlayer2 != BackKills.get(i)
			&& bestPlayer3 != BackKills.get(i) && bestPlayer4 != BackKills.get(i)){
			bestScore2 = i;
		bestPlayer2 = BackKills.get(i);
		}
		}
		}
		Kills.remove(bestPlayer2);
		for (int i : Kills.values()) {
		if (i > bestScore3) {
			if(bestPlayer != BackKills.get(i) && bestPlayer2 != BackKills.get(i)
			&& bestPlayer3 != BackKills.get(i) && bestPlayer4 != BackKills.get(i)){
		bestScore3 = i;
		bestPlayer3 = BackKills.get(i);
		}
		}
		}
		Kills.remove(bestPlayer3);
		for (int i : Kills.values()) {
		if (i > bestScore4) {
			if(bestPlayer != BackKills.get(i) && bestPlayer2 != BackKills.get(i)
			&& bestPlayer3 != BackKills.get(i) && bestPlayer4 != BackKills.get(i)){
		bestScore4 = i;
		bestPlayer4 = BackKills.get(i);
		}
		}
		}
		Kills.remove(bestPlayer4);
		for (int i : Kills.values()) {
		if (i > bestScore5) {
			if(bestPlayer != BackKills.get(i) && bestPlayer2 != BackKills.get(i)
			&& bestPlayer3 != BackKills.get(i) && bestPlayer4 != BackKills.get(i)){
		bestScore5 = i;
		bestPlayer5 = BackKills.get(i);
		}
		}
		}
		Kills.remove(bestPlayer5);
		Top.put(bestPlayer, bestScore);
		Top.put(bestPlayer2, bestScore2);
		Top.put(bestPlayer3, bestScore3);
		Top.put(bestPlayer4, bestScore4);
		Top.put(bestPlayer5, bestScore5);
    	return Top;
    }
	public double Health(Player player){
		int dur1 = 0;
		int dur2 = 0;
		int dur3 = 0;
		int dur4 = 0;
		int dur5 = 75;
		double HP = 50;
		CharSequence d = "";
		CharSequence a = "";
		CharSequence f = "";
		CharSequence h = "";
        if(player.getInventory().getBoots() != null){
        	if(player.getInventory().getBoots().hasItemMeta()){
        	if(player.getInventory().getBoots().getItemMeta().hasLore()){
        		if(!player.getInventory().getBoots().getItemMeta().getLore().contains("§4Needs Charge!")){
   			 List<String> hlore = player.getInventory().getBoots().getItemMeta().getLore();
   			 f = hlore.get(0).subSequence(6,hlore.get(0).length());
   			 dur1 = Integer.parseInt((String) f);
        	}
        }
        	}
        }
        if(player.getInventory().getLeggings() != null){
        	if(player.getInventory().getLeggings().hasItemMeta()){
        	if(player.getInventory().getLeggings().getItemMeta().hasLore()){
        		if(!player.getInventory().getLeggings().getItemMeta().getLore().contains("§4Needs Charge!")){
   			 List<String> slore = player.getInventory().getLeggings().getItemMeta().getLore();
   			 h = slore.get(0).subSequence(6,slore.get(0).length());
   			 dur2 = Integer.parseInt((String) h);
        }
        	}
        }
        }
        if(player.getInventory().getChestplate() != null){
        	if(player.getInventory().getChestplate().hasItemMeta()){
        	if(player.getInventory().getChestplate().getItemMeta().hasLore()){
        		if(!player.getInventory().getChestplate().getItemMeta().getLore().contains("§4Needs Charge!")){
   			 List<String> alore = player.getInventory().getChestplate().getItemMeta().getLore();
   			 d = alore.get(0).subSequence(6,alore.get(0).length());
   			 dur3 = Integer.parseInt((String) d);
        }
        	}
        }
        }
        if(player.getInventory().getHelmet() != null){
        	if(player.getInventory().getHelmet().hasItemMeta()){
        	if(player.getInventory().getHelmet().getItemMeta().hasLore()){
        		if(!player.getInventory().getHelmet().getItemMeta().getLore().contains("§4Needs Charge!")){
   			 List<String> dlore = player.getInventory().getHelmet().getItemMeta().getLore();
   			 a = dlore.get(0).subSequence(6,dlore.get(0).length());
   			 dur4 = Integer.parseInt((String) a);
        }
        }
        	}
        }
   			 HP = dur1 + dur2 + dur3 + dur4 + dur5;
   			 double percent = this.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".FORTITUDE");
   			 percent = percent / 3;
   			 percent = percent / 100;
   			 percent = 1.0 - percent;
   			 HP = (HP / percent);
   			 return HP;
	}
	public double HPRegenAmount(Player player){
		int dur1 = 0;
		int dur2 = 0;
		int dur3 = 0;
		int dur4 = 0;
		int PlaceHolder = 10;
		int Return = 0;
		String d = "";
		String a = "";
		String f = "";
		String h = "";
        if(player.getInventory().getBoots() != null){
        	if(player.getInventory().getBoots().hasItemMeta()){
        	if(player.getInventory().getBoots().getItemMeta().hasLore()){
        		if(player.getInventory().getBoots().getItemMeta().getLore().size() > 1){
        		if(player.getInventory().getBoots().getItemMeta().getLore().get(1).contains("Health Regen")){
   			 List<String> hlore = player.getInventory().getBoots().getItemMeta().getLore();
   			 f = hlore.get(1);
   			 f = f.substring(16, f.length() - 2);
   			 dur1 = Integer.parseInt(f);
        	}
        }
        	}
        }
        }
        if(player.getInventory().getLeggings() != null){
        	if(player.getInventory().getLeggings().hasItemMeta()){
        	if(player.getInventory().getLeggings().getItemMeta().hasLore()){
        		if(player.getInventory().getLeggings().getItemMeta().getLore().size() > 1){
        		if(player.getInventory().getLeggings().getItemMeta().getLore().get(1).contains("Health Regen")){
   			 List<String> slore = player.getInventory().getLeggings().getItemMeta().getLore();
   			 d = slore.get(1);
   			 d = d.substring(16, d.length() - 2);
   			 dur2 = Integer.parseInt(d);
        }
        }
        	}
        	}
        }
        if(player.getInventory().getChestplate() != null){
        	if(player.getInventory().getChestplate().hasItemMeta()){
        	if(player.getInventory().getChestplate().getItemMeta().hasLore()){
        		if(player.getInventory().getChestplate().getItemMeta().getLore().size() > 1){
        		if(player.getInventory().getChestplate().getItemMeta().getLore().get(1).contains("Health Regen")){
   			 List<String> alore = player.getInventory().getChestplate().getItemMeta().getLore();
   			 h = alore.get(1);
   			 h = h.substring(16, h.length() - 2);
   			 dur3 = Integer.parseInt(h);
        }
        		}
        }
        }
        }
        if(player.getInventory().getHelmet() != null){
        if(player.getInventory().getHelmet().hasItemMeta()){
        if(player.getInventory().getHelmet().getItemMeta().hasLore()){
    		if(player.getInventory().getHelmet().getItemMeta().getLore().size() > 1){
        if(player.getInventory().getHelmet().getItemMeta().getLore().get(1).contains("Health Regen")){
   		List<String> dlore = player.getInventory().getHelmet().getItemMeta().getLore();
   		a = dlore.get(1);
   		a = a.substring(16, a.length() - 2);
   		dur4 = Integer.parseInt((String) a);
        }
        }
        }
        }
        }
   			 Return = dur1 + dur2 + dur3 + dur4 + PlaceHolder;
   			 int r = this.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".ENDURANCE");
   			 double s = r / 100;
   			 s = s / 2;
   			 s = 1.0 - s;
   			 Return = (int) (Return / s);
   			 return Return;
	}
	public double ManaRegenAmount(Player player){
		int dur1 = 0;
		int dur2 = 0;
		int dur3 = 0;
		int dur4 = 0;
		int PlaceHolder = 0;
		int Return = 0;
		String d = "";
		String a = "";
		String f = "";
		String h = "";
        if(player.getInventory().getBoots() != null){
        	if(player.getInventory().getBoots().hasItemMeta()){
        	if(player.getInventory().getBoots().getItemMeta().hasLore()){
        		if(player.getInventory().getBoots().getItemMeta().getLore().size() > 1){
        		if(player.getInventory().getBoots().getItemMeta().getLore().get(1).contains("Mana Regen")){
   			 List<String> hlore = player.getInventory().getBoots().getItemMeta().getLore();
   			 f = hlore.get(1);
   			 f = f.replaceAll("§5Mana Regen +", "").replaceAll("/s", "");
   			 dur1 = Integer.parseInt(f);
        	}
        }
        	}
        }
        }
        if(player.getInventory().getLeggings() != null){
        	if(player.getInventory().getLeggings().hasItemMeta()){
        	if(player.getInventory().getLeggings().getItemMeta().hasLore()){
        		if(player.getInventory().getLeggings().getItemMeta().getLore().size() > 1){
        		if(player.getInventory().getLeggings().getItemMeta().getLore().get(1).contains("Mana Regen")){
   			 List<String> slore = player.getInventory().getLeggings().getItemMeta().getLore();
   			 d = slore.get(1);
   			 d = d.replaceAll("§5Mana Regen +", "").replaceAll("/s", "");
   			 dur2 = Integer.parseInt(d);
        }
        }
        	}
        	}
        }
        if(player.getInventory().getChestplate() != null){
        	if(player.getInventory().getChestplate().hasItemMeta()){
        	if(player.getInventory().getChestplate().getItemMeta().hasLore()){
        		if(player.getInventory().getChestplate().getItemMeta().getLore().size() > 1){
        		if(player.getInventory().getChestplate().getItemMeta().getLore().get(1).contains("Mana Regen")){
   			 List<String> alore = player.getInventory().getChestplate().getItemMeta().getLore();
   			 h = alore.get(1);
   			 h = h.replaceAll("§5Mana Regen +", "").replaceAll("/s", "");
   			 dur3 = Integer.parseInt(h);
        }
        		}
        }
        }
        }
        if(player.getInventory().getHelmet() != null){
        if(player.getInventory().getHelmet().hasItemMeta()){
        if(player.getInventory().getHelmet().getItemMeta().hasLore()){
    		if(player.getInventory().getHelmet().getItemMeta().getLore().size() > 1){
        if(player.getInventory().getHelmet().getItemMeta().getLore().get(1).contains("Mana Regen")){
   		List<String> dlore = player.getInventory().getHelmet().getItemMeta().getLore();
   		a = dlore.get(1);
   		a = a.replaceAll("§5Mana Regen +", "").replaceAll("/s", "");
   		dur4 = Integer.parseInt((String) a);
        }
        }
        }
        }
        }
   			 Return = dur1 + dur2 + dur3 + dur4 + PlaceHolder;
   			 int r = this.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".ENDURANCE");
   			 double s = r / 100;
   			 s = s / 2;
   			 s = 1.0 - s;
   			 Return = (int) (Return / s);
   			 return Return;
	}
	public double getDamageReduced(Player player, double dmg)
    {
        PlayerInventory inv = player.getInventory();

        double red = 0.0;
        double DMG = dmg;
        if(inv.getHelmet() != null){
            ItemStack helmet = inv.getHelmet();
        if(helmet.getType() == Material.LEATHER_HELMET){
        	red = red + 0.04;
        }else if(helmet.getType() == Material.GOLD_HELMET){
        	red = red + 0.08;
        }else if(helmet.getType() == Material.CHAINMAIL_HELMET){
        	red = red + 0.08;
        }else if(helmet.getType() == Material.IRON_HELMET){
        	red = red + 0.08;
        }else if(helmet.getType() == Material.DIAMOND_HELMET){
        	red = red + 0.12;
        }
        }
        //
        if(inv.getBoots() != null){
            ItemStack boots = inv.getBoots();
        if(boots.getType() == Material.LEATHER_BOOTS){
        	red = red + 0.04;
        }else if(boots.getType() == Material.GOLD_BOOTS){
        	red = red + 0.04;
        }else if(boots.getType() == Material.CHAINMAIL_BOOTS){
        	red = red + 0.04;
        }else if(boots.getType() == Material.IRON_BOOTS){
        	red = red + 0.08;
        }else if(boots.getType() == Material.DIAMOND_BOOTS){
        	red = red + 0.12;
        }
        }
        //
        if(inv.getLeggings() != null){
            ItemStack pants = inv.getLeggings();
        if(pants.getType() == Material.LEATHER_LEGGINGS){
        	red = red + 0.08;
    }   else if(pants.getType() == Material.GOLD_LEGGINGS){
    	red = red + 0.12;
    }   else if(pants.getType() == Material.CHAINMAIL_LEGGINGS){
    	red = red + 0.16;
    }   else if(pants.getType() == Material.IRON_LEGGINGS){
    	red = red + 0.20;
    }else if(pants.getType() == Material.DIAMOND_LEGGINGS){
    	red = red + 0.24;
        }}//
        if(inv.getChestplate() != null){
            ItemStack chest = inv.getChestplate();
        if(chest.getType() == Material.LEATHER_CHESTPLATE){
        	red = red + 0.12;
        }else if(chest.getType() == Material.GOLD_CHESTPLATE){
        	red = red + 0.20;
    }else if(chest.getType() == Material.CHAINMAIL_CHESTPLATE){
    	red = red + 0.20;
}     else if(chest.getType() == Material.IRON_CHESTPLATE){
	red = red + 0.24;
}       else if(chest.getType() == Material.DIAMOND_CHESTPLATE){
	red = red + 0.32;
}
        }
        red = 1.0 - red;
        if(red == 1.0){
        	DMG = dmg;
        }else{
        DMG = dmg / red;
        DMG = DMG*2;
        }
        return DMG;
    }
	public double getDamageReducedMob(LivingEntity ent, Double dmg)
    {
        ItemStack boots = ent.getEquipment().getBoots();
        ItemStack helmet = ent.getEquipment().getLeggings();
        ItemStack chest = ent.getEquipment().getChestplate();
        ItemStack pants = ent.getEquipment().getLeggings();
        double red = 0.0;
        double DMG = dmg;
        if(helmet != null){
        if(helmet.getType() == Material.LEATHER_HELMET){
        	red = red + 0.04;
        }else if(helmet.getType() == Material.GOLD_HELMET){
        	red = red + 0.08;
        }else if(helmet.getType() == Material.CHAINMAIL_HELMET){
        	red = red + 0.08;
        }else if(helmet.getType() == Material.IRON_HELMET){
        	red = red + 0.08;
        }else if(helmet.getType() == Material.DIAMOND_HELMET){
        	red = red + 0.12;
        }
        }
        //
        if(boots != null){
        if(boots.getType() == Material.LEATHER_BOOTS){
        	red = red + 0.04;
        }else if(boots.getType() == Material.GOLD_BOOTS){
        	red = red + 0.04;
        }else if(boots.getType() == Material.CHAINMAIL_BOOTS){
        	red = red + 0.04;
        }else if(boots.getType() == Material.IRON_BOOTS){
        	red = red + 0.08;
        }else if(boots.getType() == Material.DIAMOND_BOOTS){
        	red = red + 0.12;
        }
        }
        //
        if(pants != null){
        if(pants.getType() == Material.LEATHER_LEGGINGS){
        	red = red + 0.08;
    }   else if(pants.getType() == Material.GOLD_LEGGINGS){
    	red = red + 0.12;
    }   else if(pants.getType() == Material.CHAINMAIL_LEGGINGS){
    	red = red + 0.16;
    }   else if(pants.getType() == Material.IRON_LEGGINGS){
    	red = red + 0.20;
    }else if(pants.getType() == Material.DIAMOND_LEGGINGS){
    	red = red + 0.24;
        }}//
        if(chest != null){
        if(chest.getType() == Material.LEATHER_CHESTPLATE){
        	red = red + 0.12;
        }else if(chest.getType() == Material.GOLD_CHESTPLATE){
        	red = red + 0.20;
    }else if(chest.getType() == Material.CHAINMAIL_CHESTPLATE){
    	red = red + 0.20;
}     else if(chest.getType() == Material.IRON_CHESTPLATE){
	red = red + 0.24;
}       else if(chest.getType() == Material.DIAMOND_CHESTPLATE){
	red = red + 0.32;
}
        }
        red = 1.0 - red;
        if(red == 1.0){
        	DMG = dmg;
        }else{
        DMG = dmg / red;
        DMG = DMG*2;
        }
        return DMG;
    }
	public int Agility(Player player){
		int dur1 = 0;
		int dur2 = 0;
		int dur3 = 0;
		int dur4 = 0;
		int Return = 0;
        if(player.getInventory().getBoots() != null){
        	if(player.getInventory().getBoots().hasItemMeta()){
        	if(player.getInventory().getBoots().getItemMeta().hasLore()){
        		for(String g : player.getInventory().getBoots().getItemMeta().getLore()){
        			if(g.contains("Agility")){
        				String h = "";
        				h = g.replaceAll("§2Agility:", "").trim();
        				dur1 = Integer.parseInt(h);
        		}
        		}
        }
        }
        }
        if(player.getInventory().getLeggings() != null){
        	if(player.getInventory().getLeggings().hasItemMeta()){
        	if(player.getInventory().getLeggings().getItemMeta().hasLore()){
for(String g : player.getInventory().getLeggings().getItemMeta().getLore()){
	if(g.contains("Agility")){
		String h = "";
		h = g.replaceAll("§2Agility:", "").trim();
		dur2 = Integer.parseInt(h);
}
}
        }
        }
        }
        if(player.getInventory().getChestplate() != null){
        	if(player.getInventory().getChestplate().hasItemMeta()){
        	if(player.getInventory().getChestplate().getItemMeta().hasLore()){
        		for(String g : player.getInventory().getChestplate().getItemMeta().getLore()){
        			if(g.contains("Agility")){
        				String h = "";
        				h = g.replaceAll("§2Agility:", "").trim();
        				dur3 = Integer.parseInt(h);
        		}
        		}
        }
        }
        }
        if(player.getInventory().getHelmet() != null){
        	if(player.getInventory().getHelmet().hasItemMeta()){
        	if(player.getInventory().getHelmet().getItemMeta().hasLore()){
        		for(String g : player.getInventory().getHelmet().getItemMeta().getLore()){
        			if(g.contains("Agility")){
        				String h = "";
        				h = g.replaceAll("§2Agility:", "").trim();
        				dur4 = Integer.parseInt(h);
        		}
        		}
        }
        }
        }
   			 Return = dur1 + dur2 + dur3 + dur4;
   			 Return = (int) Math.floor(Return / 5);
   			 return Return;
	}
}
