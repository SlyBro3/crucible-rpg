package com.me.sly.CrucibleEmpire.Main;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.server.v1_7_R4.Item;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import com.me.sly.resources.ParticleEffects;

public class LevelChecker implements Listener{
public static ceMain plugin;
public LevelChecker(ceMain instance) {
    plugin = instance;
}

public ItemStack Dexterity = new ItemStack(Material.BOW, 1);
public ItemStack Strength = new ItemStack(Material.GOLD_AXE, 1);
public ItemStack Intelligence = new ItemStack(Material.BLAZE_ROD, 1);
public ItemStack Fortitude = new ItemStack(Material.GOLD_CHESTPLATE, 1);
public ItemStack Stability = new ItemStack(Material.GOLD_SWORD, 1);
public ItemStack Arcana = new ItemStack(Material.ENDER_PEARL, 1);
public ItemStack Hold = new ItemStack(Material.GLASS, 1);
public ItemStack Endurance = new ItemStack(Material.BLAZE_POWDER, 1);
public ItemStack Precision = new ItemStack(Material.GOLD_SPADE, 1);
public ItemStack PointsLeft = new ItemStack(Material.NETHER_STAR, 1);
public ItemStack PointsUsedTotal = new ItemStack(Material.DIAMOND, 1);
public ItemStack HelpBook = new ItemStack(Material.WRITTEN_BOOK, 1);
public Inventory LevelUp, LevelUp1;
public ItemMeta Meta;
public List<String> Lore = new ArrayList<String>();
public ItemStack PointPutterMeta(ItemStack item, String name){
	Lore.clear();
	Meta = null;
	item.setAmount(1);
	Meta = item.getItemMeta();
	Meta.setDisplayName(name);
	Lore.add(ChatColor.GRAY + "Click to Spend a Point on " + name.substring(4));
	Meta.setLore(Lore);
	item.setItemMeta(Meta);
	Lore.clear();
	Meta = null;
	return item;
}
public ItemStack PointsLeft(ItemStack item, Player player){
	int Points = plugin.getPlayerConfig(player.getName()).getInt(player.getName() + ".ExtraPoints");
	Lore.clear();
	Meta = null;
	try {
		  Field field = Item.class.getDeclaredField("maxStackSize");
		  field.setAccessible(true);
		  field.setInt(item, 127);
		 
		} catch (Exception e) {}
	item.setAmount(Points);
	Meta = item.getItemMeta();
	Meta.setDisplayName("�b�l" + Points + " Points left");
	Lore.add(ChatColor.GRAY + "You have " + Points + " Points");
	Lore.add(ChatColor.GRAY + "left to spend on skills");
	Meta.setLore(Lore);
	item.setItemMeta(Meta);
	Lore.clear();
	Meta = null;
	return item;
}
public ItemStack PointsUsed(ItemStack item, Player player){
	int Points = plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".ENDURANCE") + plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".DEXTERITY")
			+ plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".PRECISION") + plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".ARCANA")
			+ plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".INTELLIGENCE") + plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".FORTITUDE")
			+ plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".STABILITY") + plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".STRENGTH");
	Lore.clear();
	Meta = null;
	Meta = item.getItemMeta();
	Meta.setDisplayName("�3�l" + "" + Points + " Total Points used");
	Lore.add(ChatColor.GRAY + "You have used " + Points + " Points");
	Lore.add(ChatColor.GRAY + "total on all your skills");
	Meta.setLore(Lore);
	item.setItemMeta(Meta);
	Lore.clear();
	Meta = null;
	return item;
}
public ItemStack QuartzStuff(String skill, int Level){
	ItemStack item = new ItemStack(Material.QUARTZ, 1);
	Lore.clear();
	Meta = null;
	Meta = item.getItemMeta();
	Meta.setDisplayName("�e�lYou have a total of " + Level + " Points in " + skill);
	String Scales = "�7[";
	for(int i = 0; i < Level; i++){
	Scales = Scales + "�a|";
	}
	Scales = Scales + "�c||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||";
	for(int i = 0; i < Level; i++){
	Scales = Scales.substring(0, Scales.length()-1);
	}
	Scales = Scales + "�7]";
	Lore.add(Scales);
	Meta.setLore(Lore);
	item.setItemMeta(Meta);
	Lore.clear();
	Meta = null;
	return item;
}
public ItemStack DescBook(List<String> desc){
	ItemStack Book = new ItemStack(Material.BOOK, 1);
	Meta = null;
	Meta = Book.getItemMeta();
	Meta.setDisplayName("�6�lDescription:");
	Meta.setLore(desc);
	Book.setItemMeta(Meta);
	Meta = null;
	
	return Book;
	
}
@SuppressWarnings("static-access")
public void LevelUpScreen(final Player player, final int Level){
	final List<String> ArcanaDesc = new ArrayList<String>();
	final List<String> FortitudeDesc = new ArrayList<String>();
	final List<String> IntelligenceDesc = new ArrayList<String>();
	final List<String> DexterityDesc = new ArrayList<String>();
	final List<String> StrengthDesc = new ArrayList<String>();
	final List<String> StabilityDesc = new ArrayList<String>();
	final List<String> PrecisionDesc = new ArrayList<String>();
	final List<String> EnduranceDesc = new ArrayList<String>();
	new BukkitRunnable(){
		public void run(){
			ArcanaDesc.add("�bIncreases:");
			ArcanaDesc.add("�9-Maximum Mana");
			ArcanaDesc.add("�9-Magic Defence");
			FortitudeDesc.add("�cIncreases:");
			FortitudeDesc.add("�9-Base Defence");
			FortitudeDesc.add("�9-Max Health");
			IntelligenceDesc.add("�dIncreases:");
			IntelligenceDesc.add("�9-Stave Damage");
			IntelligenceDesc.add("�9-Spell Damage");
			DexterityDesc.add("�aIncreases:");
			DexterityDesc.add("�9-Bow Damage");
			DexterityDesc.add("�9-Base Agility");
			StrengthDesc.add("�8Increases:");
			StrengthDesc.add("�9-Axe Damage");
			StrengthDesc.add("�9-Base Critical Hit");
			StabilityDesc.add("�4Increases:");
			StabilityDesc.add("�9-Sword Damage");
			StabilityDesc.add("�9-Accuracy");
			PrecisionDesc.add("�9Increases:");
			PrecisionDesc.add("�9-Spear Damage");
			PrecisionDesc.add("�9-Armor Penetration");
			EnduranceDesc.add("�2Increases:");
			EnduranceDesc.add("�9-Health Regeneration");
			EnduranceDesc.add("�9-Mana Regeneration");
LevelUp = Bukkit.createInventory(player, 27, "Level Up!");
LevelUp1 = Bukkit.createInventory(player, 27, "Level Up!");
Dexterity = PointPutterMeta(Dexterity, "�a�lDexterity");
Strength = PointPutterMeta(Strength, "�8�lStrength");
Intelligence = PointPutterMeta(Intelligence, "�d�lIntelligence");
Fortitude = PointPutterMeta(Fortitude, "�c�lFortitude");
Stability = PointPutterMeta(Stability, "�4�lStability");
Arcana = PointPutterMeta(Arcana, "�b�lArcana");
Endurance = PointPutterMeta(Endurance, "�2�lEndurance");
Precision = PointPutterMeta(Precision, "�9�lPrecision");
	if(Level < 10){
	player.openInventory(LevelUp1);
	LevelUp1.setItem(0, PointsUsed(PointsUsedTotal, player));
	LevelUp1.setItem(1, plugin.addGlow(QuartzStuff("Arcana", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".ARCANA"))));
	LevelUp1.setItem(2, plugin.addGlow(QuartzStuff("Fortitude", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".FORTITUDE"))));
	LevelUp1.setItem(3, plugin.addGlow(QuartzStuff("Intelligence", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".INTELLIGENCE"))));
	LevelUp1.setItem(4, plugin.addGlow(QuartzStuff("Dexterity", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".DEXTERITY"))));
	LevelUp1.setItem(5, plugin.addGlow(QuartzStuff("Strength", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".STRENGTH"))));
	LevelUp1.setItem(6, plugin.addGlow(QuartzStuff("Stability", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".STABILITY"))));
	LevelUp1.setItem(7, plugin.addGlow(QuartzStuff("Precision", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".PRECISION"))));
	LevelUp1.setItem(8, plugin.addGlow(QuartzStuff("Endurance", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".ENDURANCE"))));
	LevelUp1.setItem(9, PointsLeft(PointsLeft, player));
	LevelUp1.setItem(10, Arcana);
	LevelUp1.setItem(11, Fortitude);
	LevelUp1.setItem(12, Intelligence);
	LevelUp1.setItem(13, Dexterity);
	LevelUp1.setItem(14, Strength);
	LevelUp1.setItem(15, Stability);
	LevelUp1.setItem(16, Precision);
	LevelUp1.setItem(17, Endurance);
	LevelUp1.setItem(18, HelpBook);
	LevelUp1.setItem(19, DescBook(ArcanaDesc));
	LevelUp1.setItem(20, DescBook(FortitudeDesc));
	LevelUp1.setItem(21, DescBook(IntelligenceDesc));
	LevelUp1.setItem(22, DescBook(DexterityDesc));
	LevelUp1.setItem(23, DescBook(StrengthDesc));
	LevelUp1.setItem(24, DescBook(StabilityDesc));
	LevelUp1.setItem(25, DescBook(PrecisionDesc));
	LevelUp1.setItem(26, DescBook(EnduranceDesc));
	}else{
	player.openInventory(LevelUp);
	LevelUp.setItem(0, PointsUsed(PointsUsedTotal, player));
	LevelUp.setItem(1, plugin.addGlow(QuartzStuff("Arcana", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".ARCANA"))));
	LevelUp.setItem(2, plugin.addGlow(QuartzStuff("Fortitude", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".FORTITUDE"))));
	LevelUp.setItem(3, plugin.addGlow(QuartzStuff("Intelligence", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".INTELLIGENCE"))));
	LevelUp.setItem(4, plugin.addGlow(QuartzStuff("Dexterity", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".DEXTERITY"))));
	LevelUp.setItem(5, plugin.addGlow(QuartzStuff("Strength", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".STRENGTH"))));
	LevelUp.setItem(6, plugin.addGlow(QuartzStuff("Stability", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".STABILITY"))));
	LevelUp.setItem(7, plugin.addGlow(QuartzStuff("Precision", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".PRECISION"))));
	LevelUp.setItem(8, plugin.addGlow(QuartzStuff("Endurance", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".ENDURANCE"))));
	LevelUp.setItem(9, PointsLeft(PointsLeft, player));
	LevelUp.setItem(10, Arcana);
	LevelUp.setItem(11, Fortitude);
	LevelUp.setItem(12, Intelligence);
	LevelUp.setItem(13, Dexterity);
	LevelUp.setItem(14, Strength);
	LevelUp.setItem(15, Stability);
	LevelUp.setItem(16, Precision);
	LevelUp.setItem(17, Endurance);
	LevelUp.setItem(18, HelpBook);
	LevelUp.setItem(19, DescBook(ArcanaDesc));
	LevelUp.setItem(20, DescBook(FortitudeDesc));
	LevelUp.setItem(21, DescBook(IntelligenceDesc));
	LevelUp.setItem(22, DescBook(DexterityDesc));
	LevelUp.setItem(23, DescBook(StrengthDesc));
	LevelUp.setItem(24, DescBook(StabilityDesc));
	LevelUp.setItem(25, DescBook(PrecisionDesc));
	LevelUp.setItem(26, DescBook(EnduranceDesc));
	}
		}}.runTaskLater(plugin, 1);
}
@SuppressWarnings("deprecation")
@EventHandler
public void LevelUp(PlayerLevelChangeEvent event){
	final Player player = event.getPlayer();
    File playerFile = new File(plugin.getDataFolder() + File.separator + "Users" + File.separator + player.getName() + ".yml");
    FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);
	if(event.getNewLevel() - 1 == event.getOldLevel()){
	fc.set(event.getPlayer().getName() + ".ExtraPoints", plugin.getPlayerConfig(event.getPlayer().getName()).getInt(event.getPlayer().getName() + ".ExtraPoints") + 2);
	}else{
		if(event.getNewLevel() - event.getOldLevel() < 0){
			
		}else{
		int p = event.getNewLevel() - event.getOldLevel();
		p = p * 2;
		fc.set(event.getPlayer().getName() + ".ExtraPoints", plugin.getPlayerConfig(event.getPlayer().getName()).getInt(event.getPlayer().getName() + ".ExtraPoints") + p);
		}
	}
    try {
		fc.save(playerFile);
	} catch (IOException e) {
	}
    player.sendMessage("�bYou have leveled up! �cLevel: " + player.getLevel() + " �bRight click with your glowstone dust to upgrade your stats");
	for(final Player p : Bukkit.getOnlinePlayers()){
		try {
			ParticleEffects.FIREWORKS_SPARK.sendToPlayer(p, event.getPlayer().getLocation().add(0, 2, 0), new Random().nextFloat(), new Random().nextFloat(), new Random().nextFloat(), 1, 50);
			} catch (Exception e) {
			e.printStackTrace();
		}
}
}
@EventHandler
public void ClickStat(InventoryClickEvent event){
	final Player player = (Player) event.getWhoClicked();
    File playerFile = new File(plugin.getDataFolder() + File.separator + "Users" + File.separator + player.getName() + ".yml");
    FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);
	ItemStack item = event.getCurrentItem();
	if(event.getInventory() instanceof PlayerInventory){
		if(item != null){
			if(item.hasItemMeta() && item.getItemMeta().hasLore()){
				if(item.getItemMeta().getLore().contains("�9Spell") || item.getType() == Material.STAINED_GLASS_PANE){
						event.setCancelled(true);
				}
			}
		}
	}
	if(event.getInventory().getTitle().contains("Level Up") || event.getInventory().getTitle().contains("Level Up") || event.getInventory().getTitle().contains("Spell")){
		if(item != null){
			event.setCancelled(true);
		player.updateInventory();
		if(plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName() + ".ExtraPoints")>0){
			this.LevelUpdate(player, player.getExpToLevel());
	if(item.getType() == Material.BOW){
		if(plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".DEXTERITY") == 100){
			player.sendMessage("�c�nYou have already maxed out this skill!");
		}else{
			player.sendMessage("�e�lYou have used 1 point on �c�l�nDexterity");
			fc.set(player.getName()+".ExtraPoints", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName() + ".ExtraPoints")-1);
		fc.set(player.getName().toUpperCase() + ".DEXTERITY", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".DEXTERITY") + 1);
        try {
			fc.save(playerFile);
		} catch (IOException e) {
		}
		}
	}else if(item.getType() == Material.GOLD_SPADE){
		if(plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".PRECISION") == 100){
			player.sendMessage("�c�nYou have already maxed out this skill!");
		}else{
			player.sendMessage("�e�lYou have used 1 point on �c�l�nPrecision");
			fc.set(player.getName()+".ExtraPoints", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName() + ".ExtraPoints")-1);
		fc.set(player.getName().toUpperCase() + ".PRECISION", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".PRECISION") + 1);
        try {
			fc.save(playerFile);
		} catch (IOException e) {
		}
		}
	}else if(item.getType() == Material.ENDER_PEARL){
		if(plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".ARCANA") == 100){
			player.sendMessage("�c�nYou have already maxed out this skill!");
		}else{
			player.sendMessage("�e�lYou have used 1 point on �c�l�nArcana");
			fc.set(player.getName()+".ExtraPoints", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName() + ".ExtraPoints")-1);
		fc.set(player.getName().toUpperCase() + ".ARCANA", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".ARCANA") + 1);
        try {
			fc.save(playerFile);
		} catch (IOException e) {
		}
		}
	}else if(item.getType() == Material.GOLD_CHESTPLATE){
		if(plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".FORTITUDE") == 100){
			player.sendMessage("�c�nYou have already maxed out this skill!");
		}else{
			player.sendMessage("�e�lYou have used 1 point on �c�l�nFortitude");
			fc.set(player.getName()+".ExtraPoints", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName() + ".ExtraPoints")-1);
		fc.set(player.getName().toUpperCase() + ".FORTITUDE", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".FORTITUDE") + 1);
        try {
			fc.save(playerFile);
		} catch (IOException e) {
		}
		}
	}else if(item.getType() == Material.BLAZE_ROD){
		if(plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".INTELLIGENCE") == 100){
			player.sendMessage("�c�nYou have already maxed out this skill!");
		}else{
			player.sendMessage("�e�lYou have used 1 point on �c�l�nIntelligence");
			fc.set(player.getName()+".ExtraPoints", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName() + ".ExtraPoints")-1);
		fc.set(player.getName().toUpperCase() + ".INTELLIGENCE", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".INTELLIGENCE") + 1);
        try {
			fc.save(playerFile);
		} catch (IOException e) {
		}
		}
	}else if(item.getType() == Material.BLAZE_POWDER){
		if(plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".ENDURANCE") == 100){
			player.sendMessage("�c�nYou have already maxed out this skill!");
		}else{
			player.sendMessage("�e�lYou have used 1 point on �c�l�nEndurance");
			fc.set(player.getName()+".ExtraPoints", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName() + ".ExtraPoints")-1);
		fc.set(player.getName().toUpperCase() + ".ENDURANCE", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".ENDURANCE") + 1);
        try {
			fc.save(playerFile);
		} catch (IOException e) {
		}
		}
	}else if(item.getType() == Material.GOLD_AXE){
		if(plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".STRENGTH") == 100){
			player.sendMessage("�c�nYou have already maxed out this skill!");
		}else{
			player.sendMessage("�e�lYou have used 1 point on �c�l�nStrength");
			fc.set(player.getName()+".ExtraPoints", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName() + ".ExtraPoints")-1);
		fc.set(player.getName().toUpperCase() + ".STRENGTH", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".STRENGTH") + 1);
        try {
			fc.save(playerFile);
		} catch (IOException e) {
		}
		}
	}else if(item.getType() == Material.GOLD_SWORD){
		if(plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".STABILITY") == 100){
			player.sendMessage("�c�nYou have already maxed out this skill!");
		}else{
			player.sendMessage("�e�lYou have used 1 point on �c�l�nStability");
			fc.set(player.getName()+".ExtraPoints", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName() + ".ExtraPoints")-1);
		fc.set(player.getName().toUpperCase() + ".STABILITY", plugin.getPlayerConfig(event.getWhoClicked().getName()).getInt(player.getName().toUpperCase() + ".STABILITY") + 1);
        try {
			fc.save(playerFile);
		} catch (IOException e) {
		}
		}
	}
	}else{
	player.closeInventory();player.sendMessage(ChatColor.YELLOW + "You have used all your skill points!");
	}

		}
	}else{
	}
}
@EventHandler
public void closeCheck(final InventoryCloseEvent event){
	if(event.getInventory().getTitle() == "Level Up!"){
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
			public void run(){
		event.getPlayer().getInventory().remove(Precision);
		event.getPlayer().getInventory().remove(Stability);
		event.getPlayer().getInventory().remove(Arcana);
		event.getPlayer().getInventory().remove(Intelligence);
		event.getPlayer().getInventory().remove(Dexterity);
		event.getPlayer().getInventory().remove(Strength);
		event.getPlayer().getInventory().remove(Endurance);
		Player player = ((Player)event.getPlayer());
		player.updateInventory();
			}}, 1L);
	}
}
public void LevelUpdate(final Player player, final int Level){
	final List<String> ArcanaDesc = new ArrayList<String>();
	final List<String> FortitudeDesc = new ArrayList<String>();
	final List<String> IntelligenceDesc = new ArrayList<String>();
	final List<String> DexterityDesc = new ArrayList<String>();
	final List<String> StrengthDesc = new ArrayList<String>();
	final List<String> StabilityDesc = new ArrayList<String>();
	final List<String> PrecisionDesc = new ArrayList<String>();
	final List<String> EnduranceDesc = new ArrayList<String>();
	Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
		@SuppressWarnings("static-access")
		public void run(){
			ArcanaDesc.add("�bIncreases:"); 
			ArcanaDesc.add("�9-Max Mana"); //50%
			ArcanaDesc.add("�9-Magic Defence");
			FortitudeDesc.add("�cIncreases:"); //DONE
			FortitudeDesc.add("�9-Base Defence");
			FortitudeDesc.add("�9-Max Health");
			IntelligenceDesc.add("�dIncreases:"); //IMPOSSIBRUH
			IntelligenceDesc.add("�9-Stave Damage");
			IntelligenceDesc.add("�9-Spell Damage");
			DexterityDesc.add("�aIncreases:"); //DONE
			DexterityDesc.add("�9-Bow Damage");
			DexterityDesc.add("�9-Base Agility");
			StrengthDesc.add("�8Increases:"); //DONE
			StrengthDesc.add("�9-Axe Damage");
			StrengthDesc.add("�9-Base Critical Hit");
			StabilityDesc.add("�4Increases:"); //DONE
			StabilityDesc.add("�9-Sword Damage");
			StabilityDesc.add("�9-Accuracy");
			PrecisionDesc.add("�9Increases:"); //DONE
			PrecisionDesc.add("�9-Spear Damage");
			PrecisionDesc.add("�9-Armor Penetration");
			EnduranceDesc.add("�2Increases:"); //DONE
			EnduranceDesc.add("�9-Health Regeneration");
			EnduranceDesc.add("�9-Mana Regeneration");
			LevelUp = Bukkit.createInventory(player, 27, "Level Up!");
			LevelUp1 = Bukkit.createInventory(player, 27, "Level Up!");
Dexterity = PointPutterMeta(Dexterity, "�a�lDexterity");
Strength = PointPutterMeta(Strength, "�8�lStrength");
Intelligence = PointPutterMeta(Intelligence, "�d�lIntelligence");
Fortitude = PointPutterMeta(Fortitude, "�c�lFortitude");
Stability = PointPutterMeta(Stability, "�4�lStability");
Arcana = PointPutterMeta(Arcana, "�b�lArcana");
Endurance = PointPutterMeta(Endurance, "�2�lEndurance");
Precision = PointPutterMeta(Precision, "�9�lPrecision");
	if(Level < 10){
		player.openInventory(LevelUp1);
	LevelUp1.setItem(0, PointsUsed(PointsUsedTotal, player));
	LevelUp1.setItem(1, plugin.addGlow(QuartzStuff("Arcana", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".ARCANA"))));
	LevelUp1.setItem(2, plugin.addGlow(QuartzStuff("Fortitude", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".FORTITUDE"))));
	LevelUp1.setItem(3, plugin.addGlow(QuartzStuff("Intelligence", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".INTELLIGENCE"))));
	LevelUp1.setItem(4, plugin.addGlow(QuartzStuff("Dexterity", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".DEXTERITY"))));
	LevelUp1.setItem(5, plugin.addGlow(QuartzStuff("Strength", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".STRENGTH"))));
	LevelUp1.setItem(6, plugin.addGlow(QuartzStuff("Stability", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".STABILITY"))));
	LevelUp1.setItem(7, plugin.addGlow(QuartzStuff("Precision", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".PRECISION"))));
	LevelUp1.setItem(8, plugin.addGlow(QuartzStuff("Endurance", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".ENDURANCE"))));
	LevelUp1.setItem(9, PointsLeft(PointsLeft, player));
	LevelUp1.setItem(10, Arcana);
	LevelUp1.setItem(11, Fortitude);
	LevelUp1.setItem(12, Intelligence);
	LevelUp1.setItem(13, Dexterity);
	LevelUp1.setItem(14, Strength);
	LevelUp1.setItem(15, Stability);
	LevelUp1.setItem(16, Precision);
	LevelUp1.setItem(17, Endurance);
	LevelUp1.setItem(18, HelpBook);
	LevelUp1.setItem(19, DescBook(ArcanaDesc));
	LevelUp1.setItem(20, DescBook(FortitudeDesc));
	LevelUp1.setItem(21, DescBook(IntelligenceDesc));
	LevelUp1.setItem(22, DescBook(DexterityDesc));
	LevelUp1.setItem(23, DescBook(StrengthDesc));
	LevelUp1.setItem(24, DescBook(StabilityDesc));
	LevelUp1.setItem(25, DescBook(PrecisionDesc));
	LevelUp1.setItem(26, DescBook(EnduranceDesc));
	}else{
		player.openInventory(LevelUp);
		LevelUp.setMaxStackSize(100);
	LevelUp.setItem(0, PointsUsed(PointsUsedTotal, player));
	LevelUp.setItem(1, plugin.addGlow(QuartzStuff("Arcana", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".ARCANA"))));
	LevelUp.setItem(2, plugin.addGlow(QuartzStuff("Fortitude", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".FORTITUDE"))));
	LevelUp.setItem(3, plugin.addGlow(QuartzStuff("Intelligence", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".INTELLIGENCE"))));
	LevelUp.setItem(4, plugin.addGlow(QuartzStuff("Dexterity", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".DEXTERITY"))));
	LevelUp.setItem(5, plugin.addGlow(QuartzStuff("Strength", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".STRENGTH"))));
	LevelUp.setItem(6, plugin.addGlow(QuartzStuff("Stability", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".STABILITY"))));
	LevelUp.setItem(7, plugin.addGlow(QuartzStuff("Precision", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".PRECISION"))));
	LevelUp.setItem(8, plugin.addGlow(QuartzStuff("Endurance", plugin.getPlayerConfig(player.getName()).getInt(player.getName().toUpperCase() + ".ENDURANCE"))));
	LevelUp.setItem(9, PointsLeft(PointsLeft, player));
	LevelUp.setItem(10, Arcana);
	LevelUp.setItem(11, Fortitude);
	LevelUp.setItem(12, Intelligence);
	LevelUp.setItem(13, Dexterity);
	LevelUp.setItem(14, Strength);
	LevelUp.setItem(15, Stability);
	LevelUp.setItem(16, Precision);
	LevelUp.setItem(17, Endurance);
	LevelUp.setItem(18, HelpBook);
	LevelUp.setItem(19, DescBook(ArcanaDesc));
	LevelUp.setItem(20, DescBook(FortitudeDesc));
	LevelUp.setItem(21, DescBook(IntelligenceDesc));
	LevelUp.setItem(22, DescBook(DexterityDesc));
	LevelUp.setItem(23, DescBook(StrengthDesc));
	LevelUp.setItem(24, DescBook(StabilityDesc));
	LevelUp.setItem(25, DescBook(PrecisionDesc));
	LevelUp.setItem(26, DescBook(EnduranceDesc));
	}
	}}, 1);
}
int S = 0;
@SuppressWarnings("deprecation")
@EventHandler
public void glowClick(final PlayerInteractEvent event){
	if(event.getItem() != null && event.getItem().getType() == Material.GLOWSTONE_DUST){
		this.LevelUpScreen(event.getPlayer(), event.getPlayer().getLevel());
		for(final Player p : Bukkit.getOnlinePlayers()){
				S = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){
				public void run(){
				try {
					if(event.getPlayer().getOpenInventory().getTopInventory().getTitle().contains("Level Up")){
					ParticleEffects.ENCHANTMENT_TABLE.sendToPlayer(p, event.getPlayer().getLocation(), new Random().nextFloat(), new Random().nextFloat(), new Random().nextFloat(), 1, 100);
					}else{
						StopF(S);
					}
					} catch (Exception e) {
					e.printStackTrace();
				}
				}}, 10, 10);
		}
	}
}
public void StopF(int i){
	Bukkit.getScheduler().cancelTask(i);
}
}

