package com.me.sly.CrucibleEmpire.Mining;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerAnimationType;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.me.sly.CrucibleEmpire.Main.ceMain;

public class FatiugeAndBreaks implements Listener{
	public static ceMain plugin;
    public FatiugeAndBreaks(ceMain instance) {
        plugin = instance;
    }
    @SuppressWarnings("deprecation")
	@EventHandler
    public void mining(PlayerAnimationEvent event){
    	if(event.getAnimationType().equals(PlayerAnimationType.ARM_SWING)){
    		if(event.getPlayer().getTargetBlock(null, 4).getType() == Material.COAL_ORE){
    			if(event.getPlayer().getItemInHand().getType() == Material.STONE_PICKAXE){
    			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5, 3));
    		}else if(event.getPlayer().getItemInHand().getType() == Material.IRON_PICKAXE){
    			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5, 2));
    		}else if(event.getPlayer().getItemInHand().getType() == Material.DIAMOND_PICKAXE){
    		}else if(event.getPlayer().getItemInHand().getType() == Material.GOLD_PICKAXE){
    			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5, 1));
    		}
    		}else if(event.getPlayer().getTargetBlock(null, 4).getType() == Material.IRON_ORE){
    			if(event.getPlayer().getItemInHand().getType() == Material.STONE_PICKAXE){
    			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5, 10000));
    		}else if(event.getPlayer().getItemInHand().getType() == Material.IRON_PICKAXE){
    			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5, 3));
    		}else if(event.getPlayer().getItemInHand().getType() == Material.DIAMOND_PICKAXE){
    			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5, 1));
    		}else if(event.getPlayer().getItemInHand().getType() == Material.GOLD_PICKAXE){
    			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5, 2));
    		}	
    		}else if(event.getPlayer().getTargetBlock(null, 4).getType() == Material.DIAMOND_ORE){
    			if(event.getPlayer().getItemInHand().getType() == Material.STONE_PICKAXE){
    			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5, 10000));
    		}else if(event.getPlayer().getItemInHand().getType() == Material.IRON_PICKAXE){
    			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5, 10000));
    		}else if(event.getPlayer().getItemInHand().getType() == Material.DIAMOND_PICKAXE){
    			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5, 4));
    		}else if(event.getPlayer().getItemInHand().getType() == Material.GOLD_PICKAXE){
    			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5, 10000));
    		}
    		}else if(event.getPlayer().getTargetBlock(null, 4).getType() == Material.GOLD_ORE){
    			if(event.getPlayer().getItemInHand().getType() == Material.STONE_PICKAXE){
    			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5, 10000));
    		}else if(event.getPlayer().getItemInHand().getType() == Material.IRON_PICKAXE){
    			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5, 10000));
    		}else if(event.getPlayer().getItemInHand().getType() == Material.DIAMOND_PICKAXE){
    			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 5, 2));
    		}else if(event.getPlayer().getItemInHand().getType() == Material.GOLD_PICKAXE){
    			event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 5, 0));
    			}
    		}
    	}
    }
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onCoalMine(BlockBreakEvent event){
		if(event.getBlock().getType()==Material.COAL_ORE){
			event.setCancelled(true);
			event.setExpToDrop(0);
			event.getBlock().setTypeId(7);
		}else if(event.getBlock().getType()==Material.IRON_ORE){
			event.setCancelled(true);
			event.setExpToDrop(0);
			event.getBlock().setTypeId(7);
		}else if(event.getBlock().getType()==Material.GOLD_ORE){
			event.setCancelled(true);
			event.setExpToDrop(0);
			event.getBlock().setTypeId(7);
		}else if(event.getBlock().getType()==Material.DIAMOND_ORE){
			event.setCancelled(true);
			event.setExpToDrop(0);
			event.getBlock().setTypeId(7);
		}
	}
	
	
	
	
}
