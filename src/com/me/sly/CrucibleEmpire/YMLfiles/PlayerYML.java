package com.me.sly.CrucibleEmpire.YMLfiles;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.bukkit.configuration.file.YamlConfiguration;

import com.me.sly.CrucibleEmpire.Main.ceMain;

public class PlayerYML{

	public static ceMain plugin;
	public PlayerYML(ceMain p){
		plugin = p;
	}


	@SuppressWarnings("deprecation")
	public static void reloadCustomConfig(String player){
		InputStream defConfigStream = plugin.getResource("CrucibleEmpire/Users/" + player + ".yml");
		if(defConfigStream != null){
			YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
			YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder().getPath() + "/Users/" + player + ".yml")).setDefaults(defConfig);
		}
	}
	public static void saveCustomConfig(String player){
		try{
			plugin.getPlayerConfig(player).save(new File(plugin.getDataFolder().getPath() + "/Users/" + player + ".yml"));
		} catch (IOException ex){
		}
	}

}
