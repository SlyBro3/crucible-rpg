package com.me.sly.CrucibleEmpire.YMLfiles;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.me.sly.CrucibleEmpire.Main.ceMain;

public class SpawnerYML{
	public static ceMain plugin;
	@SuppressWarnings("static-access")
	public SpawnerYML(ceMain plugin){
		this.plugin = plugin;
	}

	private static FileConfiguration customConfig = null;
	private static File customConfigFile = null;
	
	@SuppressWarnings("deprecation")
	public static void reloadCustomConfig(){
		if (customConfigFile == null){
			customConfigFile = new File(plugin.getDataFolder(), "Spawner.yml");
		}

		customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

		InputStream defConfigStream = plugin.getResource("Spawner.yml");
		if(defConfigStream != null){
			YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
			customConfig.setDefaults(defConfig);
		}
	}

	public static FileConfiguration getCustomConfig(){
		if(customConfig == null){
			reloadCustomConfig();
		}
		return customConfig;
	}

	public static void saveCustomConfig(){
		if(customConfig == null || customConfigFile == null){
			return;
		}
		try{
			getCustomConfig().save(customConfigFile);
		} catch (IOException ex){
			plugin.getLogger().log(Level.SEVERE, "Could not save config to " + customConfigFile, ex);
		}
	}

	public static void saveDefaultConfig(){
		if(customConfigFile == null){
			customConfigFile = new File(plugin.getDataFolder(), "Spawner.yml");
		}
		if(!customConfigFile.exists()){
			plugin.saveResource("Spawner.yml", false);
		}
	}

}
