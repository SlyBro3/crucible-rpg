package com.me.sly.resources;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class BetterItems{

	public static ItemStack DisplayName(Material type, int amount, String displayname){
		ItemStack R = new ItemStack(type, amount);
		ItemMeta rMeta = R.getItemMeta();
		rMeta.setDisplayName(displayname);
		R.setItemMeta(rMeta);
		return R;
	}
	public static ItemStack DisplayNameAndLore(Material type, int amount, String displayname, String... lore){
		ItemStack R = new ItemStack(type, amount);
		ItemMeta rMeta = R.getItemMeta();
		List<String> rLore = new ArrayList<String>();
		for(String rlo : lore){
			rLore.add(rlo);
		}
		rMeta.setLore(rLore);
		rMeta.setDisplayName(displayname);
		R.setItemMeta(rMeta);
		return R;
	}
	public static ItemStack Lore(Material type, int amount, String... lore){
		ItemStack R = new ItemStack(type, amount);
		ItemMeta rMeta = R.getItemMeta();
		List<String> rLore = new ArrayList<String>();
		for(String rlo : lore){
			rLore.add(rlo);
		}
		rMeta.setLore(rLore);
		R.setItemMeta(rMeta);
		return R;
	}
	
	
	
	
	
	
	
	
	
	
}
