package com.me.sly.resources;

import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.entity.Player;

public class MyXpBar {

        private HashMap<Integer, Integer> defaultXpLevels = new HashMap<Integer, Integer>();
        private HashMap<Integer, Integer> customXpLevels = new HashMap<Integer, Integer>();
        
        /**
         * Constructor
         * @param cl HashMap with levels and its required experience (f.e. 1,10 - Level 1 requires 10 experience)
         */
        public MyXpBar(HashMap<Integer, Integer> cl) {
                
                loadDefaultXpLevels();
                customXpLevels = defaultXpLevels;
                loadCustomXpLevels(cl);
        }
        
        /**
         * Adds xp to a player
         * @param p the Player
         * @param amount the amount of xp
         */
        public void addXp(Player p, int amount) {
                
                int level = p.getLevel();
                float oldXpBar = p.getExp();
                float oldTotalXp = p.getTotalExperience();

                if(customXpLevels.size() < (level+1)) {
                        
                        System.out.println("[MyXpBar] Not enough custom levels set.");
                        return;
                }
                
                float needCustomXp = customXpLevels.get(level+1);
                
                float newTotalXp = oldTotalXp+amount;
                p.setTotalExperience((int) newTotalXp);
                
                float newXpBar = oldXpBar + ((1/needCustomXp)*amount);
                if(newXpBar >= 1) {
                        
                        float overXpBar = newXpBar-1;
                        p.setLevel(level+1);
                        p.setExp(overXpBar);
                } else {

                        p.setExp(newXpBar);
                }
        }        
        
        /**
         * Removes xp from a player
         * @param p the player
         * @param amount the amount of xp
         */
        public void removeXp(Player p, int amount) {
                
                int level = p.getLevel();
                float oldXpBar = p.getExp();
                float oldTotalXp = p.getTotalExperience();

                if(customXpLevels.size() < (level)) {
                        
                        System.out.println("[MyXpBar] Not enough custom levels set.");
                        return;
                }

                float needCustomXp = customXpLevels.get(level);
                
                float newTotalXp = oldTotalXp-amount;
                p.setTotalExperience((int) newTotalXp);
                
                float newXpBar = oldXpBar - ((1/needCustomXp)*amount);
                if(newXpBar <= 0) {
                        
                        float underXpBar = 1F-newXpBar;
                        p.setLevel(level-1);
                        p.setExp(underXpBar);
                } else {

                        p.setExp(newXpBar);
                }
        }
        
        private void loadCustomXpLevels(HashMap<Integer, Integer> cl) {
                
                for(Entry<Integer, Integer> e : cl.entrySet()) {
                        
                        customXpLevels.put(e.getKey(), e.getValue());
                }
        }
        
        private void loadDefaultXpLevels() {

                defaultXpLevels.put(1, 17); 
                defaultXpLevels.put(2, 17);
                defaultXpLevels.put(3, 17);
                defaultXpLevels.put(4, 17);
                defaultXpLevels.put(5, 17);
                defaultXpLevels.put(6, 17);
                defaultXpLevels.put(7, 17);
                defaultXpLevels.put(8, 17);
                defaultXpLevels.put(9, 17);
                defaultXpLevels.put(10, 17);
                defaultXpLevels.put(11, 17);
                defaultXpLevels.put(12, 17);
                defaultXpLevels.put(13, 17);
                defaultXpLevels.put(14, 17);
                defaultXpLevels.put(15, 17);
                defaultXpLevels.put(16, 17);
                defaultXpLevels.put(17, 20);
                defaultXpLevels.put(18, 23);
                defaultXpLevels.put(19, 26);
                defaultXpLevels.put(20, 29);
                defaultXpLevels.put(21, 32);
                defaultXpLevels.put(22, 35);
                defaultXpLevels.put(23, 38);
                defaultXpLevels.put(24, 41);
                defaultXpLevels.put(25, 44);
                defaultXpLevels.put(26, 47);
                defaultXpLevels.put(27, 50);
                defaultXpLevels.put(28, 53);
                defaultXpLevels.put(29, 56);
                defaultXpLevels.put(30, 59);
                defaultXpLevels.put(31, 62);
                defaultXpLevels.put(32, 69);
                defaultXpLevels.put(33, 76);
                defaultXpLevels.put(34, 83);
                defaultXpLevels.put(35, 90);
                defaultXpLevels.put(36, 97);
                defaultXpLevels.put(37, 104);
                defaultXpLevels.put(38, 111);
                defaultXpLevels.put(39, 118);
                defaultXpLevels.put(40, 125);
                defaultXpLevels.put(41, 132);
                defaultXpLevels.put(42, 139);
                defaultXpLevels.put(43, 146);
                defaultXpLevels.put(44, 153);
                defaultXpLevels.put(45, 160);
                defaultXpLevels.put(46, 167);
                defaultXpLevels.put(47, 174);
                defaultXpLevels.put(48, 181);
                defaultXpLevels.put(49, 188);
                defaultXpLevels.put(50, 195);
        }
}